/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2021 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/* REXX OS/2 Support                                                          */
/*                                                                            */
/* Semaphore support for OS/2 systems                                         */
/*                                                                            */
/******************************************************************************/

#ifndef SysSemaphore_DEFINED
#define SysSemaphore_DEFINED

#include <sys/types.h>
#define  INCL_DOS
#define  INCL_WIN
#define  INCL_ERRORS
#define  OS2EMX_PLAIN_CHAR
#include <os2.h>
#include "SysThread.hpp"


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Default constructors of the SysSemaphore and SysMutex don't zeroes the handle.
// This is bad. But otherwise Interpreter::resourceLock and Interpreter::dispatchLock
// can't be initialized from __dll_initialize() because this function is called
// before their constructors. Must be redesigned?
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


class SysSemaphore {
public:
     SysSemaphore() {}
     SysSemaphore(bool);
     ~SysSemaphore() { close(); }

     void create();
     void close();
     void post();
     void wait();
     bool wait(uint32_t);
     void reset();
     bool posted();

protected:
     HEV  hev;
};


class SysMutex {
public:
     SysMutex() : bypassMessageLoop(false) {}
     SysMutex(bool create, bool critical = false);
     ~SysMutex() { close(); }
     void create(bool critical = false);
     void close();
     bool request();
     bool request(uint32_t t);
     bool requestImmediate();
     bool release();

protected:
     HMUX hmtx;
     bool bypassMessageLoop;
};

#endif
