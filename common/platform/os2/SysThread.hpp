/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2022 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/* REXX Kernel                                              SysThread.hpp     */
/*                                                                            */
/* Abstract system thread class.  This is just a template class, which no     */
/* virtual functions defined.  This gives the implementing platform a lot of  */
/* implementation flexibility to use inline methods where appropriate,        */
/* particularly for functions that are NOPs.  The platform is also free to    */
/* define any internal data necessary to implement the thread functions.      */
/*                                                                            */
/******************************************************************************/

#ifndef Included_SysThread
#define Included_SysThread

#include <sys/types.h>
#include <stddef.h>
#define  INCL_DOS
#define  INCL_WIN
#define  INCL_LONGLONG
#define  OS2EMX_PLAIN_CHAR
#include <os2.h>

/**
 * A wrapper anound an OS/2-style thread.
 */
class SysThread
{

public:

    enum
    {
        THREAD_STACK_SIZE = 1024*512
    };

    SysThread() : attached(false), tid(-1) {}
    virtual ~SysThread() {}

    virtual void attachThread();
    virtual void dispatch();
    void terminate();
    void startup();
    void shutdown();
    void yield();
    uintptr_t threadID() { return (uintptr_t)tid; }
    bool equals(SysThread &other);
    size_t hash() { return (((size_t)tid) >> 8) * 37; }
    void waitForTermination();

    static void sleep(int msecs);
    static int longSleep(uint64_t microseconds);
    static uint64_t getMillisecondTicks();
    static int createThread(unsigned long &threadNumber, unsigned int stackSize, void (*startRoutine)(void *), void *startArgument);

protected:
    void createThread();

    bool attached;   // indicates whether this was a created thread or attached
    TID  tid;        // thread identifier
};

#endif
