/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2022 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/* REXX Kernel                                              SysThread.cpp     */
/*                                                                            */
/* OS/2 implementation of the SysThread class.                                */
/*                                                                            */
/******************************************************************************/

#include "SysThread.hpp"
#include "SysProcess.hpp"
#include <stdlib.h>
#include <process.h>

/**
 * Stub function for handling the intial startup on a new thread.
 *
 * @param arguments The void argument object, which is just a pointer to the
 *                  SysThread object.
 *
 * @return Always returns 0.
 */
static void call_thread_function(void * arguments)
{
    HAB  hab = NULLHANDLE;
    HMQ  hmq = NULLHANDLE;

    if (SysProcess::isGUI())
    {
         hab = WinInitialize(0);
         hmq = WinCreateMsgQueue(hab, 0);
    }

    ((SysThread *)arguments)->dispatch();

    if (hmq)
    {
         WinDestroyMsgQueue(hmq);
    }
    if (hab)
    {
         WinTerminate(hab);
    }

    _endthread();
}


/**
 * Create a new thread
 */
void SysThread::createThread()
{
    tid = _beginthread(call_thread_function, NULL, THREAD_STACK_SIZE, (void *)this);
    attached = false;
}


/**
 * Create a new thread.
 *
 * @param threadId  The id of the created thread
 * @param stackSize The required stack size
 * @param startRoutine
 *                  The thread startup routine.
 * @param startArgument
 *                  The typeless argument passed to the startup routine.
 *
 * @return The success/failure return code.
 */
int SysThread::createThread(unsigned long &threadNumber, unsigned int stackSize, void (*startRoutine)(void *), void *startArgument)
{
   threadNumber = _beginthread(startRoutine, NULL, THREAD_STACK_SIZE, startArgument);
   return threadNumber != -1;
}


/**
 * Attach an activity to an existing thread
 */
void SysThread::attachThread()
{
    PTIB ptib;

    if (DosGetInfoBlocks(&ptib, NULL) == 0)
    {
         tid = ptib->tib_ptib2->tib2_ultid;
    }

    // we don't own this one (and don't terminate it)
    attached = true;
}


/**
 * This is a dummy method intended for subclass overrides. This
 * dispatches the code on the new thread.
 */
void SysThread::dispatch()
{
    // default dispatch returns immediately
}


/**
 * Do any platform specific termination
 */
void SysThread::terminate()
{
    // This is a NOP on OS/2, because usage of this method is not clear.
}


/**
 * Do any thread-specific startup activity.
 */
void SysThread::startup()
{
    // This is a NOP on OS/2, because usage of this method is not clear.
}


/**
 * Perform any platform-specific thread shutdown activities.
 */
void SysThread::shutdown()
{
    // This is a NOP on OS/2, because usage of this method is not clear.
}


/**
 * Yield control to other threads.
 */
void SysThread::yield()
{
    // Just give up the time slice
    DosSleep(1);
}


/**
 * Test whether two threads are the same.
 *
 * @param other  The other thread object.
 *
 * @return True if they are the same thread, false if not.
 */
bool SysThread::equals(SysThread &other)
{
    return tid == other.tid;
}


/**
 * Wait for the thread to terminate
 */
void SysThread::waitForTermination()
{
    if (!attached && tid != -1)
    {
        DosWaitThread(&tid, DCWW_WAIT);
        tid = -1;
    }
}


/**
 * Platform wrapper around a simple sleep function.
 *
 * @param msecs  The number of milliseconds to sleep.
 */
void SysThread::sleep(int msecs)
{
    DosSleep(msecs);
}


/**
 * Platform wrapper around a sleep function that can sleep for long periods of time. .
 *
 * @param microseconds
 *               The number of microseconds to delay.
 */
int SysThread::longSleep(uint64_t microseconds)
{
    // convert to milliseconds, no overflow possible
    ULONG msecs = (long)(microseconds / 1000);
    ULONG timer_id;

    // Using DosSleep with a long timeout risks sleeping on a thread with a message
    // queue, which can make the system sluggish, or possibly deadlocked.  If the
    // sleep is longer than 333 milliseconds use a window timer to avoid this
    // risk.
    if (msecs > 333 && SysProcess::isGUI())
    {
        if ((timer_id = WinStartTimer(NULLHANDLE, NULLHANDLE, 0, msecs)) == 0)
        {
            return WinGetLastError(NULLHANDLE);
        }

        QMSG qms;
        while (WinGetMsg(NULLHANDLE, &qms, 0, 0, 0))
        {
            if (qms.msg == WM_TIMER && LONGFROMMP(qms.mp1) == timer_id && qms.hwnd == NULLHANDLE)
            {
                // If our timer message, exit loop.
                WinStopTimer(NULLHANDLE, NULLHANDLE, timer_id);
                break;
            }
            WinDispatchMsg(NULLHANDLE, &qms);
        }
    }
    else
    {
        DosSleep(msecs);
    }
    return 0;
}


/**
 * Retrieves the number of milliseconds that have elapsed since the system was started.
 */

uint64_t SysThread::getMillisecondTicks()
{
  ULONGLONG time;
  ULONG freq;

  DosTmrQueryFreq(&freq);
  DosTmrQueryTime((PQWORD)&time);
  return (time / freq * 1000) + (time % freq * 1000 / freq);
}
