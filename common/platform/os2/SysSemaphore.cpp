/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2021 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/* REXX OS/2 Support                                                          */
/*                                                                            */
/* Semaphore support for OS/2                                                 */
/*                                                                            */
/******************************************************************************/

#include "SysSemaphore.hpp"
#include <stdio.h>


/**
 * Create a semaphore with potential creation-time
 * initialization.
 *
 * @param create Indicates whether the semaphore should be created now.
 */
SysSemaphore::SysSemaphore(bool createSem)
{
    hev = 0;
    if (createSem)
    {
        create();
    }
}


/**
 * Create the semaphore
 */
void SysSemaphore::create()
{
    if (hev == 0)
    {
        APIRET rc = DosCreateEventSem(NULL, &hev, 0, FALSE);
        if (rc != 0)
        {
            fprintf(stderr,"*** ERROR: At SysSemaphore(), DosCreateEventSem returns %d!\r\n", rc);
        }
    }
}


/**
 * Close the semaphore
 */
void SysSemaphore::close()
{
    if (hev != 0)
    {
        APIRET rc = DosCloseEventSem(hev);
        if (rc != 0)
        {
            fprintf(stderr,"*** ERROR: At SysSemaphore(), DosCloseEventSem returns %d!\r\n", rc);
        }
        hev = 0;
    }
}


/**
 * Post that an event has occurred.
 */
void SysSemaphore::post()
{
    APIRET rc = DosPostEventSem(hev);
    if (rc != 0)
    {
        fprintf(stderr,"*** ERROR: At SysSemaphore(), DosPostEventSem returns %d!\r\n", rc);
    }
}


/**
 * Wait for an event semaphore to be posted.
 */
void SysSemaphore::wait()
{
    wait(SEM_INDEFINITE_WAIT);
}


/**
 * Wait for the event, with a timeout.
 *
 * @param t      The timeout value, in milliseconds.
 *
 * @return       true if the event occurrec, false if there was
 *               a timeout.
 */
bool SysSemaphore::wait(uint32_t t)
{
    APIRET rc = DosWaitEventSem(hev, t);
    if (rc != 0 && rc != ERROR_TIMEOUT)
    {
        fprintf(stderr,"*** ERROR: At SysSemaphore(), DosWaitEventSem returns %d!\r\n", rc);
    }
    return (rc == 0);
}


/**
 * Reset the semaphore
 */
void SysSemaphore::reset()
{
    ULONG ulPostCt;
    APIRET rc = DosResetEventSem(hev, &ulPostCt);
    if (rc != 0 && rc != ERROR_ALREADY_RESET)
    {
        fprintf(stderr,"*** ERROR: At SysSemaphore(), DosResetEventSem returns %d!\r\n", rc);
    }
}


/**
 * Retrieves the post state for an event semaphore.
 */
bool SysSemaphore::posted()
{
    ULONG ulPostCt = 0;
    DosQueryEventSem(hev, &ulPostCt);
    return (ulPostCt != 0);
}


/**
 * Create a semaphore with potential creation-time
 * initialization.
 *
 * @param createSem
 * @param critical  Critical indicates the most efficient dispatching mechanism should be use.
 */
SysMutex::SysMutex(bool createSem, bool critical)
{
    hmtx = 0;
    bypassMessageLoop = critical;
    if (createSem)
    {
        create(critical);
    }
}


/**
 * Create a mutex for operation
 *
 * @param critical Indicates whether this is a "critical time" semaphore that will only be
 *                 held for short windows. The most optimal (i.e., no message loop on PM)
 *                 mechanism will be used for requesting the semaphore.
 */
void SysMutex::create(bool critical)
{
    bypassMessageLoop = critical;
    if (hmtx == 0)
    {
        APIRET rc = DosCreateMutexSem(NULL, (PHMTX)&hmtx, 0, FALSE);
        if (rc != 0)
        {
            fprintf(stderr,"*** ERROR: At SysMutex(), DosCreateMutexSem returns %d!\r\n", rc);
        }
    }
}


/**
 * Close the mutex semaphore.
 */
void SysMutex::close()
{
    if (hmtx != 0)
    {
        APIRET rc = DosCloseMutexSem(hmtx);
        if (rc == ERROR_SEM_BUSY)
        {
            rc = DosReleaseMutexSem(hmtx);
            rc = DosCloseMutexSem(hmtx);
        }
        if (rc != 0)
        {
            fprintf(stderr,"*** ERROR: At SysMutex(), DosCloseMutexSem returns %d!\r\n", rc);
        }
        hmtx = 0;
    }
}


/**
 * Lock the mutex with a wait time.
 *
 * @param t      The time to wait
 *
 * @return true of the lock was obtained, false otherwise.
 */
bool SysMutex::request(uint32_t t)
{
    APIRET rc;

    if (bypassMessageLoop)
    {
        rc = DosRequestMutexSem(hmtx, t);
    }
    else
    {
        rc = WinRequestMutexSem(hmtx, t);
    }

    if (rc != 0 && rc != ERROR_TIMEOUT)
    {
        fprintf(stderr,"*** ERROR: At SysMutex(), DosRequestMutexSem returns %d!\r\n", rc);
    }

    return (rc == 0);
}


/**
 * Lock the mutex with a wait time.
 *
 * @return true of the lock was obtained, false otherwise.
 */
bool SysMutex::request()
{
    return request(SEM_INDEFINITE_WAIT);
}


/**
 * Immediate lock the mutex.
 *
 * @return true of the lock was obtained, false otherwise.
 */
bool SysMutex::requestImmediate()
{
    return (DosRequestMutexSem(hmtx, 0) == 0);
}


/**
 * Release the mutex lock.
 *
 * @return true of the lock was released, false otherwise.
 */
bool SysMutex::release()
{
    APIRET rc = DosReleaseMutexSem(hmtx);
    if (rc != 0)
    {
        fprintf(stderr,"*** ERROR: At SysMutex(), DosReleaseMutexSem returns %d!\r\n", rc);
    }
    return (rc == 0);
}
