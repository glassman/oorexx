/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2020 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/* REXX OS/2 Support                                                          */
/*                                                                            */
/* Process support for OS/2                                                   */
/*                                                                            */
/******************************************************************************/

#include "SysProcess.hpp"
#include "SysNLS.hpp"
#include "SysLocalAPIManager.hpp"
#include <string.h>
#include <stdio.h>

// full path of the currently running executable
const char *SysProcess::executableFullPath = NULL;
// directory of our Rexx shared libraries
const char *SysProcess::libraryLocation = NULL;


/**
 * Get the current user name information.
 *
 * @param buffer The buffer (of at least MAX_USERID_LENGTH characters) into which the userid is copied.
 */
void SysProcess::getUserID(char *buffer)
{
    PSZ userid;

    if (DosScanEnv("USER", &userid) != 0)
    {
        userid = (PSZ)"USER";
    }

    strlcpy(buffer, userid, MAX_USERID_LENGTH);
}


/**
 * Get the current process identifier.
 */
int SysProcess::getPid()
{
    PPIB ppib;

    if (DosGetInfoBlocks(NULL, &ppib) == 0)
    {
        return ppib->pib_ulpid;
    }

    return 0;
}


/**
 * Determine the location of the running program. This returns the full path
 * of the currently running executable.
 *
 * @return A character string of the path (does not need to be freed by the caller)
 */
const char *SysProcess::getExecutableFullPath()
{
    if (executableFullPath != NULL)
    {
        return executableFullPath;
    }

    char modulePath[CCHMAXPATH];
    PPIB ppib;

    if (DosGetInfoBlocks(NULL, &ppib) == 0)
    {
        if (DosQueryModuleName(ppib->pib_hmte, sizeof(modulePath), modulePath) == 0)
        {
             executableFullPath = strdup(modulePath);
        }
    }

    return executableFullPath;
}


/**
 * Determine the location of the rexxapi.dll library. This returns the
 * directory portion of the library's path with a trailing backslash.
 *
 * @return A character string of the location (does not need to be freed by the caller)
 */
const char *SysProcess::getLibraryLocation()
{
    if (libraryLocation == NULL)
    {
        char modulePath[CCHMAXPATH];

        if (RexxLibraryLocation(modulePath, sizeof(modulePath)) == 0)
        {
            libraryLocation = strdup(modulePath);
        }
    }

    return libraryLocation;
}

/**
 * do a beep tone
 *
 * @param frequency The frequency to beep at
 * @param duration  The duration to beep (in milliseconds)
 */
void SysProcess::beep(int frequency, int duration)
{
    DosBeep(frequency, duration);
}


/**
 * Remove an environment variable.
 *
 * @param variableName
 *               The name of the environment variable.
 */
int SysProcess::removeEnvironmentVariable(const char *variableName)
{
    PPIB   ppib;
    size_t envsize = 0;
    char*  pvariable = NULL;
    size_t namelen = strlen(variableName);
    char*  pmem;
    APIRET rc;

    // get the current environment
    if ((rc = DosGetInfoBlocks(NULL, &ppib)) != 0)
    {
        return rc;
    }

    // calculate the current environment size and search variable
    for (pmem = ppib->pib_pchenv; *pmem;)
    {
        size_t len = strlen(pmem) + 1;

        if (SysNLS::strnicmp(pmem, variableName, namelen) == 0 && pmem[namelen] == '=')
        {
            pvariable = pmem;
        }

        envsize += len;
        pmem += len;
    }
    ++envsize;

    // remove variable if found
    if (pvariable)
    {
        size_t len = strlen(pvariable) + 1;
        memmove(pvariable, pvariable + len,  ppib->pib_pchenv + envsize - pvariable - len);
    }
    return 0;
}


/**
 * Set an environment variable to a new value.
 *
 * @param variableName
 *               The name of the environment variable.
 * @param value  The variable value.
 */
int SysProcess::setEnvironmentVariable(const char *variableName, const char *value)
{
    PPIB   ppib;
    char*  pmem;
    size_t envsize = 0;
    ULONG  envflag = 0;
    APIRET rc;
    PSZ    oldvalue = NULL;
    size_t oldlen = 0;
    size_t newlen = 0;
    size_t newsize;
    ULONG  memsize = 65535;

    // first check the extended LIBPATH variables as they require
    // special processing.
    if (stricmp(variableName, "BEGINLIBPATH") == 0)
    {
        return DosSetExtLIBPATH(value, BEGIN_LIBPATH);
    }
    if (stricmp(variableName, "ENDLIBPATH") == 0)
    {
        return DosSetExtLIBPATH(value, END_LIBPATH);
    }
    if (stricmp(variableName, "LIBPATHSTRICT") == 0)
    {
        return DosSetExtLIBPATH(value, LIBPATHSTRICT);
    }

    // NULL is special, it removes the variable
    if (value == NULL || !*value)
    {
        return removeEnvironmentVariable(variableName);
    }

    // get the current environment
    if ((rc = DosGetInfoBlocks(NULL, &ppib)) != 0)
    {
        return rc;
    }

    // calculate the current environment size
    for (pmem = ppib->pib_pchenv; *pmem;)
    {
        size_t len = strlen(pmem) + 1;
        envsize += len;
        pmem += len;
    }
    ++envsize;

    // try to find old value of variable
    if (DosScanEnv(variableName, &oldvalue) == 0)
    {
        oldlen = strlen(oldvalue) + 1;
        newlen = strlen(value) + 1;
    }
    else
    {
        newlen = strlen(variableName) + 1 + strlen(value) + 1;
    }

    // calculate new environment size
    newsize = envsize + newlen - oldlen;

    // prepary environment memory to change
    if ((rc = DosQueryMem(ppib->pib_pchenv, &memsize, &envflag)) != 0)
    {
        return rc;
    }
    if (newsize > memsize)
    {
        if (newsize > 65535)
        {
            return ERROR_NOT_ENOUGH_MEMORY;
        }
        else
        {
            ULONG grow = newsize - memsize;
            if ((rc = DosSetMem(ppib->pib_pchenv+grow, grow, PAG_COMMIT | PAG_WRITE | PAG_READ)) != 0)
            {
                return rc;
            }
        }
    }

    if (oldvalue)
    {
        // replace the old value
        size_t size = ppib->pib_pchenv + envsize - oldvalue - oldlen;
        if (newlen != oldlen)
        {
            memmove(oldvalue+newlen, oldvalue+oldlen, size);
        }
        memcpy(oldvalue, value, newlen);
    }
    else
    {
        // add the new variable to the end
        sprintf(ppib->pib_pchenv + envsize - 1, "%s=%s%c", variableName, value, 0);
    }

    return 0;
}


/**
 * Determine the process type.
 *
 * @return true if currrent process is a Presentation Manager protect-mode session.
 */

bool SysProcess::isGUI()
{
    PPIB ppib;

    if (DosGetInfoBlocks( NULL, &ppib ) == 0)
    {
        return (ppib->pib_ultype == 3);
    }

    return false;
}
