/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 2005-2020 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/* OS/2 NLS class                                                             */
/******************************************************************************/

#include "SysNLS.hpp"

bool SysNLS::initialized = 0;
char SysNLS::mapcase[256];

/**
 * Initializes the national language support class.
 */
void SysNLS::initialize()
{
    if (!initialized)
    {
        COUNTRYCODE countrycode = {0, 0};
        int i;

        for (i = 0; i < 256; i++) {
            mapcase[i] = (char)i;
        }

        DosMapCase(256, &countrycode, mapcase);
        initialized = true;
    }
}


/**
 * Compare strings without case sensitivity.
 */

int SysNLS::stricmp(const char *string1, const char *string2)
{
    int relationship = 0;

    if (!initialized)
    {
        initialize();
    }

    do {
        relationship = mapcase[(unsigned char)*string1] -
                       mapcase[(unsigned char)*string2];

    } while (*string1++ && *string2++ && relationship == 0);

    return relationship;
}

/**
 * Compare strings without case sensitivity.
 */

int SysNLS::strnicmp( const char* string1, const char* string2, int n )
{
    int relationship = 0;

    if (!initialized)
    {
        initialize();
    }

    if (n > 0) {
        do {
            relationship = mapcase[(unsigned char)*string1] -
                           mapcase[(unsigned char)*string2];

        } while (*string1++ && *string2++ && --n && relationship == 0);
    }

    return relationship;
}
