/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 2005-2021 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include "SysCSNamedPipeStream.hpp"
#include "SysProcess.hpp"
#include <string.h>
#include <stdio.h>


// The pipe and mutex names used for this userid
const char *SysServerNamedPipeConnectionManager::userPipeName = NULL;
const char *SysServerNamedPipeConnectionManager::userMutexName = NULL;


/**
 * Open a connection to a named pipe.
 *
 * @param pipeName The name of the target pipe
 *
 * @return True on success, otherwise false.
 */
bool SysNamedPipeConnection::connect(const char *pipeName)
{
    ULONG  action;
    APIRET rc;
    ULONG  attempts = 0;

    // we may need to retry this a few times if things are busy
    while((rc = DosOpen((PSZ)pipeName, &hPipe, &action, 0, FILE_NORMAL,
                        OPEN_ACTION_FAIL_IF_NEW  | OPEN_ACTION_OPEN_IF_EXISTS,
                        OPEN_SHARE_DENYREADWRITE | OPEN_ACCESS_READWRITE | OPEN_FLAGS_FAIL_ON_ERROR,
                        NULL )) == ERROR_PIPE_BUSY && attempts++ < 1000 )
    {
        DosSleep(1);
    }

    if (rc == 0)
    {
        errcode = CSERROR_OK;
        return true;
    }
    else
    {
        errcode = CSERROR_CONNX_FAILED;
        return false;
    }
}

/**
 * Close the server connection.
 *
 * @return True on success, otherwise false.
 */
bool SysNamedPipeConnection::disconnect()
{
    if (hPipe != NULLHANDLE)
    {
        if (DosClose(hPipe) == 0)
        {
            hPipe = NULLHANDLE;
            errcode = CSERROR_OK;
            return true;
        }
    }

    errcode = CSERROR_INTERNAL;
    return false;
}


/**
 * Read from the connection.
 *
 * @param buf       Target buffer for the read operation.
 * @param bufsize   Size of the target buffer.
 * @param bytesread Number of bytes actually read.
 *
 * @return True on success, otherwise false.
 */
bool SysNamedPipeConnection::read(void *buf, size_t bufsize, size_t *bytesread)
{
    if (hPipe == NULLHANDLE)
    {
        errcode = CSERROR_IO_FAILED;
        return false;
    }

    ULONG actual = 0;

    // try to read from the pipe
    if (DosRead(hPipe, buf, bufsize, &actual) != 0)
    {
        errcode = CSERROR_IO_FAILED;
        *bytesread = 0;
        return false;
    }
    *bytesread = actual;
    errcode = CSERROR_OK;
    return true;
}


/**
 * Write a buffer to the connection.
 *
 * @param buf     Source buffer for the write operation.
 * @param bufsize Size of the source buffer.
 * @param byteswritten
 *                Number of bytes actually written to the connection.
 *
 * @return True on success, otherwise false.
 */
bool SysNamedPipeConnection::write(void *buf, size_t bufsize, size_t *byteswritten)
{
    if (hPipe == NULLHANDLE)
    {
        errcode = CSERROR_IO_FAILED;
        return false;
    }

    ULONG actual = 0;

    // write the data to the pipe
    if (DosWrite(hPipe, buf, bufsize, &actual) != 0)
    {
        errcode = CSERROR_IO_FAILED;
        *byteswritten = 0;
        return false;
    }
    *byteswritten = actual;
    errcode = CSERROR_OK;
    return true;
}


/**
 * Write a multi-buffer message to the connection.
 *
 * @param buf     Source buffer for the write operation.
 * @param bufsize Size of the source buffer.
 * @param byteswritten
 *                Number of bytes actually written to the connection.
 *
 * @return True on success, otherwise false.
 */
bool SysNamedPipeConnection::write(void *buf, size_t bufsize, void *buf2, size_t buf2size, size_t *byteswritten)
{
    // if the second buffer is of zero size, we can handle without
    // copying
    if (buf2size == 0)
    {
        return write(buf, bufsize, byteswritten);
    }

    size_t bufferSize = bufsize + buf2size;

    // get a buffer large enough for both buffer
    char *buffer = getMessageBuffer(bufferSize);
    // if we can't get a buffer, then try sending this in pieces
    if (buffer == NULL)
    {
        // write the first buffer
        if (!write(buf, bufsize, byteswritten))
        {
            return false;
        }
        size_t buf2written = 0;
        if (!write(buf2, buf2size, &buf2written))
        {
            return false;
        }
        *byteswritten += buf2written;
        return true;
    }

    // copy the message and attached data into a single buffer
    memcpy(buffer, buf, bufsize);
    memcpy(buffer + bufsize, buf2, buf2size);

    // perform the write now
    write(buffer, bufferSize, byteswritten);

    // we're done with the buffer, regardless of whether this works or fails
    returnMessageBuffer(buffer);
    return true;
}


/**
 * Bind to the named pipe by creating the first instance of the
 * pipe with the given name.
 *
 * @return true if the pipe could be created successfully, false for any creation failures.
 */
bool SysServerNamedPipeConnectionManager::bind()
{
    // Generate the unique names for this user.
    generatePipeName();

    // Try to create our unique named mutex. If this fails, there must be another instance running,
    // se we'll just shutdown right now.
    if (DosCreateMutexSem(userMutexName, &hMutex, DC_SEM_SHARED, TRUE) != 0)
    {
        errcode = CSERROR_CONNX_FAILED;
        return false;
    }

    errcode = CSERROR_OK;
    return true;
}


/**
 * Accept a connection from a client.
 *
 * @return True on success, otherwise false.
 */
ApiConnection *SysServerNamedPipeConnectionManager::acceptConnection()
{
    HFILE  hPipe;

    // We need to create a new named pipe instance for each inbound connection
    if (DosCreateNPipe(userPipeName, &hPipe,
                       NP_ACCESS_DUPLEX,
                       NP_WAIT | NP_TYPE_BYTE | NP_READMODE_BYTE | NP_UNLIMITED_INSTANCES,
                       2048, 2048, 500) != 0)
    {
        errcode = CSERROR_CONNX_FAILED;
        return NULL;
    }

    // Wait for the client to connect
    if (DosConnectNPipe(hPipe) != 0)
    {
        errcode = CSERROR_CONNX_FAILED;
        return NULL;
    }

    errcode = CSERROR_OK;
    // now make a connection object that can be used to read and write the client requests.
    return new SysNamedPipeConnection(hPipe);
}


/**
 * Close the connection to the host.
 *
 * @return True on success, otherwise false.
 */
bool SysServerNamedPipeConnectionManager::disconnect()
{
    if (hMutex != NULLHANDLE)
    {
        DosCloseMutexSem(hMutex);
        hMutex = NULLHANDLE;
        // this is only done when the server is shutting down prior
        // to termination. We don't really need to get rid of this, but
        // it is good practice
        free((void *)userPipeName);
        userPipeName = NULL;
        free((void *)userMutexName);
        userMutexName = NULL;
    }

    errcode = CSERROR_OK;
    return true;
}


/**
 * Generate a unique string to be used for interprocess
 * communications for this userid. Also generates the unique
 * mutex name used to ensure only one instance exists.
 *
 * @return A unique identifier used to create the named pipes.
 */
const char *SysServerNamedPipeConnectionManager::generatePipeName()
{
    // if we've already generated this, we're done
    if (userPipeName != NULL && userMutexName != NULL)
    {
        return userPipeName;
    }

    // a buffer for generating the name
    char pNameBuffer[CCHMAXPATH];
    // name of the user
    char userid[MAX_USERID_LENGTH];

    // get the userid from the process
    SysProcess::getUserID(userid);

    snprintf(pNameBuffer, sizeof(pNameBuffer), "\\PIPE\\ooRexx-%d.%d.%d-%s", ORX_VER, ORX_REL, ORX_MOD, userid);
    userPipeName = strdup(pNameBuffer);
    snprintf(pNameBuffer, sizeof(pNameBuffer), "\\SEM32\\ooRexx-%d.%d.%d-%s", ORX_VER, ORX_REL, ORX_MOD, userid);
    userMutexName = strdup(pNameBuffer);
    return userPipeName;
}
