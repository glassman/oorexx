/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2021 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#define  INCL_DOS
#define  OS2EMX_PLAIN_CHAR
#include <os2.h>
#include <stdio.h>
#include "SysLocalAPIManager.hpp"
#include "SysCSNamedPipeStream.hpp"
#include "SysProcess.hpp"


/**
 * Check if the rxapi daemon process is running and start it up if not.
 */
void SysLocalAPIManager::startServerProcess()
{
    const char *installLocation = SysProcess::getLibraryLocation();
    const char *apiExeName = "rxapi.exe";
    char fullExeName[CCHMAXPATH] = "";
    char failObject[CCHMAXPATH];
    char pgmArgs[CCHMAXPATH+2];
    RESULTCODES resCodes;
    FILESTATUS3L fs;

    if (installLocation)
    {
        strlcpy(fullExeName, installLocation, sizeof(fullExeName));
    }

    strlcat(fullExeName, apiExeName, sizeof(fullExeName));

    if ((DosQueryPathInfo(fullExeName, FIL_STANDARDL, &fs, sizeof(fs)) != 0) ||
        (fs.attrFile & FILE_DIRECTORY))
    {
        if (DosSearchPath(SEARCH_IGNORENETERRS | SEARCH_ENVIRONMENT, "PATH",
                          apiExeName, fullExeName, sizeof(fullExeName)) != 0)
        {
            throw new ServiceException(API_FAILURE, "Unable to start API server");
        }
    }

    // The first of these strings is the program name, and the second string
    // consists of the parameters for the program. The second ASCIIZ string is
    // followed by an additional byte of zeros.
    memset(pgmArgs, 0, sizeof(pgmArgs));
    strcpy(pgmArgs, fullExeName);

    if (DosExecPgm(failObject, sizeof(failObject), EXEC_BACKGROUND,
                   pgmArgs, NULL, &resCodes, fullExeName) != 0)
    {
        throw new ServiceException(API_FAILURE, "Unable to start API server");
    }
}


/**
 * Check to see if we've inherited a session queue from a calling process.  This shows
 * up as an environment variable value.
 *
 * @param sessionQueue
 *               The returned session queue handle, if it exists.
 *
 * @return true if the session queue is inherited, false if a new once needs to be created.
 */
bool SysLocalAPIManager::getActiveSessionQueue(QueueHandle &sessionQueue)
{
    PSZ envbuffer;

    if (DosScanEnv("RXQUEUESESSION", &envbuffer) == 0)
    {
        sscanf(envbuffer, "%p", (void **)&sessionQueue);
        return true;
    }
    return false;
}


/**
 * Set the active session queue as an environment variable.
 *
 * @param sessionQueue
 *               The session queue handle.
 */
void SysLocalAPIManager::setActiveSessionQueue(QueueHandle sessionQueue)
{
    char envbuffer[MAX_QUEUE_NAME_LENGTH+1];
    // and set this as an environment variable for programs we call
    sprintf(envbuffer, "%p", (void *)sessionQueue);
    SysProcess::setEnvironmentVariable("RXQUEUESESSION", envbuffer);
}


/**
 * Create a new connection instance of the appropiate type for
 * connection to the daemon process.
 *
 * @return A connection instance.
 */
ApiConnection *SysLocalAPIManager::newClientConnection()
{
    SysNamedPipeConnection *connection = new SysNamedPipeConnection();

    // open the pipe to the server
    if (!connection->connect(SysServerNamedPipeConnectionManager::generatePipeName()))
    {
        // don't leak memory!
        delete connection;
        throw new ServiceException(CONNECTION_FAILURE, "Failure connecting to rxapi server");
    }
    return connection;
}

/**
 * Returns location of the current library.
 */

RexxReturnCode RexxEntry RexxLibraryLocation(char* location, int size)
{
    char modulePath[CCHMAXPATH], *p;
    HMODULE hmod;
    ULONG ObjNum = 0, Offset = 0;
    APIRET rc;

    rc = DosQueryModFromEIP( &hmod, &ObjNum, sizeof(modulePath), modulePath, &Offset, (ULONG)(&RexxLibraryLocation));
    if (rc == 0)
    {
         rc = DosQueryModuleName(hmod, sizeof(modulePath), modulePath);
         if (rc == 0)
         {
              // Scan backwards to find the last directory delimiter
              if ((p = strrchr(modulePath, '\\')) != NULL)
              {
                   *++p = '\0';
              }
              strlcpy(location, modulePath, size);
              return rc;
         }

    }

    if (size)
    {
         *location = 0;
    }

    return rc;
}
