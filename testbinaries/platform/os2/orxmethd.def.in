; Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.
; Copyright (c) 2005-2021 Rexx Language Association. All rights reserved.
;
; This program and the accompanying materials are made available under
; the terms of the Common Public License v1.0 which accompanies this
; distribution. A copy is also available at the following address:
; https://www.oorexx.org/license.html
;
; Redistribution and use in source and binary forms, with or
; without modification, are permitted provided that the following
; conditions are met:
;
; Redistributions of source code must retain the above copyright
; notice, this list of conditions and the following disclaimer.
; Redistributions in binary form must reproduce the above copyright
; notice, this list of conditions and the following disclaimer in
; the documentation and/or other materials provided with the distribution.
;
; Neither the name of Rexx Language Association nor the names
; of its contributors may be used to endorse or promote products
; derived from this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
; OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
; OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
LIBRARY ORXMETHD INITINSTANCE TERMINSTANCE
DESCRIPTION '@#Rexx Language Association:@ORX_MAJOR@.@ORX_MINOR@.@ORX_BLD_LVL@#@##1## @BUILDTIME@        @HOSTNAME@::::::@@Open Object Rexx Test Library'
DATA MULTIPLE NONSHARED LOADONCALL
EXPORTS
    TestIsBuffer
    TestBufferInit
    TestBufferCSelf
    TestBufferObjectToCSelf
    TestGetBuffer
    TestZeroIntArgs
    TestOneIntArg
    TestTwoIntArgs
    TestThreeIntArgs
    TestFourIntArgs
    TestFiveIntArgs
    TestSixIntArgs
    TestSevenIntArgs
    TestEightIntArgs
    TestNineIntArgs
    TestTenIntArgs
    TestIntArg
    TestInt32Arg
    TestUint32Arg
    TestInt8Arg
    TestUint8Arg
    TestInt16Arg
    TestUint16Arg
    TestInt64Arg
    TestUint64Arg
    TestIntPtrArg
    TestUintPtrArg
    TestWholeNumberArg
    TestStringSizeArg
    TestPositiveWholeNumberArg
    TestNonnegativeWholeNumberArg
    TestSizeArg
    TestSSizeArg
    TestLogicalArg
    TestFloatArg
    TestDoubleArg
    TestCstringArg
    TestPointerValue
    TestPointerArg
    TestNullPointerValue
    TestPointerStringValue
    TestPointerStringArg
    TestNullPointerStringValue
    TestStemArg
    TestObjectArg
    TestStringArg
    TestArrayArg
    TestClassArg
    TestObjectToWholeNumber
    TestObjectToWholeNumberAlt
    TestObjectToStringSize
    TestObjectToStringSizeAlt
    TestObjectToInt64
    TestObjectToInt64Alt
    TestObjectToUnsignedInt64
    TestObjectToUnsignedInt64Alt
    TestObjectToInt32
    TestObjectToInt32Alt
    TestObjectToUnsignedInt32
    TestObjectToUnsignedInt32Alt
    TestObjectToIntptr
    TestObjectToIntptrAlt
    TestObjectToUintptr
    TestObjectToUintptrAlt
    TestObjectToLogical
    TestObjectToLogicalAlt
    TestObjectToDouble
    TestObjectToDoubleAlt
    TestWholeNumberToObject
    TestWholeNumberToObjectAlt
    TestStringSizeToObject
    TestStringSizeToObjectAlt
    TestInt64ToObject
    TestInt64ToObjectAlt
    TestUnsignedInt64ToObject
    TestUnsignedInt64ToObjectAlt
    TestInt32ToObject
    TestInt32ToObjectAlt
    TestUnsignedInt32ToObject
    TestUnsignedInt32ToObjectAlt
    TestIntptrToObject
    TestIntptrToObjectAlt
    TestUintptrToObject
    TestUintptrToObjectAlt
    TestLogicalToObject
    TestLogicalToObjectAlt
    TestDoubleToObject
    TestDoubleToObjectAlt
    TestObjectToValue
    TestOptionalIntArg
    TestOptionalInt32Arg
    TestOptionalUint32Arg
    TestOptionalInt8Arg
    TestOptionalUint8Arg
    TestOptionalInt16Arg
    TestOptionalUint16Arg
    TestOptionalInt64Arg
    TestOptionalUint64Arg
    TestOptionalIntPtrArg
    TestOptionalUintPtrArg
    TestOptionalWholeNumberArg
    TestOptionalPositiveWholeNumberArg
    TestOptionalNonnegativeWholeNumberArg
    TestOptionalStringSizeArg
    TestOptionalSizeArg
    TestOptionalSSizeArg
    TestOptionalLogicalArg
    TestOptionalFloatArg
    TestOptionalDoubleArg
    TestOptionalCstringArg
    TestOptionalPointerArg
    TestOptionalPointerStringArg
    TestOptionalStemArg
    TestOptionalObjectArg
    TestOptionalStringArg
    TestOptionalArrayArg
    TestOptionalClassArg
    TestSuperArg
    TestScopeArg
    TestNameArg
    TestArglistArg
    TestOSelfArg
    TestGetArguments
    TestGetArgument
    TestGetMessageName
    TestGetMethod
    TestGetSelf
    TestGetSuper
    TestGetScope
    TestSetObjectVariable
    TestGetObjectVariable
    TestGetObjectVariableOrNil
    TestDropObjectVariable
    TestForwardMessage
    TestFindContextClass
    TestNewStem
    TestSetStemElement
    TestGetStemElement
    TestDropStemElement
    TestSetStemArrayElement
    TestGetStemArrayElement
    TestDropStemArrayElement
    TestGetAllStemElements
    TestGetStemValue
    TestIsStem
    TestSupplierItem
    TestSupplierIndex
    TestSupplierAvailable
    TestSupplierNext
    TestNewSupplier
    TestArrayAt
    TestArrayPut
    TestArrayAppend
    TestArrayAppendString
    TestArraySize
    TestArrayItems
    TestArrayDimension
    TestNewArray
    TestArrayOfOne
    TestArrayOfTwo
    TestArrayOfThree
    TestArrayOfFour
    TestArrayOfOneAlt
    TestArrayOfTwoAlt
    TestArrayOfThreeAlt
    TestArrayOfFourAlt
    TestIsArray
    TestDirectoryPut
    TestDirectoryAt
    TestDirectoryRemove
    TestNewDirectory
    TestIsDirectory
    TestStringTablePut
    TestStringTableAt
    TestStringTableRemove
    TestNewStringTable
    TestIsStringTable
    TestSendMessage
    TestSendMessageScoped
    TestSendMessage0
    TestSendMessage1
    TestSendMessage2
    TestIsInstanceOf
    TestHasMethod
    TestLoadPackage
    TestLoadPackageFromData
    TestFindClass
    TestFindPackageClass
    TestGetPackageRoutines
    TestGetPackagePublicRoutines
    TestGetPackageClasses
    TestGetPackagePublicClasses
    TestGetPackageMethods
    TestCallRoutine
    TestCallProgram
    TestNewMethod
    TestNewRoutine
    TestIsMethod
    TestIsRoutine
    TestGetMethodPackage
    TestGetRoutinePackage
    TestObjectToString
    TestStringGet
    TestStringLength
    TestStringData
    TestStringUpper
    TestStringLower
    TestIsString
    TestRaiseException0
    TestRaiseException1
    TestRaiseException2
    TestRaiseException
    TestRaiseCondition
    TestInterpreterVersion
    TestLanguageLevel
    TestNewStringFromAsciiz
    TestNewStringFromAsciizAlt
    TestNewString
    TestNewStringAlt
    TestCStringToObject
    TestObjectToCString
    TestNewMutableBuffer
    TestIsMutableBuffer
    TestMutableBufferLength
    TestSetMutableBufferLength
    TestMutableBufferCapacity
    TestSetMutableBufferCapacity
    TestSetMutableBufferValue
    TestGetMutableBufferValue
    TestAllocateObjectMemory
    TestFreeObjectMemory
    TestReallocateObjectMemory
    TestSetObjectMemory
    TestGetObjectMemory
    TestGetObjectVariableReference
    TestIsVariableReference
    TestSetVariableReferenceValue
    TestVariableReferenceName
    TestVariableReferenceValue
    TestThrowException0
    TestThrowException1
    TestThrowException2
    TestThrowException
    TestThrowCondition
