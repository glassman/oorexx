/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2022 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/* REXX Kernel                                              SysFileSystem.cpp */
/*                                                                            */
/* OS/2 implementation of the SysFileSystem class.                            */
/*                                                                            */
/******************************************************************************/

#include "RexxCore.h"
#include "RexxDateTime.hpp"
#include "SysFileSystem.hpp"
#include "ActivityManager.hpp"
#include "FileNameBuffer.hpp"

const char  SysFileSystem::EOF_Marker = 0x1A;    // the end-of-file marker
const char *SysFileSystem::EOL_Marker = "\r\n";  // the end-of-line marker
const char  SysFileSystem::PathDelimiter = '\\'; // directory path delimiter
const char  SysFileSystem::NewLine = '\n';
const char  SysFileSystem::CarriageReturn = '\r';

const int64_t NoTimeStamp = -999999999999999999; // invalid file time

/**
 * Search for a given filename, returning the fully
 * resolved name if it is found.
 *
 * @param name     The original name.
 * @param fullName A pointer to the buffer where the resolved name is returned.
 *
 * @return True if the file was found, false otherwise.
 */
bool SysFileSystem::searchFileName(const char *name, FileNameBuffer &fullName)
{
    CHAR szFilename[CCHMAXPATH];
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_QUERYFULLNAME, szFilename, sizeof(szFilename));
    if (rc == 0 && fileExists(szFilename))
    {
      fullName = szFilename;
      return true;
    }
    rc = DosSearchPath(SEARCH_IGNORENETERRS | SEARCH_ENVIRONMENT, "PATH",
                       name, (PBYTE)szFilename, sizeof(szFilename));
    if (rc == 0)
    {
      fullName = szFilename;
      return true;
    }
    return false;
}


/**
 * Get the fully qualified name for a file.
 *
 * @param name     The input file name.
 * @param fullName The return full file name
 */
void SysFileSystem::qualifyStreamName(const char *name, FileNameBuffer &fullName)
{
    // if already expanded, then do nothing
    if (fullName.length() != 0)
    {
        return;
    }

    CHAR szFilename[CCHMAXPATH];
    CHAR* pszFindName = strdup(name);
    size_t len = strlen(name);

    if (pszFindName)
    {
        if ((len != 3 || pszFindName[1] != ':' || !isalpha(pszFindName[0])) &&
            (pszFindName[len-1] == '\\' || pszFindName[len-1] == '/'))
        {
            pszFindName[len-1] = 0;
        }

        APIRET rc = DosQueryPathInfo(pszFindName, FIL_QUERYFULLNAME, szFilename, sizeof(szFilename));
        free(pszFindName);

        if (rc == 0)
        {
            fullName = szFilename;
            return;
        }
    }

    // reset to a null string for any failures
    fullName = "";
}


/**
 * Test if a given file exists.
 *
 * @param fname  The target file name.
 *
 * @return true if the file exists, false otherwise.
 */
bool SysFileSystem::fileExists(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc == 0)
    {
        if (!(fs.attrFile & FILE_DIRECTORY))
        {
            return true;
        }
    }
    return false;
}


/**
 * Do a search for a single variation of a filename.
 *
 * @param name      The name to search for.
 * @param path      The path to search over (can be NULL)
 * @param extension A potential extension to add to the file name (can be NULL).
 * @param resolvedName
 *                  The buffer used to return the resolved file name.
 *
 * @return true if the file was located.  A true returns indicates the
 *         resolved file name has been placed in the provided buffer.
 */
bool SysFileSystem::searchName(const char *name, const char *path, const char *extension, FileNameBuffer &resolvedName)
{
    return primitiveSearchName(name, path, extension, resolvedName);
}


/**
 * Do a search for a single variation of a filename.
 *
 * NOTE:  This version does not do anything with the
 * kernel lock, so it is callable before the first activity
 * is set up.
 *
 * @param name      The name to search for.
 * @param path      The path to be searched on
 * @param extension A potential extension to add to the file name (can be NULL).
 * @param resolvedName
 *                  The buffer used to return the resolved file name.
 *
 * @return true if the file was located.  A true returns indicates the
 *         resolved file name has been placed in the provided buffer.
 */
bool SysFileSystem::primitiveSearchName(const char *name, const char *path, const char *extension, FileNameBuffer &resolvedName)
{
    CHAR szFilename[CCHMAXPATH];
    FileNameBuffer fullName;
    APIRET rc;

    fullName = name;
    if (extension != NULL)
    {
        fullName += extension;
    }

    // If this appears to be a fully qualified name, then check it as-is and
    // quit. The path searches might give incorrect results if performed with such
    // a name and this should only check on the raw name.
    if (hasDirectory(fullName))
    {
        // check the file as is first
        return checkCurrentFile(fullName, resolvedName);
    }

    rc = DosSearchPath(SEARCH_IGNORENETERRS, path,
                       fullName, (PBYTE)szFilename, sizeof(szFilename));
    if (rc == 0)
    {
        resolvedName = szFilename;
        return true;
    }
    return false;
}


/**
 * Do a path search for a file.
 *
 * @param name   The name to search for.
 * @param path   The search path to use.
 * @param resolvedName
 *               A buffer used for returning the resolved name.
 *
 * @return Returns true if the file was located.  If true, the resolvedName
 *         buffer will contain the returned name.
 */
bool SysFileSystem::searchPath(const char *name, const char *path, FileNameBuffer &resolvedName)
{
    return searchName(name, path, NULL, resolvedName);
}


/**
 * Try to locate a file using just the raw name passed in, as
 * opposed to searching along a path for the name.
 *
 * @param name   The name to use for the search.
 * @param resolvedName
 *               The buffer for the returned name.
 *
 * @return true if the file was located, false otherwise.
 */
bool SysFileSystem::checkCurrentFile(const char *name, FileNameBuffer &resolvedName)
{
    CHAR szFilename[CCHMAXPATH];
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_QUERYFULLNAME, szFilename, sizeof(szFilename));
    if (rc == 0 && fileExists(szFilename))
    {
      resolvedName = szFilename;
      return true;
    }

    // not found
    resolvedName = "";
    return false;
}


/**
 * Test if a filename has an extension.
 *
 * @param name   The name to check.
 *
 * @return true if an extension was found on the file, false if there
 *         is no extension.
 */
bool SysFileSystem::hasExtension(const char *name)
{
    const char *endPtr = name + strlen(name) - 1;

    // scan backwards looking for a directory delimiter.  This name should
    // be fully qualified, so we don't have to deal with drive letters
    while (name < endPtr)
    {
        // find the first directory element?
        if (*endPtr == '\\')
        {
            return false;        // found a directory portion before an extension...we're extensionless
        }
        // is this the extension dot?
        else if (*endPtr == '.')
        {
            // return everything from the period on.  Keeping the period on is a convenience.
            return true;
        }
        endPtr--;
    }
    return false;          // not available
}


/**
 * Test if a filename has a directory portion
 *
 * @param name   The name to check.
 *
 * @return true if a directory was found on the file, false if
 *         there is no directory.
 */
bool SysFileSystem::hasDirectory(const char *name)
{
    // hasDirectory() means we have enough absolute directory
    // information at the beginning to bypass performing path searches.
    // We really only need to look at the first character.
    return name[0] == '\\' || name[1] == ':'   ||
          (name[0] == '.'  && name[1] == '\\') ||
          (name[0] == '.'  && name[1] == '.' && name[2] == '\\');
}


/**
 * Extract directory information from a file name.
 *
 * @param file   The input file name.  If this represents a real source file,
 *               this will be fully resolved.
 *
 * @return The directory portion of the file name.  If the file name
 *         does not include a directory portion, then OREF_NULL is returned.
 */
RexxString *SysFileSystem::extractDirectory(RexxString *file)
{
    const char *pathName = file->getStringData();
    const char *endPtr = pathName + file->getLength() - 1;

    // scan backwards looking for a directory delimiter.  This name should
    // be fully qualified, so we don't have to deal with drive letters
    while (pathName < endPtr)
    {
        // find the first directory element?
        if (*endPtr == '\\')
        {
            // extract the directory information, including the final delimiter
            // and return as a string object.
            return new_string(pathName, endPtr - pathName + 1);
        }
        endPtr--;
    }
    return OREF_NULL;      // not available
}


/**
 * Extract extension information from a file name.
 *
 * @param file   The input file name.  If this represents a real source file,
 *               this will be fully resolved.
 *
 * @return The extension portion of the file name.  If the file
 *         name does not include an extension portion, then
 *         OREF_NULL is returned.
 */
RexxString *SysFileSystem::extractExtension(RexxString *file)
{
    const char *pathName = file->getStringData();
    const char *endPtr = pathName + file->getLength() - 1;

    // scan backwards looking for a directory delimiter.  This name should
    // be fully qualified, so we don't have to deal with drive letters
    while (pathName < endPtr)
    {
        // find the first directory element?
        if (*endPtr == '\\')
        {
            return OREF_NULL;    // found a directory portion before an extension...we're extensionless
        }
        // is this the extension dot?
        else if (*endPtr == '.')
        {
            // return everything from the period on.  Keeping the period on is a convenience.
            return new_string(endPtr);
        }
        endPtr--;
    }
    return OREF_NULL;      // not available
}


/**
 * Extract file information from a file name.
 *
 * @param file   The input file name.  If this represents a real source file,
 *               this will be fully resolved.
 *
 * @return The file portion of the file name.  If the file name
 *         does not include a directory portion, then the entire
 *         string is returned
 */
RexxString *SysFileSystem::extractFile(RexxString *file)
{
    const char *pathName = file->getStringData();
    const char *endPtr = pathName + file->getLength() - 1;

    // scan backwards looking for a directory delimiter.  This name should
    // be fully qualified, so we don't have to deal with drive letters
    while (pathName < endPtr)
    {
        // find the first directory element?
        if (*endPtr == '\\')
        {
            // extract the directory information, including the final delimiter
            // and return as a string object.
            return new_string(endPtr);
        }
        endPtr--;
    }
    return file;     // this is all filename
}


/**
 * Delete a file from the file system.
 *
 * @param name   The fully qualified name of the file.
 *
 * @return The return code from the delete operation.
 */
int SysFileSystem::deleteFile(const char *name)
{
    return DosDelete(name);
}


/**
 * Delete a directory from the file system.
 *
 * @param name   The name of the target directory.
 *
 * @return The return code from the delete operation.
 */
int SysFileSystem::deleteDirectory(const char *name)
{
    return DosDeleteDir(name);
}


/**
 * Test if a given file name is for a directory.
 *
 * @param name   The target name.
 *
 * @return true if the file is a directory, false for any other
 *         type of entity.
 */
bool SysFileSystem::isDirectory(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc == 0)
    {
        if (fs.attrFile & FILE_DIRECTORY)
        {
            return true;
        }
    }
    return false;
}


/**
 * Test is a file is readable .
 *
 * @param name   The target file name.
 *
 * @return true if the file can be read.
 */
bool SysFileSystem::canRead(const char *name)
{
    APIRET rc;
    HFILE  hFile;
    ULONG  ulAction;

    rc = DosOpen(name, &hFile, &ulAction, 0, 0,
                 OPEN_ACTION_FAIL_IF_NEW | OPEN_ACTION_OPEN_IF_EXISTS,
                 OPEN_SHARE_DENYNONE | OPEN_ACCESS_READONLY, NULL);
    if (rc == 0)
    {
        DosClose(hFile);
        return true;
    }

    return false;
}


/**
 * Test if the process can write to the file. This is not the
 * same as being read only
 *
 * @param name   The target file name.
 *
 * @return true if the file is writeable.
 */
bool SysFileSystem::canWrite(const char *name)
{
    APIRET rc;
    HFILE  hFile;
    ULONG  ulAction;

    rc = DosOpen(name, &hFile, &ulAction, 0, 0,
                 OPEN_ACTION_FAIL_IF_NEW | OPEN_ACTION_OPEN_IF_EXISTS,
                 OPEN_SHARE_DENYNONE | OPEN_ACCESS_WRITEONLY, NULL);
    if (rc == 0)
    {
        DosClose(hFile);
        return true;
    }

    return false;
}


/**
 * Test is a file is read only.
 *
 * @param name   The target file name.
 *
 * @return true if the file is marked as read-only.
 */
bool SysFileSystem::isReadOnly(const char *name)
{
    return !canWrite(name) && canRead(name);
}


/**
 * Test if a file is marked as write-only.
 *
 * @param name   The target file name.
 *
 * @return true if the file is only writeable.  false if read
 *         operations are permitted.
 */
bool SysFileSystem::isWriteOnly(const char *name)
{
    return !canRead(name) && canWrite(name);
}


/**
 * Test if a give file name is for a real file (not
 * a directory).
 *
 * @param name   The target file name.
 *
 * @return true if the file is a real file, false if some other
 *         filesystem entity.
 */
bool SysFileSystem::isFile(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc == 0)
    {
        if (!(fs.attrFile & FILE_DIRECTORY))
        {
            return true;
        }
    }
    return false;
}


/**
 * Determine if the given file is a symbolic link to another file.
 *
 * @param name   The file name.
 *
 * @return True if the file is considered a link, false if this is the real
 *         file object.
 */
bool SysFileSystem::isLink(const char *name)
{
    return false;
}


/**
 * Test if a file or directory exists using a fully qualified name.
 *
 * @param name   The target file or directory name.
 *
 * @return True if the file or directory exists, false if it is unknown.
 */
bool SysFileSystem::exists(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc == 0)
    {
        return true;
    }
    return false;
}


/**
 * Get the last modified file date as a file time value.
 *
 * @param name   The target name.
 *
 * @return the file time value for the modified date, or -999999999999999999
 *         for any errors.
 */
int64_t SysFileSystem::getLastModifiedDate(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc == 0)
    {
        RexxDateTime time(fs.fdateLastWrite.year + 1980, fs.fdateLastWrite.month,  fs.fdateLastWrite.day,
                          fs.ftimeLastWrite.hours, fs.ftimeLastWrite.minutes, fs.ftimeLastWrite.twosecs * 2, 0);
        return time.getBaseTime();
    }

    return NoTimeStamp;
}


/**
 * Get the file last access data as a file time value.
 *
 * @param name   The target name.
 *
 * @return the file time value for the last access date, or -999999999999999999
 *         for any errors.
 */
int64_t SysFileSystem::getLastAccessDate(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc == 0)
    {
        RexxDateTime time(fs.fdateLastAccess.year + 1980, fs.fdateLastAccess.month,  fs.fdateLastAccess.day,
                          fs.ftimeLastAccess.hours, fs.ftimeLastAccess.minutes, fs.ftimeLastAccess.twosecs * 2, 0);
        return time.getBaseTime();
    }

    return NoTimeStamp;
}


/**
 * Set the last modified date for a file.
 *
 * @param name   The target name.
 * @param time   The new file time.
 *
 * @return true if the filedate was set correctly, false otherwise.
 */
bool SysFileSystem::setLastModifiedDate(const char *name, int64_t time)
{
    FILESTATUS3L fs;
    RexxDateTime ft(time);
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc != 0)
    {
        return false;
    }

    fs.fdateLastWrite.year = ft.year - 1980;
    fs.fdateLastWrite.month = ft.month;
    fs.fdateLastWrite.day = ft.day;
    fs.ftimeLastWrite.hours = ft.hours;
    fs.ftimeLastWrite.minutes = ft.minutes;
    fs.ftimeLastWrite.twosecs = ft.seconds / 2;

    rc = DosSetPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs), 0);
    if (rc == 0)
    {
        return true;
    }

    return false;
}


/**
 * Set the last access date for a file.
 *
 * @param name   The target name.
 * @param time   The new file time.
 *
 * @return true if the filedate was set correctly, false otherwise.
 */
bool SysFileSystem::setLastAccessDate(const char *name, int64_t time)
{
    FILESTATUS3L fs;
    RexxDateTime ft(time);
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc != 0)
    {
        return false;
    }

    fs.fdateLastAccess.year = ft.year - 1980;
    fs.fdateLastAccess.month = ft.month;
    fs.fdateLastAccess.day = ft.day;
    fs.ftimeLastAccess.hours = ft.hours;
    fs.ftimeLastAccess.minutes = ft.minutes;
    fs.ftimeLastAccess.twosecs = ft.seconds / 2;

    rc = DosSetPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs), 0);
    if (rc == 0)
    {
        return true;
    }

    return false;
}


/**
 * Retrieve the size of a file.
 *
 * @param name   The name of the target file.
 *
 * @return the 64-bit file size.
 */
uint64_t SysFileSystem::getFileLength(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc == 0)
    {
        return fs.cbFile;
    }

    return 0;
}


/**
 * Create a directory in the file system.
 *
 * @param name   The target name.
 *
 * @return The success/failure flag.
 */
bool SysFileSystem::makeDirectory(const char *name)
{
    return DosCreateDir(name, 0) == 0;
}


/**
 * Test if a given file or directory is hidden.
 *
 * @param name   The target name.
 *
 * @return true if the file or directory is hidden, false otherwise.
 */
bool SysFileSystem::isHidden(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc == 0)
    {
        if (fs.attrFile & FILE_HIDDEN)
        {
            return true;
        }
    }
    return false;
}


/**
 * Set the read-only attribute on a file or directory.
 *
 * @param name   The target name.
 *
 * @return true if the attribute was set, false otherwise.
 */
bool SysFileSystem::setFileReadOnly(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc != 0)
    {
        return false;
    }

    fs.attrFile |= FILE_READONLY;

    rc = DosSetPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs), 0);
    if (rc == 0)
    {
        return true;
    }

    return false;
}


/**
 * Set the write attribute on a file or directory.
 *
 * @param name   The target name.
 *
 * @return true if the attribute was set, false otherwise.
 */
bool SysFileSystem::setFileWritable(const char *name)
{
    FILESTATUS3L fs;
    APIRET rc;

    rc = DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs));
    if (rc != 0)
    {
        return false;
    }

    fs.attrFile &= ~FILE_READONLY;

    rc = DosSetPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs), 0);
    if (rc == 0)
    {
        return true;
    }

    return false;
}


/**
 * indicate whether the file system is case sensitive.
 *
 * @return For OS/2 always returns false.
 */
bool SysFileSystem::isCaseSensitive()
{
    return false;
}


/**
 * test if an individual file is a case sensitive name
 *
 * @return For OS/2 always returns false.
 */
bool SysFileSystem::isCaseSensitive(const char *name)
{
    return false;
}


/**
 * Retrieve the file system root elements. On OS/2,
 * each of the drives is a root element.
 *
 * @return The number of roots located.
 */
int SysFileSystem::getRoots(FileNameBuffer &roots)
{
    ULONG disknum;
    ULONG logical;
    APIRET rc;
    int length = 0, i;

    rc = DosQueryCurrentDisk(&disknum, &logical);
    if (rc == 0) {
        for (i = 0; i < 32; i++) {
            if (logical & (1<<i)) {
                ++length;
            }
        }
        roots.ensureCapacity(length * 4 + 1);
        char* p = roots;
        for (i = 0; i < 32; i++) {
            if (logical & (1<<i))
            {
                p[0] = 'A' + i;
                p[1] = ':';
                p[2] = '\\';
                p[3] = 0;
                p += 4;
            }
        }
        p[0] = 0;
    }
    return length;
}


/**
 * Return the separator used for separating path names.
 *
 * @return The ASCII-Z version of the path separator.
 */
const char *SysFileSystem::getSeparator()
{
    return "\\";
}


/**
 * Return the separator used for separating search path elements
 *
 * @return The ASCII-Z version of the path separator.
 */
const char *SysFileSystem::getPathSeparator()
{
    return ";";
}


/**
 * Return the string used as a line-end separator.
 *
 * @return The ASCII-Z version of the line end sequence.
 */
const char *SysFileSystem::getLineEnd()
{
    return "\r\n";
}


/**
 * Retrieve the current working directory into a FileNameBuffer.
 *
 * @param directory The return directory name.
 *
 * @return true if this was retrieved, false otherwise.
 */
bool SysFileSystem::getCurrentDirectory(FileNameBuffer &directory)
{
    ULONG disknum;
    ULONG logical;
    CHAR  szPathname[CCHMAXPATH];
    ULONG size = CCHMAXPATH - 3;
    APIRET rc;

    rc = DosQueryCurrentDisk(&disknum, &logical);
    if (rc == 0)
    {
        szPathname[0] = 'A' + disknum - 1;
        szPathname[1] = ':';
        szPathname[2] = '\\';
        rc = DosQueryCurrentDir(0, (PBYTE)(szPathname + 3), &size);
        if (rc == 0)
        {
            directory = szPathname;
            return true;
        }
    }

    directory = "";
    return false;
}


/**
 * Set the current working directory
 *
 * @param directory The new directory set.
 *
 * @return True if this worked, false for any failures
 */
bool SysFileSystem::setCurrentDirectory(const char *directory)
{
    APIRET rc;
    const char* dir = directory;

    if (dir[1] == ':') {
        if (DosSetDefaultDisk(toupper(dir[0])-'A'+1) != 0) {
            return false;
        }
        dir += 2;
    }
    return DosSetCurrentDir(dir) == 0;
}


/**
 * Implementation of a copy file operation.
 *
 * @param fromFile The from file of the copy operation
 * @param toFile   The to file of the copy operation.
 *
 * @return Any error codes.
 */
int SysFileSystem::copyFile(const char *fromFile, const char *toFile)
{
    return DosCopy(fromFile, toFile, 0);
}


/**
 * Move a file from one location to another. This is typically just a rename, but if necessary, the file will be copied and the original unlinked.
 *
 * @param fromFile The file we're copying from.
 * @param toFile   The target file.
 *
 * @return 0 if this was successful, otherwise the system error code.
 */
int SysFileSystem::moveFile(const char *fromFile, const char *toFile)
{
    return DosMove(fromFile, toFile);
}


/**
 * Locate the last directory delimiter in a file spec name
 *
 * @param name   The give name.
 *
 * @return The location of the last directory delimiter, or NULL if one is
 *         not found.
 */
const char* SysFileSystem::getPathEnd(const char *name)
{
    // find the last backslash in name}
    return strrchr(name, PathDelimiter);
}


/**
 * Locate the first directory delimiter in a file spec name
 *
 * @param name   The give name.
 *
 * @return The location of the first directory delimiter, or NULL if one is
 *         not found.
 */
const char* SysFileSystem::getPathStart(const char *name)
{
    // we look for the first colon, because this could also be a device specification
    const char *driveEnd = strchr(name, ':');

    return driveEnd == NULL ? name : driveEnd + 1;
}


/**
 * Retrieve the location where temporary files should be stored.
 * This will be the value of environment variable TMPDIR, if defined, or /tmp.
 *
 * @return true if successful, false otherwise.
 */
bool SysFileSystem::getTemporaryPath(FileNameBuffer &temporary)
{
    PSZ value;

    if (DosScanEnv("TEMP", &value) == 0)
    {
        temporary = value;
        return true;
    }

    // make sure this is a null string
    temporary = "";
    return false;
}


/**
 * Create a new SysFileIterator instance.
 *
 * @param path    The path we're going to be searching in
 * @param pattern The pattern to use. If NULL, then everything in the path will be returned.
 * @param buffer  A FileNameBuffer object used to construct the path name.
 * @param c       the caseless flag (ignored for OS/2)
 */
SysFileIterator::SysFileIterator(const char *path, const char *pattern, FileNameBuffer &buffer, bool c)
{
    ULONG findCount = 1;

    buffer = path;
    buffer.addFinalPathDelimiter();
    buffer += (pattern == NULL) ? "*" : pattern;
    hdir = HDIR_CREATE;
    completed = true;

    if (DosFindFirst(buffer, &hdir, FILE_DIRECTORY | FILE_HIDDEN | FILE_SYSTEM |
                     FILE_READONLY | FILE_ARCHIVED, &findFileData, sizeof(findFileData),
                     &findCount, FIL_STANDARDL) == 0)
    {
        completed = false;
    }
}


/**
 * Destructor for the iteration operation.
 */
SysFileIterator::~SysFileIterator()
{
    close();
}


/**
 * close the iterator.
 */
void SysFileIterator::close()
{
    if (hdir != HDIR_CREATE)
    {
        DosFindClose(hdir);
        hdir = HDIR_CREATE;
    }
}


/**
 * Check if the iterator has new results it can return.
 *
 * @return true if the iterator has another value to return, false if
 *         the iteration is complete.
 */
bool SysFileIterator::hasNext()
{
    return !completed;
}


/**
 * Retrieve the next iteration value.
 *
 * @param buffer     The buffer used to return the value.
 * @param attributes The returned system-dependent attributes of the next file.
 */
void SysFileIterator::next(FileNameBuffer &buffer, SysFileIterator::FileAttributes &attributes)
{
    if (completed)
    {
        buffer = "";
    }
    else
    {
        buffer = findFileData.achName;
        attributes.fs = findFileData;
    }

    // find the next entry
    ULONG findCount = 1;
    if (DosFindNext(hdir, &findFileData, sizeof(findFileData), &findCount) != 0)
    {
        completed = true;
    }
}
