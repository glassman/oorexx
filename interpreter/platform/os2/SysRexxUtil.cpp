/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2020 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/*                                                                            */
/* OS/2-specific RexxUtil functions                                           */
/*                                                                            */
/******************************************************************************/
/******************************************************************************
*                                                                             *
*   This program extends the REXX language by providing many REXX external    *
*   functions.                                                                *
*   These functions are:                                                      *
*       SysSetFileHandle    -- Sets the maximum number of file handles        *
*                              available to the current process.              *
*       SysAddFileHandle    -- Increases the number of file handles available *
*                              to the current process.                        *
*       SysBootDrive        -- Return the windows boot drive.                 *
*       SysCls              -- Clear the screen in an OS/2 fullscreen or      *
*                              windowed command prompt session.               *
*       SysCurPos           -- Set and/or Query the cursor position in an     *
*                              OS/2 fullscreen or windowed command prompt     *
*                              session.                                       *
*       SysCurState         -- Make the cursor visible or invisible in an     *
*                              OS/2 fullscreen or windowed command prompt     *
*                              session.                                       *
*       SysGetKey           -- Returns one by of data indicating the key      *
*                              which was pressed, in a command prompt session *
*       SysTextScreenRead   -- Reads characters from the screen, in a command *
*                              prompt session command prompt session.         *
*       SysTextScreenSize   -- Returns the size of the window in rows and     *
*                              columns, in a command prompt session.          *
*       SysCreateEventSem   -- Create an Event semaphore.                     *
*       SysOpenEventSem     -- Open an Event semaphore.                       *
*       SysPostEventSem     -- Post an Event semaphore.                       *
*       SysResetEventSem    -- Reset an Event semaphore.                      *
*       SysPulseEventSem    -- Post and reset an Event semaphore.             *
*       SysWaitEventSem     -- Wait on an Event semaphore.                    *
*       SysCloseEventSem    -- Close an Event semaphore.                      *
*       SysCreateMutexSem   -- Create a Mutex semaphore.                      *
*       SysOpenMutexSem     -- Open a Mutex semaphore.                        *
*       SysRequestMutexSem  -- Request a Mutex semaphore.                     *
*       SysReleaseMutexSem  -- Release a Mutex semaphore.                     *
*       SysCloseMutexSem    -- Close a Mutex semaphore.                       *
*       SysDriveInfo        -- Returns information about a specific drive.    *
*       SysDriveMap         -- Returns a list of the drives on the machine.   *
*       SysElapsedTime      -- Reports, and optionally resets, the number of  *
*                              seconds and microseconds on the                *
*                              high-resolution elapsed time clock.            *
*       SysGetCollate       -- etrieves the collating sequence table for the  *
*                              specified country and codepage combination.    *
*       SysQueryEAList      -- Retrieves a complete list of the names of a    *
*                              file's extended attributes.                    *
*       SysGetEA            -- Retrieves the value of the specified extended  *
*                              attribute value from a file.                   *
*       SysPutEA            -- Writes an extended attribute.                  *
*       SysGetFileDateTime  -- Get the last modified date of a file.          *
*       SysSetFileDateTime  -- Set the last modified date of a file.          *
*       SysGetMessage       -- Retrieves the specified text string from an    *
*                              OS/2 message file.                             *
*       SysIni              -- Reads and/or updates entries in .INI files.    *
*       SysMapCase          -- Converts a string to uppercase.                *
*       SysMkDir            -- Creates a directory.                           *
*       SysNationalLanguageCompare -- Compares two strings for equality.      *
*       SysOS2Ver           -- Returns the OS/2 version.                      *
*       SysVersion          -- Returns the OS and Version number.             *
*       SysProcessType      -- Return type of process.                        *
*       SysQueryExtLIBPATH  -- Queries the current value of one of the        *
*                              extended LIBPATHs.                             *
*       SysSetExtLIBPATH    -- Set the value of one of the extended LIBPATHs. *
*       SysQueryProcessCodePage -- Queries the codepage of the process in     *
*                              which the REXX program is running.             *
*       SysSetProcessCodePage -- Sets the codepage for the current process.   *
*       SysQuerySwitchList  -- Retrieves the current contents of the          *
*                              Presentation Manager window list.              *
*       SysSetPriority      -- Set current thread priority.                   *
*       SysShutDownSystem   -- Shutdown the system.                           *
*       SysSwitchSession    -- Switch to a named session.                     *
*       SysWaitForShell     -- Checks for the specified WPS initialization    *
*                              event, and optionally blocks until that event  *
*                              has occurred.                                  *
*       SysWaitNamedPipe    -- Wait on a named pipe.                          *
*       SysWildCard         -- Transforms a filename string using the         *
*                              specified wildcard pattern.                    *
*       SysCreateObject     -- Creates a new instance of a WPS class.         *
*       SysCreateShadow     -- Creates a shadow of an object.                 *
*       SysCopyObject       -- Creates a copy of an existing WPS object.      *
*       SysMoveObject       -- Moves a WPS object to the specified folder.    *
*       SysDestroyObject    -- Destroys an existing Workplace Shell object.   *
*       SysOpenObject       -- Opens an existing WPS object according to the  *
*                              specified view.                                *
*       SysSaveObject       -- Instructs the (WPS to save the object's state. *
*       SysSetIcon          -- Sets the specified file's icon.                *
*       SysSetObjectData    -- Changes the settings of the WPS object.        *
*       SysQueryClassList   -- Retrieves list of all registered WPS classes.  *
*       SysRegisterObjectClass -- Registers a new object class with the WPS.  *
*       SysDeregisterObjectClass -- Deregisters an object class from the WPS. *
*       SysSystemDirectory  -- Return the Windows system directory.           *
*       SysFileSystemType   -- Return drive file system type.                 *
*       SysVolumeLabel      -- Return the drive label.                        *
******************************************************************************/

#define  OS2EMX_PLAIN_CHAR
#define  INCL_DOS
#define  INCL_VIO
#define  INCL_KBD
#define  INCL_LONGLONG
#define  INCL_ERRORS
#define  INCL_WIN
#define  INCL_SHLERRORS
#define  INCL_WINWORKPLACE
#include <os2.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>
#include <stdlib.h>
#include "oorexxapi.h"
#include "RexxUtilCommon.hpp"
#include "Utilities.hpp"

extern "C" {
    /* Allow applications to wait for the Shell to be available */
    #define WWFS_QUERY            0x80000000
    #define WWFS_DESKTOPCREATED   1
    #define WWFS_DESKTOPOPENED    2
    #define WWFS_DESKTOPPOPULATED 3
    BOOL APIENTRY WinWaitForShell(ULONG ulEvent);
}

#define OPEN_DEFAULT    0
#define OPEN_CONTENTS   1
#define OPEN_SETTINGS   2
#define OPEN_HELP       3
#define OPEN_RUNNING    4
#define OPEN_PROMPTDLG  5
#define OPEN_TREE       101
#define OPEN_DETAILS    102
#define OPEN_PALETTE    121


/******************************************************************************
* Gets file information for a file or subdirectory.                           *
******************************************************************************/
static APIRET QueryPathInfo(PCSZ pszPathName, ULONG ulInfoLevel, PVOID pInfoBuf, ULONG cbInfoBuf)
{
    APIRET rc =
        DosQueryPathInfo(pszPathName, ulInfoLevel, pInfoBuf, cbInfoBuf);

    // Because the DosQueryPathInfo try to open a file object for read access,
    // with a deny-write sharing mode, the ERROR_SHARING_VIOLATION can be occured.
    // In this case try to open file manually without a deny-write sharing mode.

    if (rc == ERROR_SHARING_VIOLATION)
    {
        HFILE hFile;
        ULONG ulAction;

        if (DosOpen(pszPathName, &hFile, &ulAction, 0, 0,
                    OPEN_ACTION_FAIL_IF_NEW  | OPEN_ACTION_OPEN_IF_EXISTS,
                    OPEN_FLAGS_FAIL_ON_ERROR | OPEN_SHARE_DENYNONE | OPEN_ACCESS_READONLY,
                    NULL) == 0)
        {
            rc = DosQueryFileInfo(hFile, ulInfoLevel, pInfoBuf, cbInfoBuf);
            DosClose(hFile);
        }
    }

    return rc;
}


/******************************************************************************
* Function:  SysSetFileHandle                                                 *
*                                                                             *
* Syntax:    result = SysSetFileHandle(number)                                *
*                                                                             *
* Params:    number - The total number of file handles to be made available.  *
*                                                                             *
* Return:    The DosSetMaxFH API return code. Possible values are:            *
*             0 Success.                                                      *
*             8 Not enough memory.                                            *
*            87 Invalid parameter.                                            *
******************************************************************************/
RexxRoutine1(uint32_t, SysSetFileHandle, uint32_t, number)
{
     return DosSetMaxFH(number);
}


/******************************************************************************
* Function:  SysAddFileHandle                                                 *
*                                                                             *
* Syntax:    result = SysSetFileHandle(number)                                *
*                                                                             *
* Params:    number - Number of additional file handles to make available.    *
*                                                                             *
* Return:    Number of file handles now available to the process.             *
******************************************************************************/
RexxRoutine1(uint32_t, SysAddFileHandle, int32_t, number)
{
     ULONG CurMaxFH = 0;
     DosSetRelMaxFH((PLONG)&number, &CurMaxFH);
     return CurMaxFH;
}


/******************************************************************************
* Function:  SysBootDrive                                                     *
*                                                                             *
* Syntax:    drive = SysBootDrive()                                           *
*                                                                             *
* Params:    none                                                             *
*                                                                             *
* Return:    'A: B: C: D: ...'                                                *
******************************************************************************/
RexxRoutine0(RexxStringObject, SysBootDrive)
{
    ULONG drive;
    char  bootdrive[3] = "X:";

    if (DosQuerySysInfo(QSV_BOOT_DRIVE, QSV_BOOT_DRIVE, &drive, sizeof(drive)) == 0)
    {
        bootdrive[0] = drive + 'A' - 1;
        return context->NewStringFromAsciiz(bootdrive);
    }
    else
    {
        return context->NullString();
    }
}


/******************************************************************************
* Function:  SysCls - Clears the screen within the current VIO session.       *
*                                                                             *
* Syntax:    call SysCls                                                      *
*                                                                             *
* Return:    NO_UTIL_ERROR - Successful.                                      *
******************************************************************************/
RexxRoutine0(int, SysCls)
{
    BYTE bCell[2];

    bCell[0] = 0x20;
    bCell[1] = 0x07;
    VioScrollDn(0, 0, (USHORT)0xFFFF, (USHORT)0XFFFF, (USHORT)0xFFFF, bCell, (HVIO)0);

    return 0;
}


/******************************************************************************
* Function:  SysCurPos - positions cursor in VIO window                       *
*                                                                             *
* Syntax:    result = SysCurPos([row, col])                                   *
*                                                                             *
* Params:    row   - row to place cursor on                                   *
*            col   - column to place cursor on                                *
*                                                                             *
* Return:    row, col                                                         *
******************************************************************************/
RexxRoutine2(RexxStringObject, SysCurPos, OPTIONAL_stringsize_t, inrow, OPTIONAL_stringsize_t, incol)
{
    USHORT row;
    USHORT col;
    char   buf[256];

    if ((argumentExists(1) && argumentOmitted(2)) || (argumentExists(2) && argumentOmitted(1)))
    {
        context->InvalidRoutine();
        return NULL;
    }

    if (VioGetCurPos(&row, &col, (HVIO)0) == 0)
    {
        snprintf(buf, sizeof(buf), "%d %d", (int)row, (int)col);

        if (argumentExists(2))
        {
            row = (USHORT)inrow;
            col = (USHORT)incol;
            VioSetCurPos(row, col, (HVIO)0);
        }

        return context->NewStringFromAsciiz(buf);
    }

    return context->NullString();
}


/******************************************************************************
* Function:  SysCurState - Displays or hides the cursor within the current    *
*                          text session.                                      *
*                                                                             *
* Syntax:    call SysCurState state                                           *
*                                                                             *
* Params:    state - Either 'ON' or 'OFF'.                                    *
*                                                                             *
* Return:    NO_UTIL_ERROR - Successful.                                      *
******************************************************************************/
RexxRoutine1(int, SysCurState, CSTRING, option)
{
    USHORT state;
    VIOCURSORINFO vioci;

    if (stricmp(option, "ON") == 0)
    {
        state = (USHORT)0;
    }
    else if (stricmp(option, "OFF") == 0)
    {
        state = (USHORT)-1;
    }
    else
    {
        invalidOptionException(context, "SysCurState", "option", "'ON' or 'OFF'", option);
    }

    VioGetCurType(&vioci, (HVIO)0);
    vioci.attr = state;
    VioSetCurType(&vioci, (HVIO)0);
    return 0;
}


/******************************************************************************
* Function:  SysGetKey - Reads a key from the keyboard buffer and returns its *
*            value                                                            *
*                                                                             *
* Syntax:    result = SysGetKey([echo])                                       *
*                                                                             *
* Params:    echo - Either of the following:                                  *
*                    'ECHO'   - Echo the inputted key (default).              *
*                    'NOECHO' - Do not echo the inputted key.                 *
*                                                                             *
* Return:    The key striked.                                                 *
******************************************************************************/
RexxRoutine1(RexxStringObject, SysGetKey, OPTIONAL_CSTRING, echoOpt)
{
    bool echo = true;                // indicates if we're expected to echo the character.
    static UCHAR ExtendedChar = 0;   // saved extended character
    KBDKEYINFO info;

    // if we have an option specified, validate it
    if (echoOpt != NULL)
    {
        if (!stricmp(echoOpt, "NOECHO"))
        {
            echo = false;
        }
        else if (stricmp(echoOpt, "ECHO"))
        {
            invalidOptionException(context, "SysGetKey", "echo", "'ECHO' or 'NOECHO'", echoOpt);
        }
    }

    // if we have the second part of an extended value, return that without
    // doing a real read.
    if (ExtendedChar)
    {
        info.chChar = ExtendedChar;
        ExtendedChar = 0;
    }
    else
    {
        if (KbdCharIn(&info, IO_WAIT, 0) == 0)
        {
            // either of these characters indicate this is an extended character.
            if ((info.fbStatus & 0x02) && (info.chChar == 0 || info.chChar == 0xE0))
            {
                // this character is saved for the next call. We return the marker
                // character first.
                ExtendedChar = info.chScan;
            }
        }
    }
    if (echo)
    {
        VioWrtTTY((PCH)&info.chChar, 1, 0);
    }

    return context->NewString((PCH)&info.chChar, 1);
}


/******************************************************************************
* Function:  SysTextScreenRead                                                *
*                                                                             *
* Syntax:    result = SysTextScreenRead(row, col [,len])                      *
*                                                                             *
* Params:    row - Horizontal row on the screen to start reading from.        *
*                  The row at the top of the screen is 0.                     *
*            col - Vertical column on the screen to start reading from.       *
*                  The column at the left of the screen is 0.                 *
*            len - The number of characters to read.  The default is the rest *
*                  of the screen.                                             *
*                                                                             *
* Return:    Characters read from text screen.                                *
******************************************************************************/
RexxRoutine3(RexxStringObject, SysTextScreenRead, int, row, int, col, OPTIONAL_int, len)
{
    CHAR buf[8160];

    if (argumentOmitted(3))
    {
        len = sizeof(buf);
    }

    if (VioReadCharStr(buf, (PUSHORT)&len, row, col, (HVIO)0) != 0)
    {
        context->InvalidRoutine();
        return NULL;
    }

    return context->NewString(buf, len);
}


/******************************************************************************
* Function:  SysTextScreenSize                                                *
*                                                                             *
* Syntax:    result = SysTextScreenSize([option], [rows, colummns])           *
*            result = SysTextScreenSize([option], [top, left, bottom, right]) *
*                                                                             *
* Params:    option - "BUFFERSIZE", "WINDOWRECT", "MAXWINDOWSIZE"             *
*               "BUFFERSIZE" (default) return or set console buffer size      *
*               "WINDOWRECT" return or set windows position                   *
*               "MAXWINDOWSIZE" return maximum window size                    *
*            lines, columns - set buffer size to lines by columns             *
*            top, left, bottom, right - set window size and position          *
*                                                                             *
* Return:    "BUFFERSIZE" or "MAXWINDOWSIZE": rows columns                    *
*            "WINDOWRECT": top left bottom right                              *
******************************************************************************/
RexxRoutine5(RexxStringObject, SysTextScreenSize,
             OPTIONAL_CSTRING, optionString,
             OPTIONAL_stringsize_t, rows, OPTIONAL_stringsize_t, columns,
             OPTIONAL_stringsize_t, rows2, OPTIONAL_stringsize_t, columns2)
{
    // check for valid option
    typedef enum {BUFFERSIZE, WINDOWRECT, MAXWINDOWSIZE} console_option;
    console_option option;

    if (optionString == NULL || stricmp(optionString, "BUFFERSIZE") == 0)
    {
        option = BUFFERSIZE;
    }
    else if (stricmp(optionString, "WINDOWRECT") == 0)
    {
        option = WINDOWRECT;
    }
    else if (stricmp(optionString, "MAXWINDOWSIZE") == 0)
    {
        option = MAXWINDOWSIZE;
    }
    else
    {
        invalidOptionException(context, "SysTextScreenSize", "option", "BUFFERSIZE, WINDOWRECT, or MAXWINDOWSIZE", optionString);
    }

    // check for valid SET arguments: either none, or two more, or four more
    size_t setArgs;

    if (argumentOmitted(2) &&
        argumentOmitted(3) &&
        argumentOmitted(4) &&
        argumentOmitted(5))
    {
        setArgs = 0;
    }
    else if (argumentExists(2) &&
             argumentExists(3) &&
            !argumentExists(4) &&
            !argumentExists(5))
    {
        setArgs = 2;
    }
    else if (argumentExists(2) &&
             argumentExists(3) &&
             argumentExists(4) &&
             argumentExists(5))
    {
        setArgs = 4;
    }
    else
    {
        context->InvalidRoutine();
        return 0;
    }

    // check that all SET arguments fit a USHORT
    if (!(setArgs == 0 ||
         (setArgs == 2 && rows <= USHRT_MAX && columns <= USHRT_MAX) ||
         (setArgs == 4 && rows <= USHRT_MAX && columns <= USHRT_MAX && rows2 <= USHRT_MAX && columns2 <= USHRT_MAX)))
    {
        context->InvalidRoutine();
        return 0;
    }

    // real work starts here
    VIOMODEINFO ModeData = {sizeof(ModeData)};
    char buf[100];
    APIRET rc;

    if (VioGetMode(&ModeData, (HVIO)0) != 0)
    {
        context->InvalidRoutine();
        return NULL;
    }

    if (option == BUFFERSIZE && setArgs == 0)
    {
        // this is a BUFFERSIZE GET, returns two values
        snprintf(buf, sizeof(buf), "%d %d", ModeData.row, ModeData.col);
    }
    else if (option == WINDOWRECT && setArgs == 0)
    {
        // this is a WINDOWRECT GET, returns four values
        snprintf(buf, sizeof(buf), "%d %d %d %d", 0, 0, ModeData.row, ModeData.col);
    }
    else if (option == MAXWINDOWSIZE && setArgs == 0)
    {
        // this is a MAXWINDOWSIZE GET, don't have ideas how to determine these
        // values under OS/2. So return the current window size.
        snprintf(buf, sizeof(buf), "%d %d", ModeData.row, ModeData.col);
    }
    else if (option == BUFFERSIZE && setArgs == 2)
    {
        // this is a BUFFERSIZE SET, requires two more arguments
        ModeData.col = rows;
        ModeData.row = columns;
        rc = VioSetMode(&ModeData, (HVIO)0);
        snprintf(buf, sizeof(buf), "%d", rc);
    }
    else if (option == WINDOWRECT  && setArgs == 4)
    {
        // this is a WINDOWRECT SET, requires four more arguments
        ModeData.col = rows2 - rows + 1;
        ModeData.row = columns2 - columns + 1;
        rc = VioSetMode(&ModeData, (HVIO)0);
        snprintf(buf, sizeof(buf), "%d", rc);
    }
    else
    {
        context->InvalidRoutine();
        return 0;
    }

    // return the buffer as result
    return context->NewStringFromAsciiz(buf);
}


/******************************************************************************
* Function:  SysCreateEventSem                                                *
*                                                                             *
* Syntax:    handle = SysCreateEventSem(name, manual)                         *
*                                                                             *
* Params:    name  -  optional name for a event semaphore any second argument *
*                     means manual reset event.                               *
* Return:    handle - token used as a event sem handle for SysPostEventSem,   *
*                     SysClearEventSem, SysCloseEventSem, and SysOpenEventSem *
*            '' - Empty string in case of any error                           *
******************************************************************************/
RexxRoutine2(RexxObjectPtr, SysCreateEventSem, OPTIONAL_CSTRING, name, OPTIONAL_CSTRING, reset)
{
    ULONG flAttr = DC_SEM_SHARED;
    HEV hev;

    if (reset != NULL)
    {
        flAttr |= DCE_POSTONE;
    }

    if (DosCreateEventSem(name, &hev, flAttr, FALSE) != 0)
    {
        return context->NullString();
    }

    return context->Uintptr((uintptr_t)hev);
}


/******************************************************************************
* Function:  SysOpenEventSem                                                  *
*                                                                             *
* Syntax:    result = SysOpenEventSem(handle)                                 *
*                                                                             *
* Params:    handle - handle of the event semaphore                           *
*                                                                             *
* Return:    Return code from DosOpenEventSem.                                *
******************************************************************************/
RexxRoutine1(int, SysOpenEventSem, uintptr_t, hev)
{
    return DosOpenEventSem(NULL, (PHEV)&hev);
}


/******************************************************************************
* Function:  SysPostEventSem                                                  *
*                                                                             *
* Syntax:    result = SysPostEventSem(handle)                                 *
*                                                                             *
* Params:    handle - token returned from SysCreateEventSem                   *
*                                                                             *
* Return:    Returns code from DosPostEventSem.                               *
******************************************************************************/
RexxRoutine1(int, SysPostEventSem, uintptr_t, hev)
{
    return DosPostEventSem(hev);
}


/******************************************************************************
* Function:  SysResetEventSem                                                 *
*                                                                             *
* Syntax:    result = SysResetEventSem(handle)                                *
*                                                                             *
* Params:    handle - token returned from SysCreateEventSem                   *
*                                                                             *
* Return:    Returns code from DosResetEventSem.                              *
******************************************************************************/
RexxRoutine1(int, SysResetEventSem, uintptr_t, hev)
{
    ULONG count;
    return DosResetEventSem(hev, &count);
}


/******************************************************************************
* Function:  SysPulseEventSem                                                 *
*                                                                             *
* Syntax:    result = SysPulseEventSem(handle)                                *
*                                                                             *
* Params:    handle - token returned from SysCreateEventSem                   *
*                                                                             *
* Return:    Returns code from DosPostEventSem.                               *
******************************************************************************/
RexxRoutine1(int, SysPulseEventSem, uintptr_t, hev)
{
    ULONG  count;
    APIRET rc = DosPostEventSem(hev);

    if (rc == 0)
    {
        DosResetEventSem(hev, &count);
    }

    return rc;
}


/******************************************************************************
* Function:  SysWaitEventSem                                                  *
*                                                                             *
* Syntax:    result = SysWaitEventSem(handle [,timeout])                      *
*                                                                             *
* Params:    handle - token returned from SysCreateEventSem                   *
*                                                                             *
* Return:    Returns code from DosWaitEventSem.                               *
******************************************************************************/
RexxRoutine2(int, SysWaitEventSem, uintptr_t, hev, OPTIONAL_int, timeout)
{
    if (!argumentExists(2))
    {
        timeout = SEM_INDEFINITE_WAIT;
    }

    return DosWaitEventSem(hev, timeout);
}


/******************************************************************************
* Function:  SysCloseEventSem                                                 *
*                                                                             *
* Syntax:    result = SysCloseEventSem(handle)                                *
*                                                                             *
* Params:    handle - token returned from SysCreateEventSem                   *
*                                                                             *
* Return:    Returns code from DosCloseEventSem.                              *
******************************************************************************/
RexxRoutine1(int, SysCloseEventSem, uintptr_t, hev)
{
    return DosCloseEventSem(hev);
}


/******************************************************************************
* Function:  SysCreateMutexSem                                                *
*                                                                             *
* Syntax:    handle = SysCreateMutexSem(name)                                 *
*                                                                             *
* Params:    name  - optional name for a mutex semaphore                      *
*                                                                             *
* Return:    handle - token used as a mutex handle for SysRequestMutexSem,    *
*                     SysReleaseMutexSem, SysCloseMutexSem, and               *
                      SysOpenEventSem                                         *
*            '' - Empty string in case of any error                           *
******************************************************************************/
RexxRoutine1(RexxObjectPtr, SysCreateMutexSem, OPTIONAL_CSTRING, name)
{
    HMTX hmtx;

    if (DosCreateMutexSem(name, &hmtx, DC_SEM_SHARED, FALSE) != 0)
    {
        return context->NullString();
    }

    return context->Uintptr((uintptr_t)hmtx);
}


/******************************************************************************
* Function:  SysOpenMutexSem                                                  *
*                                                                             *
* Syntax:    result = SysOpenMutexSem(handle)                                 *
*                                                                             *
* Params:    handle - handle of the mutex semaphore                           *
*                                                                             *
* Return:    Returns code from DosOpenMutexSem.                               *
******************************************************************************/
RexxRoutine1(int, SysOpenMutexSem, uintptr_t, hmtx)
{
    return DosOpenMutexSem(NULL, (PHMTX)&hmtx);
}


/******************************************************************************
* Function:  SysRequestMutexSem                                               *
*                                                                             *
* Syntax:    result = SysRequestMutexSem(handle [,timeout])                   *
*                                                                             *
* Params:    handle - token returned from SysCreateMutexSem                   *
*                                                                             *
* Return:    Returns code from DosRequestMutexSem,                            *
******************************************************************************/
RexxRoutine2(int, SysRequestMutexSem, uintptr_t, hmtx, OPTIONAL_int, timeout)
{
    if (argumentOmitted(2))
    {
        timeout = SEM_INDEFINITE_WAIT;
    }

    return DosRequestMutexSem(hmtx, timeout);
}


/******************************************************************************
* Function:  SysReleaseMutexSem                                               *
*                                                                             *
* Syntax:    result = SysReleaseMutexSem(handle)                              *
*                                                                             *
* Params:    handle - token returned from SysCreateMutexSem                   *
*                                                                             *
* Return:    Returns code from DosReleaseMutexSem.                            *
******************************************************************************/
RexxRoutine1(int, SysReleaseMutexSem, uintptr_t, hmtx)
{
    return DosReleaseMutexSem(hmtx);
}


/******************************************************************************
* Function:  SysCloseMutexSem                                                 *
*                                                                             *
* Syntax:    result = SysCloseMutexSem(handle)                                *
*                                                                             *
* Params:    handle - token returned from SysCreateMutexSem                   *
*                                                                             *
* Return:    Returns code from DosCloseMutexSem.                              *
******************************************************************************/
RexxRoutine1(int, SysCloseMutexSem, uintptr_t, hmtx)
{
    return DosCloseMutexSem(hmtx);
}


/******************************************************************************
* Function:  SysDriveInfo - returns total number of free bytes, total number  *
*                           of bytes, and volume label                        *
*                                                                             *
* Syntax:    result = SysDriveInfo([drive])                                   *
*                                                                             *
* Params:    drive - d, d:, d:\                                               *
*                    if omitted, defaults to current drive                    *
*                                                                             *
* Return:    result - drive free total label                                  *
*                     null string for any error                               *
******************************************************************************/
RexxRoutine1(RexxStringObject, SysDriveInfo, OPTIONAL_CSTRING, drive)
{
    ULONG drivenum = 0;
    FSALLOCATE FSAlloc;
    FSINFO FSInfo;
    APIRET rc;

    if (drive != NULL)
    {
        if (!isalpha(drive[0]) || (strlen(drive) > 1 && drive[1] != ':'))
        {
            context->InvalidRoutine();
            return NULL;
        }

        drivenum = toupper(drive[0])-'A'+1;
    } else {
        ULONG logical;
        DosQueryCurrentDisk(&drivenum, &logical);
    }

    DosError(FERR_DISABLEHARDERR);
    rc = DosQueryFSInfo(drivenum, FSIL_VOLSER, &FSInfo, sizeof(FSInfo));
    DosQueryFSInfo(drivenum, FSIL_ALLOC, &FSAlloc, sizeof(FSAlloc));
    DosError(FERR_ENABLEHARDERR);

    if (rc == 0)
    {
        char retstr[256];
        snprintf(retstr, sizeof(retstr), "%c: %llu %llu %s",
                (drivenum+'A'-1),
                (ULONGLONG)FSAlloc.cUnitAvail * FSAlloc.cSectorUnit * FSAlloc.cbSector,
                (ULONGLONG)FSAlloc.cUnit * FSAlloc.cSectorUnit * FSAlloc.cbSector,
                FSInfo.vol.szVolLabel);

        return context->String(retstr);
    }

    return context->NullString();
}


/******************************************************************************
* Function:  SysDriveMap                                                      *
*                                                                             *
* Syntax:    result = SysDriveMap([drive] [,mode])                            *
*                                                                             *
* Params:    drive - 'C', 'D', 'E', etc.  The drive to start the search with. *
*            mode  - Any of the following:  'FREE', 'USED', 'DETACHED',       *
*                    'LOCAL', 'REMOTE'                                        *
*                                                                             *
* Return:    'A: B: C: D: ...'                                                *
******************************************************************************/
RexxRoutine2(RexxStringObject, SysDriveMap, OPTIONAL_CSTRING, drive, OPTIONAL_CSTRING, mode)
{
    typedef enum {USED, FREE, DETACHED, REMOTE, LOCAL} mode_type;
    mode_type selectMode = USED;
    ULONG start = 3;
    ULONG CurDrive;
    ULONG DriveMap;

    FileNameBuffer driveMapBuffer;  // the map is built here
    char driveStr[8];               // just enough space to format a drive specifier

    if (drive != NULL)
    {
        if (!isalpha(drive[0]) || (strlen(drive) > 1 && drive[1] != ':'))
        {
            context->InvalidRoutine();
            return NULL;
        }

        start = toupper(drive[0])-'A'+1;
    }
    if (mode != NULL)
    {
        if (stricmp(mode, "USED") == 0)
        {
            selectMode = USED;
        }
        else if (stricmp(mode, "FREE") == 0)
        {
            selectMode = FREE;
        }
        else if (stricmp(mode, "DETACHED") == 0)
        {
            selectMode = DETACHED;
        }
        else if (stricmp(mode, "REMOTE") == 0)
        {
            selectMode = REMOTE;
        }
        else if (stricmp(mode, "LOCAL") == 0)
        {
            selectMode = LOCAL;
        }
        else
        {
            context->ThrowException1(Rexx_Error_Incorrect_call_user_defined, context->String("Invalid drive type"));
        }
    }

    DosError(FERR_DISABLEHARDERR);
    DosQueryCurrentDisk(&CurDrive, &DriveMap);
    // Shift to the first drive
    DriveMap >>= (start-1);

    for (ULONG drivenum = start; drivenum <= 26; drivenum++)
    {
        if (!(DriveMap & 1) && selectMode == FREE)
        {
            // Hey, we have a free drive
            sprintf(driveStr, "%c: ", drivenum + 'A' - 1);
            driveMapBuffer += driveStr;
        }
        else if ((DriveMap & 1) && selectMode == USED)
        {
            // Hey, we have a used drive
            sprintf(driveStr, "%c: ", drivenum + 'A' - 1);
            driveMapBuffer += driveStr;
        }
        // Check specific drive info
        else if (DriveMap & 1)
        {
            FSQBUFFER2 fsqb;
            ULONG fsqblen = sizeof(fsqb);
            FSINFO FSInfo;
            APIRET rc;

            sprintf(driveStr, "%c:", drivenum + 'A' - 1);
            DosQueryFSAttach(driveStr, 0, FSAIL_QUERYNAME, &fsqb, &fsqblen);
            rc = DosQueryFSInfo(drivenum, FSIL_VOLSER, &FSInfo, sizeof(FSInfo));

            if (rc == ERROR_BAD_NET_NAME && fsqb.iType == FSAT_REMOTEDRV && selectMode == DETACHED)
            {
                // Hey, we have a detached drive
                sprintf(driveStr, "%c: ", drivenum + 'A' - 1);
                driveMapBuffer += driveStr;
            }
            else if (fsqb.iType == FSAT_REMOTEDRV && selectMode == REMOTE)
            {
                sprintf(driveStr, "%c: ", drivenum + 'A' - 1);
                driveMapBuffer += driveStr;
            }
            else if (fsqb.iType == FSAT_LOCALDRV  && selectMode == LOCAL)
            {
                sprintf(driveStr, "%c: ", drivenum + 'A' - 1);
                driveMapBuffer += driveStr;
            }
        }

        // Shift to the next drive
        DriveMap >>= 1;
    }

    DosError(FERR_ENABLEHARDERR);

    size_t driveLength = driveMapBuffer.length();
    // if we are returning anything, then remove the last blank
    if (driveLength > 0)
    {
        driveLength--;
    }

    return context->NewString(driveMapBuffer, driveLength);
}


/******************************************************************************
* Function:  SysElapsedTime                                                   *
*                                                                             *
* Syntax:    result = SysElapsedTime([option])                                *
*                                                                             *
* Params:    option - Controls the behaviour of the call to SysElapsedTime.   *
*            The acceptable values are: 'ELAPSED", 'RESET'. Only the first    *
*            letter is required (e.g. 'E').                                   *
*                                                                             *
* Return:    'sssssssss.uuuuuu' where 'sssssssss' is one to nine digits       *
*             representing seconds (no leading zeros or spaces), and 'uuuuuu' *
*             is always six digits representing microseconds.                 *
******************************************************************************/
RexxRoutine1(RexxStringObject, SysElapsedTime, OPTIONAL_CSTRING, option)
{
    typedef enum {ELAPSED, RESET} option_type;
    option_type selectMode = ELAPSED;
    ULONGLONG time;
    ULONG freq;

    static ULONGLONG start = 0;
    static bool reset = false;
    char retstr[32];

    if (option != NULL)
    {
        if (toupper(option[0]) == 'R')
        {
            selectMode = RESET;
        }
        else if (toupper(option[0]) == 'E')
        {
            selectMode = ELAPSED;
        }
        else
        {
            context->ThrowException1(Rexx_Error_Incorrect_call_user_defined, context->String("Invalid option"));
        }
    }

    DosTmrQueryTime((PQWORD)&time);
    DosTmrQueryFreq(&freq);

    if (!reset)
    {
        strcpy(retstr, "0");
    }
    else
    {
        double secs = (double)(time - start) / freq;
        snprintf(retstr, sizeof(retstr), "%.6f", secs);

        if (selectMode == RESET)
        {
            start = time;
            reset = true;
        }
    }

    return context->String(retstr);
}


/******************************************************************************
* Function:  SysGetCollate                                                    *
*                                                                             *
* Syntax:    result = SysGetCollate([country] [,codepage])                    *
*                                                                             *
* Params:    country - The three-digit country code. A value of 0 indicates   *
*            the currently-active country code; this is also the default if   *
*            not specified.                                                   *
*            codepage - The codepage number. A value of 0 indicates the       *
*            currently-active codepage; this is also the default if not       *
*            specified.                                                       *
*                                                                             *
* Return:    a 256-character string containing the collating sequence table.  *
******************************************************************************/
RexxRoutine2(RexxStringObject, SysGetCollate, OPTIONAL_int, country, OPTIONAL_int, codepage)
{
    COUNTRYCODE ccode;
    CHAR  collate[256];
    ULONG len;

    ccode.country = country;
    ccode.codepage = codepage;

    if (DosQueryCollate(sizeof(collate), &ccode, collate, &len) != 0)
    {
        context->InvalidRoutine();
        return NULL;
    }

    return context->NewString(collate, len);
}


/******************************************************************************
* Function:  SysQueryEAList                                                   *
*                                                                             *
* Syntax:    result = SysQueryEAList(filename, stem)                          *
*                                                                             *
* Params:    filename - The name of the file or directory whose extended      *
*            attributes will be listed.                                       *
*            stem - The name of a stem variable in which the list of extended *
*            attribute names will be saved.                                   *
*                                                                             *
* Return:    0 if successful; otherwise, it returns an operating system error *
*            code.                                                            *
******************************************************************************/
RexxRoutine2(int, SysQueryEAList, CSTRING, filename, RexxObjectPtr, stem)
{
    StemHandler names(context, stem, 2);
    FILESTATUS4 fs;
    DENA2 *pEANames;
    DENA2 *pEAFound;
    ULONG  size;
    ULONG  count = -1;
    APIRET rc;

    if ((rc = QueryPathInfo(filename, FIL_QUERYEASIZE, &fs, sizeof(fs))) != 0) {
        return rc;
    }

    // The buffer size is less than or equal to twice the size of the file's
    // entire EA set on disk.
    if((size = fs.cbList*2) == 0) {
        return rc;
    }

    pEANames = (DENA2*)malloc(size);
    if (pEANames == NULL)
    {
        outOfMemoryException(context);
    }

    rc = DosEnumAttribute(ENUMEA_REFTYPE_PATH, (PVOID)filename, 1, pEANames, size,
                          &count, ENUMEA_LEVEL_NO_VALUE);

    if (rc == 0 && count)
    {
        pEAFound = pEANames;
        while (count--)
        {
            names.addValue(pEAFound->szName);
            pEAFound = (DENA2*)((PCH)pEAFound + pEAFound->oNextEntryOffset);
        }
    }

    free(pEANames);
    return rc;
}


/******************************************************************************
* Function:  SysGetEA                                                         *
*                                                                             *
* Syntax:    result = SysGetEA(filename, attribute, variable)                 *
*                                                                             *
* Params:    filename  - The name of the file whose extended attribute is     *
*                        being queried.                                       *
*            attribute - The name of the extended attribute being queried.    *
*            variable  - The name of the variable into which the extended     *
                         attribute's value will be placed.                    *
*                                                                             *
* Return:    0 if successful; otherwise, it returns an operating system error *
*            code.                                                            *
******************************************************************************/
RexxRoutine3(int, SysGetEA, CSTRING, filename, CSTRING, attribute, CSTRING, var)
{
    EAOP2 eaop2;
    int gea2list_size = sizeof(GEA2LIST) + strlen(attribute);
    int fea2list_size = sizeof(FEA2LIST) + sizeof(FEA2) + 65535;
    APIRET rc;

    eaop2.fpGEA2List = (PGEA2LIST)calloc(gea2list_size, 1);
    eaop2.fpFEA2List = (PFEA2LIST)calloc(fea2list_size, 1);

    if (eaop2.fpGEA2List == NULL || eaop2.fpFEA2List == NULL)
    {
        outOfMemoryException(context);
    }

    eaop2.fpFEA2List->cbList = fea2list_size;
    eaop2.fpGEA2List->cbList = gea2list_size;
    eaop2.fpGEA2List->list[0].oNextEntryOffset = 0;
    eaop2.fpGEA2List->list[0].cbName = strlen(attribute);
    strcpy(eaop2.fpGEA2List->list[0].szName, attribute);

    rc = QueryPathInfo(filename, FIL_QUERYEASFROMLIST, &eaop2, sizeof(eaop2));

    if (rc == 0)
    {
        PCHAR  pdata = eaop2.fpFEA2List->list[0].szName + eaop2.fpFEA2List->list[0].cbName + 1;
        USHORT len = eaop2.fpFEA2List->list[0].cbValue;
        context->SetContextVariable(var, context->NewString(pdata, len));
    }
    else
    {
        context->SetContextVariable(var, context->NullString());
    }

    free(eaop2.fpGEA2List);
    free(eaop2.fpFEA2List);
    return rc;
}


/******************************************************************************
* Function:  SysPutEA                                                         *
*                                                                             *
* Syntax:    result = SysPutEA(filename, attribute, value)                    *
*                                                                             *
* Params:    filename  - The file to which the extended attribute will be     *
*                        written                                              *
*            attribute - The name of the extended attribute to write.         *
*            value     - The new value of the extended attribute.             *
*                                                                             *
* Return:    0 if successful; otherwise, it returns an operating system error *
*            code.                                                            *
******************************************************************************/
RexxRoutine3(int, SysPutEA, CSTRING, filename, CSTRING, attribute, RexxStringObject, value)
{
    EAOP2 eaop2;
    PFEA2 pfea2;
    int fea2list_size = sizeof(FEA2LIST) + sizeof(FEA2) + 65535;
    APIRET rc;

    if (strlen(attribute) > 255)
    {
        context->ThrowException1(Rexx_Error_Incorrect_call_user_defined, context->String("Invalid option"));
    }

    eaop2.fpGEA2List = NULL;            // Unused
    eaop2.fpFEA2List = (PFEA2LIST)calloc(fea2list_size, 1);

    if (eaop2.fpFEA2List == NULL)
    {
        outOfMemoryException(context);
    }

    pfea2 = &eaop2.fpFEA2List->list[0]; // point to first FEA
    pfea2->oNextEntryOffset = 0;
    pfea2->fEA = 0;
    pfea2->cbName = (BYTE)strlen(attribute);
    pfea2->cbValue = (SHORT)context->StringLength(value);
    strcpy((PSZ)pfea2->szName, attribute);
    memcpy((PSZ)pfea2->szName + (pfea2->cbName+1), context->StringData(value), pfea2->cbValue);
    eaop2.fpFEA2List->cbList = sizeof(ULONG) + sizeof(FEA2) + pfea2->cbName + pfea2->cbValue;

    rc = DosSetPathInfo(filename, 2, &eaop2, sizeof(EAOP2), 0);
    free(eaop2.fpFEA2List);
    return rc;
}


/******************************************************************************
* Function:  SysGetFileDateTime                                               *
*                                                                             *
* Syntax:    result = SysGetFileDateTime(filename [,timesel])                 *
*                                                                             *
* Params:    filename - name of the file to query                             *
*            timesel  - What filetime to query: Created/Access/Write          *
*                                                                             *
* Return:    -1 - file date/time query failed                                 *
*            other - date and time as YYYY-MM-DD HH:MM:SS                     *
******************************************************************************/
RexxRoutine2(RexxObjectPtr, SysGetFileDateTime, CSTRING, name, OPTIONAL_CSTRING, selector)
{
    FILESTATUS3L fs;
    FDATE date;
    FTIME time;

    if (DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs)) != 0)
    {
        return context->WholeNumber(-1);
    }

    if (selector != NULL)
    {
        switch (selector[0])
        {
            case 'c':
            case 'C':
                date = fs.fdateCreation;
                time = fs.ftimeCreation;
                break;
            case 'a':
            case 'A':
                date = fs.fdateLastAccess;
                time = fs.ftimeLastAccess;
                break;
            case 'w':
            case 'W':
                date = fs.fdateLastWrite;
                time = fs.ftimeLastWrite;
                break;
            default:
                invalidOptionException(context, "SysGetFileDateTime", "time selector", "'A', 'C', or 'W'", selector);
        }
    }
    else
    {
        date = fs.fdateLastWrite;
        time = fs.ftimeLastWrite;
    }

    char buf[256];
    snprintf(buf, sizeof(buf), "%4d-%02d-%02d %02d:%02d:%02d",
             date.year + 1980, date.month, date.day,
             time.hours, time.minutes, time.twosecs*2);
    return context->String(buf);
}


/******************************************************************************
* Function:  SysSetFileDateTime                                               *
*                                                                             *
* Syntax:    result = SysSetFileDateTime(filename [,newdate] [,newtime])      *
*                                                                             *
* Params:    filename - name of the file to update                            *
*            newdate  - new date to set in format YYYY-MM-DD (YYYY>1800)      *
*            newtime  - new time to set in format HH:MM:SS                    *
*                                                                             *
* Return:    0 - file date/time was updated correctly                         *
*            -1 - failure attribute update                                    *
******************************************************************************/
RexxRoutine3(int, SysSetFileDateTime, CSTRING, name, OPTIONAL_CSTRING, newdate, OPTIONAL_CSTRING, newtime)
{
    FILESTATUS3L fs;
    unsigned int year, month, day, hours, minutes, seconds;
    DATETIME dt;

    if (DosQueryPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs)) != 0)
    {
        return -1;
    }

    if (newdate == NULL && newtime == NULL)
    {
        // we set the timestamp to the current time and date
        DosGetDateTime(&dt);

        fs.fdateLastWrite.year = dt.year - 1980;
        fs.fdateLastWrite.month = dt.month;
        fs.fdateLastWrite.day = dt.day;
        fs.ftimeLastWrite.hours = dt.hours;
        fs.ftimeLastWrite.minutes = dt.minutes;
        fs.ftimeLastWrite.twosecs = dt.seconds / 2;
    }
    else
    {
        // now parse the new date/time
        if (newdate != NULL)
        {
            if (sscanf(newdate, "%4hu-%2hu-%2hu", &year, &month, &day) != 3)
            {
                return -1;
            }
            if (year < 1800)
            {
                return -1;
            }
        }
        if (newtime != NULL)
        {
            if (sscanf(newtime, "%2hu:%2hu:%2hu", &hours, &minutes, &seconds) != 3)
            {
                return -1;
            }
        }

        fs.fdateLastWrite.year = year - 1980;
        fs.fdateLastWrite.month = month;
        fs.fdateLastWrite.day = day;
        fs.ftimeLastWrite.hours = hours;
        fs.ftimeLastWrite.minutes = minutes;
        fs.ftimeLastWrite.twosecs = seconds / 2;
    }

    if (DosSetPathInfo(name, FIL_STANDARDL, &fs, sizeof(fs), 0) != 0)
    {
        return -1;
    }

    return 0;
}


/******************************************************************************
* Function:  SysGetMessage                                                    *
*                                                                             *
* Syntax:    result = SysGetMessage(msgnum [,file] [,str1]...[,str9])         *
*                                                                             *
* Params:    msgnum         - Number of message being queried.                *
*            file           - Name of message file to get message from.       *
*                             Default is OSO001.MSG.                          *
*            str1 ... str9  - Insertion strings.  For messages which          *
*                             contain %1, %2, etc, the str1, str2, etc        *
*                             strings will be inserted (if given).            *
*                                                                             *
* Return:    The message with the inserted strings (if given).                *
******************************************************************************/
RexxRoutine3(RexxStringObject, SysGetMessage, nonnegative_wholenumber_t, msgnum,
             OPTIONAL_CSTRING, msgfile, ARGLIST, args)
{
    ULONG cTable = 0;
    PCHAR pTable[9];
    size_t numargs = context->ArraySize(args);
    CHAR msgbuf[8160];
    ULONG msglen;

    // we only support 9 substitution values
    if (numargs > 2 + 9)
    {
        context->InvalidRoutine();
        return NULL;
    }

    if (msgfile == NULL)
    {
        msgfile = "OSO001.MSG";
    }

    for (size_t i = 3; i <= numargs; i++)
    {
        RexxObjectPtr o = context->ArrayAt(args, i);
        if (o == NULLOBJECT)
        {
            // use a null string for any omitted ones
            pTable[cTable++] = (PCHAR)"";
        }
        else
        {
            pTable[cTable++] = (PCHAR)context->ObjectToStringValue(o);
        }
    }

    if (DosGetMessage(pTable, cTable, msgbuf, sizeof(msgbuf), msgnum, msgfile, &msglen) != 0)
    {
        return context->NullString();
    }

    return context->NewString(msgbuf, msglen);
}


/******************************************************************************
* Function:  SysIni                                                           *
*                                                                             *
* Syntax:    result = SysIni([inifile], app [,key/stem] [,val/stem])          *
*                                                                             *
* Params:    inifile - INI file from which to query or write info.  The       *
*                       default is the current user INI file.                 *
*            app     - Application title to work with.  May be either         *
*                       of the following:                                     *
*                        'ALL:' - All app titles will be returned in the      *
*                                  stem variable specified by the next        *
*                                  parameter.                                 *
*                        other  - Specific app to work with.                  *
*            key     - Key to work with.  May be any of the following:        *
*                        'ALL:'    - All key titles will be returned in       *
*                                     the stem variable specified by the      *
*                                     next parameter.                         *
*                        'DELETE:' - All keys associated with the app         *
*                                     will be deleted.                        *
*                        other     - Specific key to work with.               *
*            val     - Key to work with. May be either of the following:      *
*                        'DELETE:' - Delete app/key pair.                     *
*                        other     - Set app/key pair info to data spec-      *
*                                     ified.                                  *
*            stem    - Name of stem variable in which to store results.       *
*                      Stem.0 = Number found (n).                             *
*                      Stem.1 - Stem.n = Entries found.                       *
*                                                                             *
* Return:    other          - Info queried from specific app/key pair.        *
*            ''             - Info queried and placed in stem or data         *
*                              deleted successfully.                          *
*            ERROR_NOMEM    - Out of memory.                                  *
*            ERROR_RETSTR   - Error opening INI or querying/writing info.     *
******************************************************************************/
RexxRoutine4(RexxStringObject, SysIni, OPTIONAL_CSTRING, iniFile, CSTRING, app,
                               RexxObjectPtr, key, OPTIONAL_RexxObjectPtr, val)
{
    HINI hini = NULLHANDLE;
    bool needclose = false;
    StemHandler names(context);
    bool write = false;
    bool array = false;
    BOOL rc;
    AutoFree buf = NULL;
    ULONG bufsize = 65535;

    const char* appstr = NULL;
    const char* keystr = NULL;
    const char* valstr = NULL;

    if (stricmp(app, "ALL:") == 0)
    {
        // the third arg is required and is the stem variable name.
        // the 4th argument cannot be specified
        if (argumentExists(4))
        {
            maxArgException(context, "SysIni ALL:", 3);
        }

        // Retrieve the stem variable using the key name
        names.setStem(key, 3);
        array = true;
    } else {
      appstr = app;
      keystr = context->ObjectToStringValue(key);

      if (stricmp(keystr, "ALL:") == 0)
      {
          // val is the stem variable for this case
          names.setStem(val, 4);
          keystr = NULL;
          array = true;
      }
      else if (stricmp(keystr, "DELETE:") == 0)
      {
          keystr = NULL;
          write = true;
      }
      else if (val != NULL)
      {
          valstr = context->ObjectToStringValue(val);
          if (stricmp(valstr, "DELETE:") == 0)
          {
              valstr = NULL;
          }
          write = true;
      }
    }

    // The following section of code gets the INI file handle given the INI
    // file spec (iniFile).
    //
    //  NULL     - Same as USERPROFILE   (OS2.INI)
    // "BOTH"    - Same as PROFILE       (OS2.INI & OS2SYS.INI)
    // "USER"    - Same as USERPROFILE   (OS2.INI)
    // "SYSTEM"  - Same as SYSTEMPROFILE (OS2SYS.INI)
    //  other    - Filespec of INI file.

    if (iniFile == NULL || stricmp(iniFile, "USER") == 0)
    {
        hini = HINI_USERPROFILE;
    }
    else if (stricmp(iniFile, "SYSTEM") == 0)
    {
        hini = HINI_SYSTEMPROFILE;
    }
    else if (stricmp(iniFile, "BOTH") == 0)
    {
        hini = HINI_PROFILE;
    }
    else
    {
        // If Ini file spec is 'other' then make sure it does not
        // specify the current USER or SYSTEM profiles.             *
        CHAR username[CCHMAXPATH];
        CHAR sysname[CCHMAXPATH];
        PRFPROFILE prfp;
        HAB hab = WinQueryAnchorBlock(HWND_DESKTOP);

        prfp.pszUserName = username;
        prfp.cchUserName = sizeof(username);
        prfp.pszSysName  = sysname;
        prfp.cchSysName  = sizeof(sysname);

        if (PrfQueryProfile(hab, &prfp))
        {
            if (stricmp(iniFile, prfp.pszUserName) == 0)
            {
                hini = HINI_USERPROFILE;
            }
            else if (stricmp(iniFile, prfp.pszSysName) == 0)
            {
                hini = HINI_SYSTEMPROFILE;
            }
        }
        if (hini == NULLHANDLE)
        {
            hini = PrfOpenProfile(hab, iniFile);
            needclose = true;
        }
        if (hini == NULLHANDLE)
        {
            return context->NewStringFromAsciiz(ERROR_RETSTR);
        }
    }

    if (write)
    {
        rc = PrfWriteProfileData(hini, appstr, keystr, (PVOID)valstr, 0);
    }
    else
    {
        // Allocate a large buffer for retrieving the information
        if ((buf = (PCHAR)malloc(bufsize)) != NULL)
        {
             rc = PrfQueryProfileData(hini, appstr, keystr, buf, &bufsize);
        }
        else
        {
             bufsize = -1;
        }
    }

    if (needclose)
    {
        PrfCloseProfile(hini);
    }

    if (rc)
    {
        if (write)
        {
            return context->NullString();
        }
        else if(array)
        {
            names.addList(buf);
            return context->NullString();
        }
        else
        {
            return context->NewString(buf, bufsize);
        }
    }
    else
    {
        return context->NewStringFromAsciiz(bufsize == -1 ? ERROR_NOMEM : ERROR_RETSTR);
    }
}


/******************************************************************************
* Function:  SysMapCase                                                       *
*                                                                             *
* Syntax:    result = SysMapCase(string [,country [,codepage]])               *
*                                                                             *
* Params:    string - String to be converted to uppercase.                    *
*            country - The three-digit country code. A value of 0 indicates   *
*            the currently-active country code; this is also the default if   *
*            not specified.                                                   *
*            codepage - The codepage number. A value of 0 indicates the       *
*            currently-active codepage; this is also the default if not       *
*            specified.                                                       *
*                                                                             *
* Return:    Returns the uppercased form of the specified string.             *
******************************************************************************/
RexxRoutine3(RexxStringObject, SysMapCase, CSTRING, string, OPTIONAL_int, country, OPTIONAL_int, codepage)
{
  COUNTRYCODE cc;
  AutoFree buf = strdup(string);
  ULONG cb;

  if (buf == NULL)
  {
      context->InvalidRoutine();
      return NULL;
  }

  cc.country = country;
  cc.codepage = codepage;
  cb = strlen(buf);

  if (DosMapCase(cb, &cc, buf) != 0)
  {
      context->InvalidRoutine();
      return NULL;
  }

  return context->NewString(buf, cb);
}


/******************************************************************************
* Function:  SysMkDir                                                         *
*                                                                             *
* Syntax:    result = SysMkDir(dir)                                           *
*                                                                             *
* Params:    dir - Directory to be created.                                   *
*                                                                             *
* Return:    Return code from DosCreateDir()                                  *
******************************************************************************/
RexxRoutine1(int, SysMkDir, CSTRING, dir)
{
    return DosCreateDir(dir, NULL);
}


/******************************************************************************
* Function:  SysNationalLanguageCompare                                       *
*                                                                             *
* Syntax:    result = SysNationalLanguageCompare(string1, string2             *
*                                                [,country [,codepage]])      *
*                                                                             *
* Params:    string1 - The first string to compare.                           *
*            string2 - The second string to compare.                          *
*            country - The three-digit country code. A value of 0 indicates   *
*            the currently-active country code; this is also the default if   *
*            not specified.                                                   *
*            codepage - The codepage number. A value of 0 indicates the       *
*            currently-active codepage; this is also the default if not       *
*            specified.                                                       *
*                                                                             *
* Return:    Returns  one of the following values:                            *
*                                                                             *
*             0 string1 and string2 are equal.                                *
*             1 string1 has a greater value than string2.                     *
*            -1 string2 has a greater value than string1.                     *
******************************************************************************/
RexxRoutine4(int, SysNationalLanguageCompare, CSTRING, string1, CSTRING, string2,
             OPTIONAL_int, country, OPTIONAL_int, codepage)
{
    COUNTRYCODE ccode;
    CHAR  collate[256];
    ULONG len;
    int rel = 0;

    ccode.country = country;
    ccode.codepage = codepage;

    if (DosQueryCollate(sizeof(collate), &ccode, collate, &len) != 0)
    {
        context->InvalidRoutine();
        return 0;
    }

    do {
        rel = collate[(unsigned char)*string1] -
              collate[(unsigned char)*string2];

    } while (*string1++ && *string2++ && rel == 0);

    return (rel > 0) - (rel < 0);
}


/******************************************************************************
* Function:  SysOS2Ver                                                        *
*                                                                             *
* Syntax:    result = SysOS2Ver()                                             *
*                                                                             *
* Return:    Returns a string in the form x.yy, where x is the OS/2 major     *
*            version number, and yy is the OS/2 minor version number.         *
******************************************************************************/
RexxRoutine0(RexxStringObject, SysOS2Ver)
{
    char  buf[16];
    ULONG ver[2];

    if (DosQuerySysInfo(QSV_VERSION_MAJOR, QSV_VERSION_MINOR, ver, sizeof(ver)) != 0)
    {
        context->InvalidRoutine();
        return NULL;
    }

    snprintf(buf, sizeof(buf), "%u.%u", ver[0], ver[1]);
    return context->NewStringFromAsciiz(buf);
}


/******************************************************************************
* Function:  SysVersion                                                       *
*                                                                             *
* Syntax:    result = SysVersion()                                            *
*                                                                             *
* Return:    Returns a string in the form OS version.                         *
******************************************************************************/
RexxRoutine0(RexxStringObject, SysVersion)
{
    char  buf[24];
    ULONG ver[2];

    if (DosQuerySysInfo(QSV_VERSION_MAJOR, QSV_VERSION_MINOR, ver, sizeof(ver)) != 0)
    {
        context->InvalidRoutine();
        return NULL;
    }

    snprintf(buf, sizeof(buf), "OS/2 %u.%u", ver[0], ver[1]);
    return context->NewStringFromAsciiz(buf);
}


/******************************************************************************
* Function:  SysProcessType                                                   *
*                                                                             *
* Syntax:    result = SysProcessType()                                        *
*                                                                             *
* Return:    Returns one of the following values:                             *
*                                                                             *
*            0 Full-screen protected mode.                                    *
*            1 Real mode.                                                     *
*            2 Windowable (VIO) protected mode.                               *
*            3 Presentation Manager protected mode.                           *
*            4 Detached protected mode.                                       *
******************************************************************************/
RexxRoutine0(int, SysProcessType)
{
    PPIB ppib;

    if (DosGetInfoBlocks(NULL, &ppib) != 0)
    {
        context->InvalidRoutine();
        return 0;
    }

    return ppib->pib_ultype;
}


/******************************************************************************
* Function:  SysQueryExtLIBPATH                                               *
*                                                                             *
* Syntax:    result = SysQueryExtLIBPATH(flag)                                *
*                                                                             *
* Params:    flag - Indicates which extended LIBPATH will be returned. This   *
*                   must be one of the following:                             *
*                                                                             *
*                   B - Returns the current BEGINLIBPATH.                     *
*                   E - Returns the current ENDLIBPATH.                       *
*                                                                             *
* Return:    Returns the value of the specified extended LIBPATH.             *
******************************************************************************/
RexxRoutine1(RexxStringObject, SysQueryExtLIBPATH, CSTRING, flag)
{
    ULONG osflag;
    CHAR buf[1024];

    switch (flag[0])
    {
        case 'b':
        case 'B':
            osflag = BEGIN_LIBPATH;
            break;
        case 'e':
        case 'E':
            osflag = END_LIBPATH;
            break;
        default:
            context->InvalidRoutine();
            return NULL;
    }

    if (DosQueryExtLIBPATH(buf, osflag) != 0)
    {
        context->InvalidRoutine();
        return NULL;
    }

    return context->NewStringFromAsciiz(buf);
}


/******************************************************************************
* Function:  SysSetExtLIBPATH                                                 *
*                                                                             *
* Syntax:    result = SysQueryExtLIBPATH(path, flag)                          *
*                                                                             *
* Params:    path - The new value of the extended LIBPATH. The path           *
*                   specification may contain the following strings, which    *
*                   are treated as substitution variables:                    *
*                   %BEGINLIBPATH% - The current value of the BEGINLIBPATH.   *
*                   %ENDLIBPATH% - The current value of the ENDLIBPATH.       *
*            flag - Indicates which extended LIBPATH will be returned. This   *
*                   must be one of the following:                             *
*                   B - Returns the current BEGINLIBPATH.                     *
*                   E - Returns the current ENDLIBPATH.                       *
*                                                                             *
* Return:    Returns the operating system return code.                        *
******************************************************************************/
RexxRoutine2(int, SysSetExtLIBPATH, CSTRING, path, CSTRING, flag)
{
    ULONG osflag;

    switch (flag[0])
    {
        case 'b':
        case 'B':
            osflag = BEGIN_LIBPATH;
            break;
        case 'e':
        case 'E':
            osflag = END_LIBPATH;
            break;
        default:
            context->InvalidRoutine();
            return 0;
    }

    return DosSetExtLIBPATH(path, osflag);
}


/******************************************************************************
* Function:  SysQueryProcessCodePage                                          *
*                                                                             *
* Syntax:    result = SysQueryProcessCodePage()                               *
*                                                                             *
* Return:    Returns the codepage number of the current process.              *
******************************************************************************/
RexxRoutine0(int, SysQueryProcessCodePage)
{
    ULONG   cp = 0;
    ULONG   cb = sizeof(cp);

    DosQueryCp(cb, &cp, &cb);
    return cp;
}


/******************************************************************************
* Function:  SysSetProcessCodePage                                            *
*                                                                             *
* Syntax:    result = SysSetProcessCodePage(codepage)                         *
*                                                                             *
* Return:    Returns the operating system return code.                        *
******************************************************************************/
RexxRoutine1(int, SysSetProcessCodePage, int, codepage)
{
    return DosSetProcessCp(codepage);
}


/******************************************************************************
* Function:  SysQuerySwitchList                                               *
*                                                                             *
* Syntax:    call SysQuerySwitchList stem [,flags]                            *
*                                                                             *
* Params:    stem - The name of a stem variable in which the list of entries  *
*                   will be be saved.                                         *
*            flags - A string containing any or all of the following options: *
*                    I - Include invisible entries in the returned list.      *
*                    G - G Include 'grayed' entries in the returned list.     *
*                    N - Include non-jumpable entries in the returned list.   *
*                    D - Use detailed format for list entries. When           *
*                        specified, each returned entry will contain the      *
*                        following fields: PID SID Visible Jumpable Type Name *
*                        PID and SID - integer values representing the        *
*                                      window's process ID and session ID,    *
*                                      respectively.                          *
*                        Visible - is the window's visibility flag, which     *
*                                  will be one of the following:              *
*                                  1 - Invisible                              *
*                                  2 - Grayed                                 *
*                                  4 - Visible                                *
*                        Jumpable - is the window's jumpable flag, which will *
*                                  be one of the following:                   *
*                                  1 - Non-jumpable                           *
*                                  2 - Jumpable                               *
*                        Type - indicates the window's program type, which    *
*                                  will be one of the following:              *
*                                  0 - Default                                *
*                                  1 - VIO full-screen                        *
*                                  2 - VIO window                             *
*                                  3 - Presentation Manager                   *
*                                  4 - DOS/Windows full-screen                *
*                                  7 - DOS/Windows window                     *
*                         Name - is the window title.                         *
*                    The default value is the empty string. Visible and       *
*                    jumpable entries are always returned.                    *
*                                                                             *
* Return:    Returns 0.                                                       *
******************************************************************************/
RexxRoutine2(int, SysQuerySwitchList, RexxObjectPtr, stem, OPTIONAL_CSTRING, flags)
{
    StemHandler list(context, stem, 1);
    HAB hab = WinQueryAnchorBlock(HWND_DESKTOP);
    ULONG cbItems = WinQuerySwitchList(hab, NULL, 0);
    ULONG bufsize = cbItems * sizeof(SWENTRY) + sizeof(ULONG);
    PSWBLOCK pswblk;
    ULONG i;

    bool invisible = false;
    bool grayed = false;
    bool nonjumpable = false;
    bool detailed = false;

    for (; flags && *flags; ++flags)
    {
        switch (*flags)
        {
            case 'i':
            case 'I':
                invisible = true;
                break;
            case 'g':
            case 'G':
                grayed = true;
                break;
            case 'n':
            case 'N':
                nonjumpable = true;
                break;
            case 'd':
            case 'D':
                detailed = true;
                break;
            default:
                context->InvalidRoutine();
                return 0;
        }
    }

    pswblk = (PSWBLOCK)malloc(bufsize);
    if (pswblk == NULL)
    {
        context->InvalidRoutine();
        return 0;
    }

    cbItems = WinQuerySwitchList(hab, pswblk, bufsize);
    for (i = 0; i < pswblk->cswentry; i++)
    {
        SWCNTRL& sw = pswblk->aswentry[i].swctl;
        if (sw.uchVisibility == SWL_VISIBLE ||
           (sw.uchVisibility == SWL_INVISIBLE && invisible) ||
           (sw.uchVisibility == SWL_GRAYED && grayed))
        {
            if (sw.fbJump == SWL_JUMPABLE || nonjumpable)
            {
                if (detailed)
                {
                    char buf[512];
                    snprintf(buf, sizeof(buf), "%u %u %u %u %u %s", sw.idProcess,
                             sw.idSession, sw.uchVisibility, sw.fbJump, sw.bProgType, sw.szSwtitle);
                    list.addValue(buf);
                }
                else
                {
                    list.addValue(sw.szSwtitle);
                }
            }
        }
    }

    free(pswblk);
    return 0;
}


/******************************************************************************
* Function:  SysSetPriority                                                   *
*                                                                             *
* Syntax:    result = SysSetPriority(Class, Level)                            *
*                                                                             *
* Params:    Class - The priority class (0-4 or HIGH,REALTIME,NORMAL,IDLE)    *
*            Level - Amount to change (-31 to +31 or IDLE, LOWEST,...)        *
*                                                                             *
* Return:    Returns the operating system return code.                        *
******************************************************************************/
RexxRoutine2(int, SysSetPriority, RexxObjectPtr, classArg, RexxObjectPtr, levelArg)
{
    int ulClass = PRTYC_NOCHANGE;
    int delta = 0;

    if (context->WholeNumber(classArg, &ulClass))
    {
       if (ulClass > 4 || ulClass < 0)
       {
           context->InvalidRoutine();
           return 0;
       }
    }
    else
    {
        const char *szClass = context->ObjectToStringValue(classArg);

        if (stricmp(szClass, "REALTIME") == 0)
        {
            ulClass = PRTYC_FOREGROUNDSERVER;
        }
        else if (stricmp(szClass, "HIGH") == 0)
        {
            ulClass = PRTYC_TIMECRITICAL;
        }
        else if (!stricmp(szClass, "NORMAL") == 0)
        {
            ulClass = PRTYC_REGULAR;
        }
        else if (stricmp(szClass, "IDLE") == 0)
        {
            ulClass = PRTYC_IDLETIME;
        }
        else
        {
            context->InvalidRoutine();
            return 0;
        }
    }

    if (context->WholeNumber(levelArg, &delta))
    {
        if (delta < PRTYD_MINIMUM || delta > PRTYD_MAXIMUM)
        {
            context->InvalidRoutine();
            return 0;
        }
    }
    else
    {
        const char *szDelta = context->ObjectToStringValue(levelArg);

        if (stricmp(szDelta, "ABOVE_NORMAL") == 0)
        {
            delta = 1;
        }
        else if (stricmp(szDelta, "BELOW_NORMAL") == 0)
        {
            delta = -1;
        }
        else if (stricmp(szDelta, "HIGHEST") == 0)
        {
            delta = 2;
        }
        else if (stricmp(szDelta, "LOWEST") == 0)
        {
            delta = -2;
        }
        else if (stricmp(szDelta, "NORMAL") == 0)
        {
            delta = 0;
        }
        else if (stricmp(szDelta, "IDLE") == 0)
        {
            delta = PRTYD_MINIMUM;
        }
        else if (stricmp(szDelta, "TIME_CRITICAL") == 0)
        {
            delta = PRTYD_MAXIMUM;
        }
        else
        {
            context->InvalidRoutine();
            return 0;
        }
    }

    return DosSetPriority(PRTYS_THREAD, ulClass, delta, 0);
}


/******************************************************************************
* Function:  SysShutDownSystem                                                *
*                                                                             *
* Syntax:    result = SysShutDownSystem()                                     *
*                                                                             *
* Return:    Returns 1 on successful shutdown, or 0 on failure.               *
******************************************************************************/
RexxRoutine5(int, SysShutDownSystem, OPTIONAL_CSTRING, computer, OPTIONAL_CSTRING, message, OPTIONAL_uint32_t, timeout,
             OPTIONAL_logical_t, forceAppsClosed, OPTIONAL_logical_t, reboot)
{
    PPIB ppib;

    DosGetInfoBlocks(NULL, &ppib);
    if (ppib->pib_ultype == 3)
    {
        HAB hab = WinQueryAnchorBlock(HWND_DESKTOP);
        return WinShutdownSystem(hab, HMQ_CURRENT);
    }

    return DosShutdown(0) == 0;
}


/******************************************************************************
* Function:  SysSwitchSession                                                 *
*                                                                             *
* Syntax:    result = SysSwitchSession(name)                                  *
*                                                                             *
* Params:    name   - name of target session                                  *
*                                                                             *
* Return:    Returns the PM return code.                                      *
******************************************************************************/
RexxRoutine1(int, SysSwitchSession, CSTRING, name)
{
    HAB hab = WinQueryAnchorBlock(HWND_DESKTOP);
    ULONG cbItems = WinQuerySwitchList(hab, NULL, 0);
    ULONG bufsize = cbItems * sizeof(SWENTRY) + sizeof(ULONG);
    PSWBLOCK pswblk;
    APIRET rc = PMERR_INVALID_TITLE;
    ULONG i;

    pswblk = (PSWBLOCK)malloc(bufsize);
    if (pswblk == NULL)
    {
        context->InvalidRoutine();
        return 0;
    }

    cbItems = WinQuerySwitchList(hab, pswblk, bufsize);
    for (i = 0; i < pswblk->cswentry; i++)
    {
        if (strcmp(name, pswblk->aswentry[i].swctl.szSwtitle) == 0)
        {
            rc = WinSwitchToProgram(pswblk->aswentry[i].hswitch);
            break;
        }
    }

    free(pswblk);
    return rc;
}


/******************************************************************************
* Function:  SysWaitForShell                                                  *
*                                                                             *
* Syntax:    result = SysWaitForShell(event [,query])                         *
*                                                                             *
* Params:    event - The initialization event to check for. Allowable values  *
*                    are: DESKTOPCREATED, DESKTOPOPENED, DESKTOPPOPULATED.    *
*            query - Optional flag indicating that the status of the          *
*                    specified event is to be be queried only. This parameter *
*                    must take the value "QUERY".                             *
*                                                                             *
* Return:    Returns 1 if event has taken place, or 0 otherwise.              *
******************************************************************************/
RexxRoutine2(int, SysWaitForShell, CSTRING, event, OPTIONAL_CSTRING, query)
{
    ULONG ulEvent;

    if (stricmp(event, "DESKTOPCREATED") == 0)
    {
        ulEvent = WWFS_DESKTOPCREATED;
    }
    else if (stricmp(event, "DESKTOPOPENED") == 0)
    {
        ulEvent = WWFS_DESKTOPOPENED;
    }
    else if (stricmp(event, "DESKTOPPOPULATED") == 0)
    {
        ulEvent = WWFS_DESKTOPPOPULATED;
    }
    else
    {
        context->InvalidRoutine();
        return 0;
    }

    if (query)
    {
        if (stricmp(query, "QUERY") == 0)
        {
            ulEvent |= WWFS_QUERY;
        }
        else
        {
            context->InvalidRoutine();
            return 0;
        }
    }

    return WinWaitForShell(ulEvent);
}


/******************************************************************************
* Function:  SysWaitNamedPipe                                                 *
*                                                                             *
* Syntax:    result = SysWaitNamedPipe(name [,timeout])                       *
*                                                                             *
* Params:    name - name of the pipe                                          *
*            timeout - amount of time to wait.                                *
*                                                                             *
* Return:    Returns the operating system return code.                        *
******************************************************************************/
RexxRoutine2(int, SysWaitNamedPipe, CSTRING, name, OPTIONAL_int, timeout)
{
    if (argumentOmitted(2))
    {
        timeout = NP_DEFAULT_WAIT;
    }
    else if (timeout < -1)
    {
        context->InvalidRoutine();
        return 0;
    }

    return DosWaitNPipe(name, timeout);
}


/******************************************************************************
* Function:  SysWildCard                                                      *
*                                                                             *
* Syntax:    result = SysWildCard(source, wildcard)                           *
*                                                                             *
* Params:    source - Filename string to be transformed.                      *
*            wildcard - Wildcard pattern used to transform the source string. *
*                                                                             *
* Return:    Returns the transformed string. If an error occurs, a null       *
*            string ("") is returned.                                         *
******************************************************************************/
RexxRoutine2(RexxStringObject, SysWildCard, CSTRING, source, CSTRING, wildcard)
{
    char buf[CCHMAXPATH];

    if (DosEditName(1, source, wildcard, (PBYTE)buf, sizeof(buf)) != 0)
    {
        return context->NullString();
    }

    return context->NewStringFromAsciiz(buf);
}


/******************************************************************************
* Function:  SysCreateObject                                                  *
*                                                                             *
* Syntax:    result = SysCreateObject(classname, title, location,             *
*                                     [[,setup] [,option]])                   *
*                                                                             *
* Params:    classname - The name of the Workplace Shell class.               *
*            title - The title of the object being created.                   *
*            location - The location in which the new object will be created. *
*            setup - A Workplace Shell setup string.                          *
*            option - The action to take if the object already exists. The    *
*                     acceptable values are: FAIL, REPLACE, UPDATE. Only the  *
*                     first letter is required.                               *
*                                                                             *
* Return:    Returns 1 for successful object creation, or 0 for failure.      *
******************************************************************************/
RexxRoutine5(int, SysCreateObject, CSTRING, classname, CSTRING, title, CSTRING, location,
             OPTIONAL_CSTRING, setup, OPTIONAL_CSTRING, option)
{
    ULONG ulFlags = CO_FAILIFEXISTS;
    HOBJECT rc;

    if (setup == NULL)
    {
        setup = "";
    }

    if (option)
    {
        switch (option[0])
        {
            case 'F':
            case 'f':
                ulFlags = CO_FAILIFEXISTS;
                break;
            case 'R':
            case 'r':
                ulFlags = CO_REPLACEIFEXISTS;
                break;
            case 'U':
            case 'u':
                ulFlags = CO_UPDATEIFEXISTS;
                break;
            default:
                context->InvalidRoutine();
                return 0;
        }
    }

    rc = WinCreateObject(classname, title, setup, location, ulFlags);
    return (rc != NULLHANDLE);
}


/******************************************************************************
* Function:  SysCreateShadow                                                  *
*                                                                             *
* Syntax:    result = SysCreateObject(object, destination)                    *
*                                                                             *
* Params:    object - The name of the object to shadow.                       *
*            destination - The location in which the shadow will be created.  *
*                                                                             *
* Return:    Returns 1 for successful shadow creation, or 0 for failure.      *
******************************************************************************/
RexxRoutine2(int, SysCreateShadow, CSTRING, object, CSTRING, destination)
{
    HOBJECT hObject;
    HOBJECT hDest;
    HOBJECT rc;

    if ((hObject = WinQueryObject(object)) == NULLHANDLE)
    {
         context->InvalidRoutine();
         return 0;
    }
    if ((hDest = WinQueryObject(destination)) == NULLHANDLE)
    {
         context->InvalidRoutine();
         return 0;
    }

    rc = WinCreateShadow(hObject, hDest, 0);
    return (rc != NULLHANDLE);
}


/******************************************************************************
* Function:  SysCopyObject                                                    *
*                                                                             *
* Syntax:    result = SysCopyObject(object, destination)                      *
*                                                                             *
* Params:    object - The name of the object to copy.                         *
*            destination - The location in which the copy will be created.    *
*                                                                             *
* Return:    Returns 1 if the operation succeeded, or 0 on failure.           *
******************************************************************************/
RexxRoutine2(int, SysCopyObject, CSTRING, object, CSTRING, destination)
{
    HOBJECT hObject;
    HOBJECT hDest;
    HOBJECT rc;

    if ((hObject = WinQueryObject(object)) == NULLHANDLE)
    {
         context->InvalidRoutine();
         return 0;
    }
    if ((hDest = WinQueryObject(destination)) == NULLHANDLE)
    {
         context->InvalidRoutine();
         return 0;
    }

    rc = WinCopyObject(hObject, hDest, 0);
    return (rc != NULLHANDLE);
}


/******************************************************************************
* Function:  SysMoveObject                                                    *
*                                                                             *
* Syntax:    result = SysMoveObject(object, destination)                      *
*                                                                             *
* Params:    object - The name of the object to move.                         *
*            destination - The name of the target folder to which the         *
*                          specified object will be moved.                    *
*                                                                             *
* Return:    Returns 1 if the operation succeeded, or 0 on failure.           *
******************************************************************************/
RexxRoutine2(int, SysMoveObject, CSTRING, object, CSTRING, destination)
{
    HOBJECT hObject;
    HOBJECT hDest;
    HOBJECT rc;

    if ((hObject = WinQueryObject(object)) == NULLHANDLE)
    {
         context->InvalidRoutine();
         return 0;
    }
    if ((hDest = WinQueryObject(destination)) == NULLHANDLE)
    {
         context->InvalidRoutine();
         return 0;
    }

    rc = WinMoveObject(hObject, hDest, 0);
    return (rc != NULLHANDLE);
}


/******************************************************************************
* Function:  SysDestroyObject                                                 *
*                                                                             *
* Syntax:    result = SysDestroyObject(name)                                  *
*                                                                             *
* Params:    name - The name of the object to destroy.                        *
*                                                                             *
* Return:    Returns 1 for successful object deletion, or 0 foir failure.     *
******************************************************************************/
RexxRoutine1(int, SysDestroyObject, CSTRING, name)
{
    HOBJECT hObject;

    if ((hObject = WinQueryObject(name)) == NULLHANDLE)
    {
         return 0;
    }

    return WinDestroyObject(hObject);
}


/******************************************************************************
* Function:  SysOpenObject                                                    *
*                                                                             *
* Syntax:    result = SysOpenObject(name, view, flag)                         *
*                                                                             *
* Params:    name - The name of the object to open, which must exist.         *
*            view - The view in which to open the object, which can be one of *
*                   the standard WPS views or a user-defined view. The view   *
*                   may be given as an integer value, or as one of the        *
*                   following: SETTINGS, DEFAULT, TREE, ICON, RUNNING, HELP,  *
*                   DETAILS, PALETTE, PROMPTDLG.                              *
*            flag - Specifies whether to open an existing view of the object, *
*                   or a new view. Must be one of: TRUE, FALSE                *
*                                                                             *
* Return:    Returns 1 if the object was opened successfully, or 0 on failure *
******************************************************************************/
RexxRoutine3(int, SysOpenObject, CSTRING, name, RexxObjectPtr, view, CSTRING, flag)
{
    HOBJECT hObject;
    int ulView;
    BOOL fFlags;

    if ((hObject = WinQueryObject(name)) == NULLHANDLE)
    {
         context->InvalidRoutine();
         return 0;
    }

    if (!context->WholeNumber(view, &ulView))
    {
        const char *szView = context->ObjectToStringValue(view);

        if (stricmp(szView, "SETTINGS") == 0)
        {
            ulView = OPEN_SETTINGS;
        }
        else if (stricmp(szView, "DEFAULT") == 0)
        {
            ulView = OPEN_DEFAULT;
        }
        else if (stricmp(szView, "TREE") == 0)
        {
            ulView = OPEN_TREE;
        }
        else if (stricmp(szView, "ICON") == 0)
        {
            ulView = OPEN_CONTENTS;
        }
        else if (stricmp(szView, "DETAILS") == 0)
        {
            ulView = OPEN_DETAILS;
        }
        else if (stricmp(szView, "RUNNING") == 0)
        {
            ulView = OPEN_RUNNING;
        }
        else if (stricmp(szView, "HELP") == 0)
        {
            ulView = OPEN_HELP;
        }
        else if (stricmp(szView, "PALETTE") == 0)
        {
            ulView = OPEN_PALETTE;
        }
        else if (stricmp(szView, "PROMPTDLG") == 0)
        {
            ulView = OPEN_PROMPTDLG;
        }
        else
        {
            context->InvalidRoutine();
            return 0;
        }
    }

    if (stricmp(flag, "TRUE") == 0)
    {
        fFlags = TRUE;
    }
    else if (stricmp(flag, "FALSE") == 0)
    {
        fFlags = FALSE;
    }
    else
    {
        invalidOptionException(context, "SysOpenObject", "flag", "'TRUE' or 'FALSE'", flag);
        return 0;
    }

    return WinOpenObject(hObject, ulView, fFlags);
}


/******************************************************************************
* Function:  SysSaveObject                                                    *
*                                                                             *
* Syntax:    result = SysSaveObject(name, async)                              *
*                                                                             *
* Params:    name - The name of the object to save, which must exist.         *
*            async - Specifies whether or not to save the object              *
*                    asynchronously. Must be one of: TRUE, FALSE, 1, 0, A, S  *
*                                                                             *
* Return:    Returns 1 if the object was saved successfully, or 0 on failure. *
******************************************************************************/
RexxRoutine2(int, SysSaveObject, CSTRING, name, CSTRING, async)
{
    HOBJECT hObject;
    BOOL fAsync;

    if ((hObject = WinQueryObject(name)) == NULLHANDLE)
    {
        context->InvalidRoutine();
        return 0;
    }

    if (stricmp(async, "FALSE") == 0 || stricmp(async, "0") == 0 || stricmp(async, "S") == 0)
    {
        fAsync = FALSE;
    } else {
        fAsync = TRUE;
    }

    return WinSaveObject(hObject, fAsync);
}


/******************************************************************************
* Function:  SysSetIcon                                                       *
*                                                                             *
* Syntax:    result = SysSetIcon(filename, iconfilename)                      *
*                                                                             *
* Params:    filename - The name of the target file whose icon is to be set.  *
*            iconfilename - he name of the OS/2 icon file.                    *
*                                                                             *
* Return:    Returns 1 if the icon was set successfully, or 0 otherwise.      *
******************************************************************************/
RexxRoutine2(int, SysSetIcon, CSTRING, filename, CSTRING, iconfilename)
{
    ICONINFO iconinfo = {sizeof(iconinfo)};

    iconinfo.fFormat = ICON_FILE;
    iconinfo. pszFileName = (PSZ)iconfilename;

    return WinSetFileIcon(filename, &iconinfo);
}


/******************************************************************************
* Function:  SysSetObjectData                                                 *
*                                                                             *
* Syntax:    result = SysSetObjectData(name, setup)                           *
*                                                                             *
* Params:    name - The name of the object whose settings are to be changed,  *
*                   which must exist                                          *
*            setup - A Workplace Shell setup string.                          *
*                                                                             *
* Return:    Returns 1 if the object updated successfully, or 0 on failure.   *
******************************************************************************/
RexxRoutine2(int, SysSetObjectData, CSTRING, name, CSTRING, setup)
{
    HOBJECT hObject;

    if ((hObject = WinQueryObject(name)) == NULLHANDLE)
    {
         context->InvalidRoutine();
         return 0;
    }

    return WinSetObjectData(hObject, setup);
}


/******************************************************************************
* Function:  SysQueryClassList                                                *
*                                                                             *
* Syntax:    call SysQueryClassList stem                                      *
*                                                                             *
* Params:    stem - The name of a stem variable in which the list of          *
*                   classes will be saved.                                    *
*                                                                             *
* Return:    does not return a meaningful value.                              *
******************************************************************************/
RexxRoutine1(int, SysQueryClassList, RexxObjectPtr, stem)
{
    StemHandler list(context, stem, 1);
    POBJCLASS buf = NULL;
    ULONG bufsize = 0;

    if (WinEnumObjectClasses(NULL, &bufsize))
    {
        buf = (POBJCLASS)malloc(bufsize);
        if (buf == NULL)
        {
            outOfMemoryException(context);
        }
        if (WinEnumObjectClasses(buf, &bufsize))
        {
           POBJCLASS pClass = buf;
           CHAR resstr[CCHMAXPATH*2];

           while (pClass)
           {
                snprintf(resstr, sizeof(resstr), "%s %s", pClass->pszClassName, pClass->pszModName);
                list.addValue(resstr);
                pClass = pClass->pNext;
           }
        }
        free(buf);
    }

  return 0;
}


/******************************************************************************
* Function:  SysRegisterObjectClass                                           *
*                                                                             *
* Syntax:    result = SysRegisterObjectClass(classname, module)               *
*                                                                             *
* Params:    classname - The name of the new object class.                    *
*            module - The name of the DLL containing the object class         *
*                     definition                                              *
*                                                                             *
* Return:    Returns 1 for successful registration, or 0 for failure.         *
******************************************************************************/
RexxRoutine2(int, SysRegisterObjectClass, CSTRING, classname, CSTRING, module)
{
    return WinRegisterObjectClass(classname, module);
}


/******************************************************************************
* Function:  SysDeregisterObjectClass                                         *
*                                                                             *
* Syntax:    result = SysDeregisterObjectClass(classname)                     *
*                                                                             *
* Params:    classname - The object class name.                               *
*                                                                             *
* Return:    Returns 1 for successful deregistration, or 0 for failure.       *
******************************************************************************/
RexxRoutine1(int, SysDeregisterObjectClass, CSTRING, classname)
{
    return WinDeregisterObjectClass(classname);
}


/******************************************************************************
* Function:  SysSystemDirectory                                               *
*                                                                             *
* Syntax:    result = SysSystemDirectory()                                    *
*                                                                             *
* Return:    'D:\OS2 ...'                                                     *
******************************************************************************/
RexxRoutine0(RexxStringObject, SysSystemDirectory)
{
    ULONG drive;
    char def[7] = "X:\\OS2";
    PSZ osdir;

    if (DosScanEnv("OSDIR", &osdir) == 0)
    {
        return context->NewStringFromAsciiz(osdir);
    }

    if (DosQuerySysInfo(QSV_BOOT_DRIVE, QSV_BOOT_DRIVE, &drive, sizeof(drive)) == 0)
    {
        def[0] = drive + 'A' - 1;
        return context->NewStringFromAsciiz(def);
    }
    else
    {
        return context->NullString();
    }
}


/******************************************************************************
* Function:  SysFileSystemType                                                *
*                                                                             *
* Syntax:    result = SysFileSystemType([drive])                              *
*                                                                             *
* Params:    drive - d, d:,                                                   *
*                    if omitted, defaults to current drive                    *
*                                                                             *
* Return:    result - the drive's file system (e. g. NTFS, FAT)               *
*                     null string for any error                               *
******************************************************************************/
RexxRoutine1(RexxStringObject, SysFileSystemType, OPTIONAL_CSTRING, drive)
{
    // Return-data buffer should be large enough to hold FSQBUFFER2
    // and the maximum data for szName, szFSDName, and rgFSAData
    // Typically, the data isn't that large.

    BYTE fsqBuffer[sizeof(FSQBUFFER2) + (3*CCHMAXPATH)] = {0};
    PFSQBUFFER2 pfsqBuffer = (PFSQBUFFER2)fsqBuffer;

    ULONG  cbBuffer        = sizeof(fsqBuffer);
    PCHAR  pszFSDName      = NULL;
    CHAR   szDeviceName[3] = "x:";
    APIRET rc;

    if (drive != NULL)
    {
        if (!isalpha(drive[0]) || (strlen(drive) > 1 && drive[1] != ':'))
        {
            context->InvalidRoutine();
            return NULL;
        }

        szDeviceName[0] = drive[0];
    } else {
        ULONG drivenum, logical;
        DosQueryCurrentDisk(&drivenum, &logical);
        szDeviceName[0] = drivenum - 1 + 'A';
    }

    DosError(FERR_DISABLEHARDERR);
    rc = DosQueryFSAttach(szDeviceName, 0, FSAIL_QUERYNAME, pfsqBuffer, &cbBuffer);
    DosError(FERR_ENABLEHARDERR);

    if (rc == 0)
    {
        pszFSDName = (PCHAR)pfsqBuffer->szName + pfsqBuffer->cbName + 1;
        return context->NewStringFromAsciiz(pszFSDName);
    }

    return context->NullString();
}


/******************************************************************************
* Function:  SysVolumeLabel                                                   *
*                                                                             *
* Syntax:    result = SysVolumeLabel([drive])                                 *
*                                                                             *
* Params:    drive - d, d:, d:\,                                              *
*                    if omitted, defaults to current drive                    *
*                                                                             *
* Return:    result - the volume label                                        *
*                     null string for any error                               *
******************************************************************************/
RexxRoutine1(RexxStringObject, SysVolumeLabel, OPTIONAL_CSTRING, drive)
{
    ULONG drivenum = 0;
    FSINFO FSInfo;
    APIRET rc;

    if (drive != NULL)
    {
        if (!isalpha(drive[0]) || (strlen(drive) > 1 && drive[1] != ':'))
        {
            context->InvalidRoutine();
            return NULL;
        }

        drivenum = toupper(drive[0])-'A'+1;
    } else {
        ULONG logical;
        DosQueryCurrentDisk(&drivenum, &logical);
    }

    DosError(FERR_DISABLEHARDERR);
    rc = DosQueryFSInfo(drivenum, FSIL_VOLSER, &FSInfo, sizeof(FSInfo));
    DosError(FERR_ENABLEHARDERR);

    if (rc == 0)
    {
        return context->NewStringFromAsciiz(FSInfo.vol.szVolLabel);
    }

    return context->NullString();
}


/******************************************************************************
*  below are OS/2-specific implementations of TreeFinder methods.             *
******************************************************************************/


/**
 * If this ends in a directory separator, add a * wildcard to the end
 */
void TreeFinder::adjustDirectory()
{
    // if this ends in a directory separator, add a * wildcard to the end
    if (fileSpec.endsWith('\\') || fileSpec.endsWith('/'))
    {
        fileSpec += "*";
    }
    // just a . or .. is wildcarded also
    else if (strcmp(fileSpec, ".") == 0 || strcmp(fileSpec, "..") == 0)
    {
        fileSpec += "\\*";
    }
    // if the end section is either \. or \.., we also add the wildcard
    else if (fileSpec.endsWith("\\.") || fileSpec.endsWith("\\..")  || fileSpec.endsWith("/.") || fileSpec.endsWith("/.."))
    {
        fileSpec += "\\*";
    }
}


/**
 * Perform any platform-specific adjustments to the platform specification.
 */
void TreeFinder::adjustFileSpec()
{
    // this is a NOP for OS/2 file systems
}


/**
 * Tests for illegal file name characters in fileSpec
 *
 * note   Double quotes in the file spec is not valid, spaces in file names do
 *        not need to be within quotes in the string passed to the Windows API.
 *
 *        A ':' is only valid if it is the second character. Technically a '*'
 *        and a '?' are not valid characters for a file name, but they are okay
 *        for fSpec. Same thing for '\' and '/', they are not valid in a file
 *        name, but they are valid in fSpec. A '/' is iffy. The OS/2 API
 *        accepts '/' as a directory separator, but most Rexx programmers
 *        probably don't know that.  Not sure if we should flag that as illegal
 *        or not.
 */
void TreeFinder::validateFileSpecName()
{
    const char illegal[] = "<>|\"";

    for (size_t i = 0; i < strlen(illegal); i++)
    {
        if (strchr((const char *)fileSpec, illegal[i]) != NULL)
        {
            throw InvalidFileName;
        }
    }

    const char *pos = strchr((const char *)fileSpec, ':');
    if (pos != NULL)
    {
        size_t p = pos - (const char *)fileSpec + 1;
        if (p != 2)
        {
            throw InvalidFileName;
        }

        if (strchr(pos + 1, ':') != NULL)
        {
            throw InvalidFileName;
        }
    }
}


/**
 * SysFileTree helper routine to see if this is a file we need to include
 * in the result set.
 *
 * param   attr - The file attributes
 *
 * return  true if the file should be included, false otherwise.
 */
bool checkInclusion(TreeFinder *finder, ULONG attr)
{
    // If this is a directory and we're not looking for dirs, this is a pass
    if ((attr & FILE_DIRECTORY) && !finder->includeDirs())
    {
        return false;
    }

    // File is not a DIR and we're only looking for directories
    if (!(attr & FILE_DIRECTORY) && !finder->includeFiles())
    {
        return false;
    }

    // If no mask is set, then everything is good
    if (finder->acceptAll())
    {
        return true;
    }

    if (!finder->archiveSelected((attr & FILE_ARCHIVED) != 0))
    {
        return  false;
    }
    // A little silly since this overlaps with the options
    if (!finder->directorySelected((attr & FILE_DIRECTORY) != 0))
    {
        return  false;
    }
    if (!finder->hiddenSelected((attr & FILE_HIDDEN) != 0))
    {
        return  false;
    }
    if (!finder->readOnlySelected((attr & FILE_READONLY) != 0))
    {
        return  false;
    }
    if (!finder->systemSelected((attr & FILE_SYSTEM) != 0))
    {
        return  false;
    }

    return  true;
}


/**
 * Returns a new file attribute value given a mask of attributes to be changed
 * and the current attribute value.
 */
ULONG mergeAttrs(TreeFinder::AttributeMask &newattr, ULONG attr)
{
    // if no settings to process, return the old set
    if (newattr.noNewSettings())
    {
        return attr;
    }

    if (newattr.isOff(TreeFinder::AttributeMask::Archive))
    {
        attr &= ~FILE_ARCHIVED;
    }
    else if (newattr.isOn(TreeFinder::AttributeMask::Archive))
    {
        attr |= FILE_ARCHIVED;
    }

    if (newattr.isOff(TreeFinder::AttributeMask::Hidden))
    {
        attr &= ~FILE_HIDDEN;
    }
    else if (newattr.isOn(TreeFinder::AttributeMask::Hidden))
    {
        attr |= FILE_HIDDEN;
    }

    if (newattr.isOff(TreeFinder::AttributeMask::ReadOnly))
    {
        attr &= ~FILE_READONLY;
    }
    else if (newattr.isOn(TreeFinder::AttributeMask::ReadOnly))
    {
        attr |= FILE_READONLY;
    }

    if (newattr.isOff(TreeFinder::AttributeMask::System))
    {
        attr &= ~FILE_SYSTEM;
    }
    else if (newattr.isOn(TreeFinder::AttributeMask::System))
    {
        attr |= FILE_SYSTEM;
    }

    return  attr;
}


/**
 * Format the system-specific file time, attribute mask, and size for
 * the given file.
 */
void formatAttrs(TreeFinder *finder, FileNameBuffer &foundFile, SysFileIterator::FileAttributes &attributes)
{
    char fileAttrs[256];

    if (finder->longTime())
    {
        snprintf(fileAttrs, sizeof(fileAttrs), "%4d-%02d-%02d %02d:%02d:%02d  ",
                 attributes.fs.fdateLastWrite.year + 1980,
                 attributes.fs.fdateLastWrite.month,
                 attributes.fs.fdateLastWrite.day,
                 attributes.fs.ftimeLastWrite.hours,
                 attributes.fs.ftimeLastWrite.minutes,
                 attributes.fs.ftimeLastWrite.twosecs*2);

    }
    else if (finder->editableTime())
    {
        snprintf(fileAttrs, sizeof(fileAttrs), "%02d/%02d/%02d/%02d/%02d  ",
                (attributes.fs.fdateLastWrite.year + 1980) % 100, 
                 attributes.fs.fdateLastWrite.month,
                 attributes.fs.fdateLastWrite.day,
                 attributes.fs.ftimeLastWrite.hours,
                 attributes.fs.ftimeLastWrite.minutes);
    }
    else
    {
        int hours = attributes.fs.ftimeLastWrite.hours;
    
        snprintf(fileAttrs, sizeof(fileAttrs), "%2d/%02d/%02d  %2d:%02d%c  ",
                 attributes.fs.fdateLastWrite.month,
                 attributes.fs.fdateLastWrite.day,
                (attributes.fs.fdateLastWrite.year + 1980) % 100,
                (hours < 13 && hours != 0) ? hours : (abs(hours - 12)),
                 attributes.fs.ftimeLastWrite.minutes,
                (hours < 12 || hours == 24) ? 'a' : 'p');
    }

    // This is the first part of the return value. Copy to the result buffer so we can
    // reuse the buffer
    foundFile = fileAttrs;

    // Now the size information
    if (finder->longSize())
    {
        snprintf(fileAttrs, sizeof(fileAttrs), "%20llu  ", attributes.fs.cbFile);
    }
    else if (attributes.fs.cbFile > 9999999999)
    {
        strcpy(fileAttrs, "9999999999  ");
    }
    else
    {
        snprintf(fileAttrs, sizeof(fileAttrs), "%10llu  ", attributes.fs.cbFile);
    }

    // The order is time, size, attributes
    foundFile += fileAttrs;

    snprintf(fileAttrs, sizeof(fileAttrs), "%c%c%c%c%c  ",
             (attributes.fs.attrFile & FILE_ARCHIVED) ? 'A' : '-',
             (attributes.fs.attrFile & FILE_DIRECTORY) ? 'D' : '-',
             (attributes.fs.attrFile & FILE_HIDDEN) ? 'H' : '-',
             (attributes.fs.attrFile & FILE_READONLY) ? 'R' : '-',
             (attributes.fs.attrFile & FILE_SYSTEM) ? 'S' : '-');

    // Add on this section
    foundFile += fileAttrs;
}


/**
 * Checks if this file should be part of the included result and adds it to the result set
 * if it should be returned.
 */
void TreeFinder::checkFile(SysFileIterator::FileAttributes &attributes)
{
    // check to see if this one should be included. If not, we just return without doing anythign
    if (checkInclusion(this, attributes.fs.attrFile))
    {
        if (!newAttributes.noNewSettings())
        {
            FILESTATUS3L fs; 

            if (QueryPathInfo(foundFile, FIL_STANDARDL,  &fs, sizeof(fs)) == 0)
            {
                fs.attrFile = mergeAttrs(newAttributes, fs.attrFile);
                DosSetPathInfo(foundFile, FIL_STANDARDL,  &fs, sizeof(fs), 0);
            }
        }

        if (nameOnly())
        {
            // If only the name is requested, create a string object and set the
            // stem varaiable
            addResult(foundFile);
        }
        else
        {
            // Format all of the attributes and add them to the foundFile result
            formatAttrs(this, foundFileLine, attributes);
            // and finally add on the file name
            foundFileLine += foundFile;
            // add this to the stem
            addResult(foundFileLine);
        }
    }
}


/**
 * Platform-specific TreeFinder method for locating the end of the
 * fileSpec directory
 *
 * return  The position of the end of the directory. -1 indicates the file spec
 *         does not contain a directory.
 */
int TreeFinder::findDirectoryEnd()
{
    // Get the maximum position of the last '\'
    int lastSlashPos = (int)fileSpec.length();

    // Step back through fileSpec until at its beginning or at a '\' or '/' character
    while (fileSpec.at(lastSlashPos) != '\\' && fileSpec.at(lastSlashPos) != '/' && lastSlashPos >= 0)
    {
        --lastSlashPos;
    }

    return lastSlashPos;
}


/**
 * Perform platform-specific drive checks on the file spec. This only
 * applies to Windows and OS/2.
 *
 * @return true if this was processed, false if additional work is required.
 */
bool TreeFinder::checkNonPathDrive()
{
    // fileSpec could be a drive designator.
    if (fileSpec.at(1) == ':')
    {
        RoutineFileNameBuffer currentDirectory(context);
        SysFileSystem::getCurrentDirectory(currentDirectory);

        char drive[4] = { 0 };

        // Just copy the drive letter and the colon, omit the rest.  This is
        // necessary e.g. for something like "I:*"
        memcpy(drive, (char*)fileSpec, 2);

        // Change to the specified drive, get the current directory, then go
        // back to where we came from.
        SysFileSystem::setCurrentDirectory(drive);
        SysFileSystem::getCurrentDirectory(filePath);
        SysFileSystem::setCurrentDirectory(currentDirectory);

        // everything after the drive delimiter is the search name.
        nameSpec = ((const char *)fileSpec) + 2;
        return true;
    }
    // not a drive, this will need to be prepended by the current directory
    return false;
}


/**
 * Perform any platform-specific fixup that might be required with
 * the supplied path.
 */
void TreeFinder::fixupFilePath()
{
    // Now go resolve current directories, userid, etc. to get a fully
    // qualified name
    RoutineQualifiedName qualifiedName(context, filePath);

    filePath = (const char *)qualifiedName;
    // Make sure this is terminated with a path delimiter
    filePath.addFinalPathDelimiter();
}
