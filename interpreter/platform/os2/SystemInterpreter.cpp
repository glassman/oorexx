/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2021 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include "SystemInterpreter.hpp"
#include "FileNameBuffer.hpp"
#include "SysProcess.hpp"


/**
 * Handle system-specific once-per-process startup tasks.
 */
void SystemInterpreter::processStartup()
{
    // now do the platform independent startup
    Interpreter::processStartup();
}


/**
 * Handle once-per-process shutdown tasks.
 */
void SystemInterpreter::processShutdown()
{
    // now do the platform independent shutdown
    Interpreter::processShutdown();
}


/**
 * Handle any platform-specific tasks associated with starting
 * an interpreter instance.
 */
void SystemInterpreter::startInterpreter()
{
}


/**
 * Handle any platform-specific tasks associated with
 * terminating an interpreter instance.
 */
void SystemInterpreter::terminateInterpreter()
{
}


/**
 * Perform any additional garbage collection marking that might
 * be required for platform-specific interpreter instance objects.
 */
void SystemInterpreter::live(size_t liveMark)
{
}


/**
 * Perform any additional garbage collection marking that might
 * be required for platform-specific interpreter instance objects.
 */
void SystemInterpreter::liveGeneral(MarkReason reason)
{
}


/**
 * Retrieve the value of an envinment variable into a smart buffer.
 *
 * @param variable The name of the environment variable.
 * @param buffer   The buffer used for the return.
 *
 * @return true if the variable exists, false otherwise.
 */
bool SystemInterpreter::getEnvironmentVariable(const char *variable, FileNameBuffer &buffer)
{
    CHAR szLibPath[1024];
    PSZ value = szLibPath;
    APIRET rc;

    // first check the extended LIBPATH variables as they require
    // special processing.
    if (stricmp(variable, "BEGINLIBPATH") == 0)
    {
        rc = DosQueryExtLIBPATH(szLibPath, BEGIN_LIBPATH);
    }
    else if(stricmp(variable, "ENDLIBPATH") == 0)
    {
        rc = DosQueryExtLIBPATH(szLibPath, END_LIBPATH);
    }
    else if(stricmp(variable, "LIBPATHSTRICT") == 0)
    {
        rc = DosQueryExtLIBPATH(szLibPath, LIBPATHSTRICT);
        szLibPath[1] = 0;
    }
    // finally, search an environment segment for an environment variable.
    else
    {
        rc = DosScanEnv(variable, &value);
    }

    if (rc == 0)
    {
        buffer = value;
        return true;
    }

    // make sure this is a null string
    buffer = "";
    return false;
}


/**
 * Set an environment variable to a new value.
 *
 * @param variableName
 *               The name of the environment variable.
 * @param value  The variable value.
 */
int SystemInterpreter::setEnvironmentVariable(const char *variableName, const char *value)
{
    return SysProcess::setEnvironmentVariable(variableName, value);
}
