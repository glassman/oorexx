/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2021 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#define  INCL_DOS
#define  INCL_WIN
#include <os2.h>
#include "SystemInterpreter.hpp"


/**
 * gets the time and date from the system clock
 *
 * @param Date   The date/time object that is used to return the time information.
 */
void SystemInterpreter::getCurrentTime(RexxDateTime *Date)
{
    DATETIME datetime;

    if (DosGetDateTime(&datetime) == 0)
    {
        Date->hours = datetime.hours;
        Date->minutes = datetime.minutes;
        Date->seconds = datetime.seconds;
        Date->microseconds = datetime.hundredths * 10000UL;
        Date->day = datetime.day;
        Date->month = datetime.month;
        Date->year = datetime.year;
        Date->timeZoneOffset = datetime.timezone * 60000000UL;
    }
}


/**
 * starts a timer and waits for it to expire.
 * An event semaphore is created that can be
 * used to cancel the timer.
 *
 * numdays - number of whole days until timer should expire.
 * alarmtime - fractional portion (less than a
 *             day) until timer should expire, expressed in
 *             milliseconds.
 */
RexxMethod2(int, alarm_startTimer, wholenumber_t, numdays, wholenumber_t, alarmtime)
{
    int msecInADay = 86400000;  // number of milliseconds in a day
    HEV eventSemHandle;
    ULONG ulPostCount;

    // Create an event semaphore that can be used to cancel the alarm.
    if (DosCreateEventSem(NULL, &eventSemHandle, 0, FALSE) != 0)
    {
        context->RaiseException0(Rexx_Error_System_service);
        return 0;
    }

    // set the state variables
    context->SetObjectVariable("EVENTSEMHANDLE", context->NewPointer((POINTER)eventSemHandle));
    context->SetObjectVariable("TIMERSTARTED", context->True());

    while (numdays > 0)
    {
        // is it some future day?
        // use the semaphore to wait for an entire day.
        // if this returns true, then this was not a timeout, which
        // probably means this was cancelled.
        WinWaitEventSem(eventSemHandle, msecInADay);
        // Check if the alarm is canceled.
        RexxObjectPtr cancelObj = context->GetObjectVariable("CANCELED");

        if (cancelObj == context->True())
        {
            DosCloseEventSem(eventSemHandle);
            return 0;
        }
        else
        {
            DosResetEventSem(eventSemHandle, &ulPostCount);
        }
        numdays--;
    }

    // now we can just wait for the alarm time to expire
    WinWaitEventSem(eventSemHandle, alarmtime);
    DosCloseEventSem(eventSemHandle);
    return 0;
}


/**
 * stops an asynchronous timer.
 *
 * eventSemHandle - handle to event semaphore
 *                  used to signal the timer should be canceled.
 */
RexxMethod1(int, alarm_stopTimer, POINTER, eventSemHandle)
{
    // Post the event semaphore to signal the alarm should be canceled. */
    if (DosPostEventSem((HEV)eventSemHandle) != 0)
    {
        context->RaiseException0(Rexx_Error_System_service);
    }
    return 0;
}


/**
 * set up for running a timer.
 */
RexxMethod0(int, ticker_createTimer)
{
    HEV eventSemHandle;

    // create an event semaphore that we use to wait and also for canceling the alarm
    // Create an event semaphore that can be used to cancel the alarm.
    if (DosCreateEventSem(NULL, &eventSemHandle, 0, FALSE) != 0)
    {
        context->RaiseException0(Rexx_Error_System_service);
        return 0;
    }
    // store this in state variables
    context->SetObjectVariable("EVENTSEMHANDLE", context->NewPointer((POINTER)eventSemHandle));
    context->SetObjectVariable("TIMERSTARTED", context->True());
    return 0;
}


/**
 * starts a timer and waits for it to expire.
 * An event semaphore is created that can be
 * used to cancel the timer.
 *
 * numdays - number of whole days until timer should expire.
 * alarmtime - fractional portion (less than a
 *             day) until timer should expire, expressed in
 *             milliseconds.
 */
RexxMethod3(int, ticker_waitTimer, POINTER, eventSemHandle, wholenumber_t, numdays, wholenumber_t, alarmtime)
{
    int msecInADay = 86400000;  // number of milliseconds in a day
    ULONG ulPostCount;

    while (numdays > 0)
    {
        // is it some future day?
        // use the semaphore to wait for an entire day.
        // if this returns true, then this was not a timeout, which
        // probably means this was cancelled.
        WinWaitEventSem((HEV)eventSemHandle, msecInADay);
        // Check if the alarm is canceled.
        RexxObjectPtr cancelObj = context->GetObjectVariable("CANCELED");

        if (cancelObj == context->True())
        {
            DosCloseEventSem((HEV)eventSemHandle);
            return 0;
        }
        else
        {
            DosResetEventSem((HEV)eventSemHandle, &ulPostCount);
        }
        numdays--;
    }

    // now we can just wait for the alarm time to expire
    WinWaitEventSem((HEV)eventSemHandle, alarmtime);
    DosCloseEventSem((HEV)eventSemHandle);
    return 0;
}


/**
 * stops an asynchronous timer.
 *
 * eventSemHandle - handle to event semaphore
 *                  used to signal the timer should be canceled.
 */
RexxMethod1(int, ticker_stopTimer, POINTER, eventSemHandle)
{
    // Post the event semaphore to signal the alarm should be canceled. */
    if (DosPostEventSem((HEV)eventSemHandle) != 0)
    {
        context->RaiseException0(Rexx_Error_System_service);
    }
    return 0;
}
