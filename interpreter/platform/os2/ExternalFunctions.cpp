/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2021 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include "SystemInterpreter.hpp"
#include "SysThread.hpp"
#include "FileNameBuffer.hpp"
#include "BufferClass.hpp"
#include "RexxActivation.hpp"
#include "PackageManager.hpp"
#define  OS2EMX_PLAIN_CHAR
#define  INCL_DOS
#define  INCL_WIN
#include <os2.h>

typedef struct _ENVENTRY {               /* setlocal/endlocal structure  */
    size_t   size;                       /* size of the saved memory     */
} ENVENTRY;

/*********************************************************************/
/*                                                                   */
/*   Subroutine Name:   BuildEnvlist                                 */
/*                                                                   */
/*   Descriptive Name:  Build saved environment block                */
/*                                                                   */
/*   Function:          Builds a block containing all of the         */
/*                      environment variables, the current drive     */
/*                      and the current directory.                   */
/*                                                                   */
/*********************************************************************/
RexxObject* SystemInterpreter::buildEnvlist()
{
    BufferClass *newBuffer;              /* Buffer object to hold env  */
    size_t      size = 0;                /* size of the new buffer     */

    PTIB   ptib;
    PPIB   ppib;
    char*  pmem;
    size_t envsize = 0;

    if (DosGetInfoBlocks(&ptib, &ppib) != 0)
    {
        return OREF_NULL;                /* no envrionment !           */
    }

    for (pmem = ppib->pib_pchenv; *pmem;)
    {
        size_t len = strlen(pmem) + 1;
        envsize += len;
        pmem += len;
    }
    ++envsize;                           /* and its terminating '\0'   */

    FileNameBuffer currdir;
    size_t dirlen;

    // start with a copy of the current working directory
    SysFileSystem::getCurrentDirectory(currdir);
    dirlen = currdir.length();

    size = envsize;
    size += dirlen;                      /* add the space for curr dir */
    size++;                              /* and its terminating '\0'   */
    size += sizeof(size_t);              /* this is for the size itself*/

    // Now we have the size for allocating the new buffer
    newBuffer = new_buffer(size);
    // Get starting address of buf
    pmem = newBuffer->getData();
    ((ENVENTRY *)pmem)->size = size;     /* first write the size       */
    pmem += sizeof(size_t);              /* update the pointer         */
    strcpy(pmem, (const char *)currdir); /* add the current dir to buf */
    pmem += dirlen + 1;                  /* update the pointer         */
    memcpy(pmem, ppib->pib_pchenv, envsize);

    return newBuffer;                    /* return the pointer         */
}


/*********************************************************************/
/*                                                                   */
/*   Subroutine Name:   RestoreEnvironment                           */
/*                                                                   */
/*   Descriptive Name:  restores environment saved by Setlocal()     */
/*                                                                   */
/*   Function:          restores the environment variables, current  */
/*                      directory and drive.                         */
/*                                                                   */
/*********************************************************************/
void SystemInterpreter::restoreEnvironment(void *CurrentEnv)
{
    PTIB   ptib;
    PPIB   ppib;
    char*  pmem;
    size_t size;                         /* size of the saved space    */
    ULONG  envsize = 65536;
    ULONG  envflag = 0;

    pmem = (char *)CurrentEnv;           /* get the saved space        */
    size = ((ENVENTRY *)pmem)->size;     /* first read out the size    */
    pmem += sizeof(size_t);              /* update the pointer         */

    if (!SysFileSystem::setCurrentDirectory(pmem))
    {
        char msg[1024];
        sprintf(msg, "Error restoring current directory: %s", pmem);
        reportException(Error_System_service_service, msg);
    }
    pmem += strlen(pmem) + 1;            /* update the pointer         */
    size -= (pmem - (char *)CurrentEnv);

    if ((DosGetInfoBlocks(&ptib, &ppib) == 0) &&
        (DosQueryMem(ppib->pib_pchenv, &envsize, &envflag) == 0))
    {
        if (size <= envsize)
        {
            memcpy(ppib->pib_pchenv, pmem, size);
            return;                      /* success                    */
        }
        else if (size <= 65535)
        {
            ULONG grow = size - envsize;
            if (DosSetMem(ppib->pib_pchenv+grow, grow, PAG_COMMIT | PAG_WRITE | PAG_READ) == 0)
            {
                memcpy(ppib->pib_pchenv, pmem, size);
                return;                  /* success                    */
            }
        }
    }

    reportException(Error_System_service_service, "Error restoring environment");
}


/**
 * Push a new environment for the SysSetLocal() BIF.
 *
 * @param context The current activation context.
 *
 * @return Returns TRUE if the environment was successfully pushed.
 */
RexxObject* SystemInterpreter::pushEnvironment(RexxActivation *context)
{
    RexxObject *Current = buildEnvlist();  /* build the new save block    */
    if (Current == OREF_NULL)              /* if unsuccessful return zero */
    {
        return TheFalseObject;
    }
    else
    {
        context->pushEnvironment(Current); /* update environemnt list    */
        return TheTrueObject;              /* this returns one           */
    }
}


/**
 * Pop an environment for the SysEndLocal() BIF.
 *
 * @param context The current activation context.
 *
 * @return Always returns FALSE.  This is a NOP on Windows.
 */
RexxObject* SystemInterpreter::popEnvironment(RexxActivation *context)
{
    BufferClass *Current = (BufferClass *)context->popEnvironment();
    if (TheNilObject == Current)           /* nothing saved?            */
    {
        return TheFalseObject;             /* return failure value      */
    }
    else
    {
        restoreEnvironment(Current->getData());
        return TheTrueObject;              /* this worked ok            */
    }
}

/******************************************************************************/
/* Name:       SysExternalFunction                                            */
/*                                                                            */
/* Notes:      Handles searching for and executing an external function.  The */
/*             search order is:                                               */
/*               1) Macro-space pre-order functions                           */
/*               2) Registered external functions                             */
/*               3) REXX programs with same extension (if applicable)         */
/*               4) REXX programs with default extension                      */
/*               5) Macro-space post-order functions                          */
/******************************************************************************/
bool SystemInterpreter::invokeExternalFunction(
    RexxActivation *activation,         /* Current Activation                */
    Activity       *activity,           /* activity in use                   */
    RexxString     *target,             /* Name of external function         */
    RexxObject    **arguments,          /* Argument array                    */
    size_t          argcount,           /* count of arguments                */
    RexxString     *calltype,           /* Type of call                      */
    ProtectedObject &result)
{
    if (activation->callMacroSpaceFunction(target, arguments, argcount, calltype, RXMACRO_SEARCH_BEFORE, result))
    {
        return true;
    }
    /* no luck try for a registered func */
    if (PackageManager::callNativeRoutine(activity, target, arguments, argcount, result))
    {
        return true;
    }
    /* have activation do the call       */
    if (activation->callExternalRexx(target, arguments, argcount, calltype, result))
    {
        return true;
    }
    /* function.  If still not found,    */
    /* then raise an error               */
    if (activation->callMacroSpaceFunction(target, arguments, argcount, calltype, RXMACRO_SEARCH_AFTER, result))
    {
        return true;
    }

    return false;
}


/*********************************************************************/
/*                                                                   */
/*   Subroutine Name:   RxMessageBox                                 */
/*                                                                   */
/*   Descriptive Name:  RxMessageBox function                        */
/*                                                                   */
/*   Function:          pops up a PM message box when called from    */
/*                      a PM session.                                */
/*   Parameters:                                                     */
/*        Input:                                                     */
/*                      Text   = The message box text.               */
/*                      Title  = The message box title.              */
/*                      Button = The message box button style.       */
/*                      Icon   = The message box icon style.         */
/*                                                                   */
/*********************************************************************/
RexxRoutine4(int, SysMessageBox, CSTRING, text, OPTIONAL_CSTRING, title,
             OPTIONAL_CSTRING, button, OPTIONAL_CSTRING, icon)
{
    ULONG style;                // window style flags
    int maxCnt;
    int i;

    PCSZ Button_Styles[] =      // message box button styles
    {
        "OK",
        "OKCANCEL",
        "CANCEL",
        "ENTER",
        "ENTERCANCEL",
        "RETRYCANCEL",
        "ABORTRETRYIGNORE",
        "YESNO",
        "YESNOCANCEL"
    };

    ULONG Button_Flags[] =
    {
        MB_OK,
        MB_OKCANCEL,
        MB_CANCEL,
        MB_ENTER,
        MB_ENTERCANCEL,
        MB_RETRYCANCEL,
        MB_ABORTRETRYIGNORE,
        MB_YESNO,
        MB_YESNOCANCEL
    };

    PCSZ Icon_Styles[] =        // message box icon styles
    {
        "NONE",
        "INFORMATION",
        "QUERY",
        "WARNING",
        "ERROR",
        "ASTERISK",
        "QUESTION",
        "EXCLAMATION",
        "HAND"
    };

    ULONG Icon_Flags[] =                 /* message box icon styles    */
    {
        MB_NOICON,
        MB_INFORMATION,
        MB_QUERY,
        MB_WARNING,
        MB_ERROR,
        MB_ICONASTERISK,
        MB_ICONQUESTION,
        MB_ICONEXCLAMATION,
        MB_ICONHAND
    };

    style = MB_MOVEABLE | MB_APPLMODAL;

    if (button == NULL)
    {
        // The default is an OK message box.
        style |= MB_OK;
    }
    else
    {
        // Search the button style table.
        maxCnt = sizeof(Button_Styles)/sizeof(PSZ);
        for (i = 0; i < maxCnt; i++)
        {
            if (stricmp(button, Button_Styles[i]) == 0)
            {
                // Found a match.  Only 1 button style can be used, so break.
                style |= Button_Flags[i];
                break;
            }
        }
        if (i == maxCnt)
        {
            // User specified an invalid button style word.
            context->InvalidRoutine();
            return 0;
        }
    }

    if (icon == NULL)
    {
        // The default is no icon.
        style |= MB_NOICON;
    }
    else
    {
        // Search the icon style table.
        maxCnt = sizeof(Icon_Styles)/sizeof(PSZ);
        for (i = 0; i < maxCnt; i++)
        {
            if (stricmp(icon,Icon_Styles[i]) == 0)
            {
                // Found a match.  Only 1 icon stle can be used, so break.
                style |= Icon_Flags[i];
                break;
            }
        }
        if (i == maxCnt)
        {
            // User specified an invalid icon style word.
            context->InvalidRoutine();
            return 0;
        }
    }

    return WinMessageBox(HWND_DESKTOP, NULLHANDLE, text, title, 100, style);
}
