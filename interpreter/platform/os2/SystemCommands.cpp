/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Copyright (c) 1995, 2004 IBM Corporation. All rights reserved.             */
/* Copyright (c) 2005-2021 Rexx Language Association. All rights reserved.    */
/*                                                                            */
/* This program and the accompanying materials are made available under       */
/* the terms of the Common Public License v1.0 which accompanies this         */
/* distribution. A copy is also available at the following address:           */
/* https://www.oorexx.org/license.html                                        */
/*                                                                            */
/* Redistribution and use in source and binary forms, with or                 */
/* without modification, are permitted provided that the following            */
/* conditions are met:                                                        */
/*                                                                            */
/* Redistributions of source code must retain the above copyright             */
/* notice, this list of conditions and the following disclaimer.              */
/* Redistributions in binary form must reproduce the above copyright          */
/* notice, this list of conditions and the following disclaimer in            */
/* the documentation and/or other materials provided with the distribution.   */
/*                                                                            */
/* Neither the name of Rexx Language Association nor the names                */
/* of its contributors may be used to endorse or promote products             */
/* derived from this software without specific prior written permission.      */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        */
/* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT          */
/* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS          */
/* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,      */
/* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   */
/* TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,        */
/* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     */
/* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    */
/* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         */
/* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/* REXX OS/2  Support                                                         */
/*                                                                            */
/* os/2 specific command processing routines                                  */
/*                                                                            */
/******************************************************************************/

#include "SystemInterpreter.hpp"
#include "SysThread.hpp"
#include "SysProcess.hpp"
#include "SysFile.hpp"
#include "ActivityManager.hpp"

const int PIPE_BUF_SIZE = 1024;

/**
 * Retrieve the globally default initial address.
 *
 * @return The string name of the default address.
 */
RexxString *SystemInterpreter::getDefaultAddressName()
{
    return GlobalNames::INITIALADDRESS;
}


/**
 * A thread that writes INPUT data to the command pipe
 */
class WriterThread : public SysThread
{
public:
    inline WriterThread() : SysThread(),
        pipe(0), inputBuffer(NULL), bufferLength(0), error(0) {}

    inline ~WriterThread() { terminate(); }

    void start(HFILE hPipe)
    {
        pipe = hPipe;
        SysThread::createThread();
    }
    virtual void dispatch()
    {
        ULONG bytesWritten = 0;
        APIRET rc = DosWrite(pipe, (char*)inputBuffer, bufferLength, &bytesWritten);

        if (rc != 0)
        {
            // We may receive ERROR_BROKEN_PIPE, which we only expect if
            // a executed finishes while we're still busy trying to pipe
            // our input.  The command may simply not need (i. e. want
            // to read) our input. We dont' consider this an error.
            if (rc != ERROR_BROKEN_PIPE)
            {
                error = rc;
            }
        }
        DosClose(pipe);
    }

    const char *inputBuffer;    // the buffer of data to write
    size_t      bufferLength;   // the length of the buffer
    HFILE       pipe;           // the pipe we write the data to
    APIRET      error;          // and error that resulted.
};


/**
 * A thread that reads ERROR or OUTPUT data from the command pipe
 */
class ReaderThread : public SysThread
{
public:
    inline ReaderThread() : SysThread(),
        pipe(0), pipeBuffer(NULL), dataLength(0), error(0) { }

    inline ~ReaderThread()
    {
        if (pipeBuffer != NULL)
        {
            free(pipeBuffer);
            pipeBuffer = NULL;
        }
        terminate();
    }

    void start(HFILE hPipe)
    {
        pipe = hPipe;
        SysThread::createThread();
    }

    virtual void dispatch()
    {
        ULONG cntRead;
        size_t bufferSize = PIPE_BUF_SIZE;          // current size of our buffer
        pipeBuffer = (char *)malloc(PIPE_BUF_SIZE); // allocate an initial buffer

        if (pipeBuffer == NULL)
        {
            // use the OS2 error code
            error = ERROR_NOT_ENOUGH_MEMORY;
            return;
        }
        for (;;)
        {
            if ((error = DosRead(pipe, pipeBuffer + dataLength, bufferSize - dataLength, &cntRead)) != 0 || cntRead == 0)
            {
                break;
            }

            dataLength += cntRead;
            // have we hit the end of the buffer?
            if (dataLength >= bufferSize)
            {
                // increase the buffer by another increment
                bufferSize += PIPE_BUF_SIZE;
                char *largerBuffer = (char *)realloc(pipeBuffer, bufferSize);
                if (largerBuffer == NULL)
                {
                    // use the OS/2 error code
                    error = ERROR_NOT_ENOUGH_MEMORY;
                    return;
                }
                pipeBuffer = largerBuffer;
            }
        }
        DosClose(pipe);
    }

    HFILE  pipe;        // the pipe we read the data from
    char  *pipeBuffer;  // initally errorBuffer = firstBuffer
    size_t dataLength;  // the length of data we've read
    APIRET error;       // and error that resulted.
};


/******************************************************************************/
/* Name:       sysCommandOS2                                                  */
/*                                                                            */
/* Arguments:  cmd - Command to be executed                                   */
/*                                                                            */
/* Returned:   rc - Return Code                                               */
/*                                                                            */
/* Notes:      Handles processing of a system command on the OS/2 system      */
/*                                                                            */
/******************************************************************************/
static APIRET sysCommandOS2(RexxExitContext *context,
                            const char *cmd,
                            const char *cmdstring,
                            RexxObjectPtr &result,
                            RexxIORedirectorContext *ioContext)
{
    WriterThread inputThread;       // used if need to write input
    ReaderThread outputThread;      // separate thread if we need to read OUTPUT
    ReaderThread errorThread;       // separate thread if we need to read ERROR

    // handles for pipes if needed
    HFILE  hSaveOut = -1, hCurrOut = SysFile::stdoutHandle,
           hSaveErr = -1, hCurrErr = SysFile::stderrHandle,
           hSaveIn  = -1, hCurrIn  = SysFile::stdinHandle,
           wPipeOut = -1, rPipeOut = -1,
           wPipeErr = -1, rPipeErr = -1,
           wPipeIn  = -1, rPipeIn  = -1;

    APIRET rc;
    size_t i, len = strlen(cmdstring);
    PSZ p, pArg = (PSZ)malloc(len+2);
    bool inQuotes = false;

    if (!pArg)
    {
        return ERROR_NOT_ENOUGH_MEMORY;
    }

    // The convention used by CMD.EXE is that the first of
    // argument strins is the program name (as entered from the
    // command prompt or found in a batch file), and the second
    // string consists of the parameters for the program. The
    // second ASCIIZ string is followed by an additional byte of
    // zeros.
    for (p = pArg, i = 0; i <= len; i++)
    {
        CHAR ch = cmdstring[i];

        if (ch == '"')
        {
            inQuotes = !inQuotes;
        }
        else if (ch == ' ' && !inQuotes)
        {
            *p++ = 0;
            // copy remain part as parameters
            for (i++ ; i <= len; i++)
            {
              *p++ = cmdstring[i];
            }
            break;
        }
        else
        {
            *p++ = ch;
        }
    }
    // additional byte of zeros.
    *p = 0;

    if (ioContext->IsRedirectionRequested())
    {
        // create the pipes needed to implement the redirection
        if (ioContext->IsInputRedirected())
        {
            if ((rc = DosCreatePipe(&rPipeIn, &wPipeIn, PIPE_BUF_SIZE)) != 0 ||
                (rc = DosSetFHState(wPipeIn, OPEN_FLAGS_NOINHERIT)) != 0 ||
                (rc = DosDupHandle(SysFile::stdinHandle, &hSaveIn)) != 0 ||
                (rc = DosDupHandle(rPipeIn, &hCurrIn)) != 0)
            {
                context->RaiseException1(Error_Execution_address_redirection_failed,
                                         context->Int32ToObject(rc));
            }
        }
        if (ioContext->IsOutputRedirected())
        {
            if ((rc = DosCreatePipe(&rPipeOut, &wPipeOut, PIPE_BUF_SIZE)) != 0 ||
                (rc = DosSetFHState(rPipeOut, OPEN_FLAGS_NOINHERIT)) != 0 ||
                (rc = DosDupHandle(SysFile::stdoutHandle, &hSaveOut)) != 0 ||
                (rc = DosDupHandle(wPipeOut, &hCurrOut)) != 0)
            {
                context->RaiseException1(Error_Execution_address_redirection_failed,
                                         context->Int32ToObject(rc));
            }
        }
        if (ioContext->IsErrorRedirected())
        {
            // send output and error to the same stream
            if (ioContext->AreOutputAndErrorSameTarget())
            {
                if ((rc = DosDupHandle(SysFile::stderrHandle, &hSaveErr)) != 0 ||
                    (rc = DosDupHandle(wPipeOut, &hCurrErr)) != 0)
                {
                    context->RaiseException1(Error_Execution_address_redirection_failed,
                                             context->Int32ToObject(rc));
                }
            }
            else
            {
                if ((rc = DosCreatePipe(&rPipeErr, &wPipeErr, PIPE_BUF_SIZE)) != 0 ||
                    (rc = DosSetFHState(rPipeErr, OPEN_FLAGS_NOINHERIT)) != 0 ||
                    (rc = DosDupHandle(SysFile::stderrHandle, &hSaveErr)) != 0 ||
                    (rc = DosDupHandle(wPipeErr, &hCurrErr)) != 0)
                {
                    context->RaiseException1(Error_Execution_address_redirection_failed,
                                             context->Int32ToObject(rc));
                }
            }
        }
    }

    CHAR szFailName[CCHMAXPATH];
    RESULTCODES resCodes;
    PID pid;

    rc = DosExecPgm(szFailName, sizeof(szFailName),
                    EXEC_ASYNCRESULT, pArg, NULL, &resCodes, pArg);

    // restore standard input, output and error handles
    if (hSaveIn != -1)
    {
        DosDupHandle(hSaveIn, &hCurrIn);
        DosClose(hSaveIn);
        DosClose(rPipeIn);
    }
    if (hSaveOut != -1)
    {
        DosDupHandle(hSaveOut, &hCurrOut);
        DosClose(hSaveOut);
        DosClose(wPipeOut);
    }
    if (hSaveErr != -1)
    {
        DosDupHandle(hSaveErr, &hCurrErr);
        DosClose(hSaveErr);

        if (wPipeErr != -1)
        {
            DosClose(wPipeErr);
        }
    }

    if (rc == 0)
    {
        // if input is redirected, write the input data to the input pipe
        if (ioContext->IsInputRedirected())
        {
            ioContext->ReadInputBuffer(&inputThread.inputBuffer, &inputThread.bufferLength);
            // only start the thread if we have real data
            if (inputThread.inputBuffer != NULL)
            {
                // we need the pipe handle to do this
                inputThread.start(wPipeIn);
            }
        }
        // if output is redirected, write the data from the output pipe
        if (ioContext->IsOutputRedirected())
        {
            // we start a separate thread to read data from the stdout pipe
            outputThread.start(rPipeOut);
        }
        // if error is redirected, write the data from the error pipe
        if (ioContext->IsErrorRedirected() && !ioContext->AreOutputAndErrorSameTarget())
        {
            // we start a separate thread to read data from the error pipe
            errorThread.start(rPipeErr);
        }

        if ((rc = DosWaitChild(DCWA_PROCESS, DCWW_WAIT, &resCodes, &pid, resCodes.codeTerminate)) == 0)
        {
            // do we have input cleanup to perform?
            if (ioContext->IsInputRedirected())
            {
                // close the process end of the pipe
                DosClose(wPipeOut);
                // wait for everything to complete
                inputThread.waitForTermination();
                // the input thread may have encountered an error .. raise it now
                if (inputThread.error != 0)
                {
                    context->RaiseException1(Error_Execution_address_redirection_failed,
                                             context->Int32ToObject(inputThread.error));
                    return inputThread.error;
                }
            }
            // did we start an output thread?
            if (ioContext->IsOutputRedirected())
            {
                // close the handle so DosRead will stop
                DosClose(rPipeOut);
                // wait for the output thread to finish
                outputThread.waitForTermination();
                if (outputThread.dataLength > 0)
                {   // return what the output thread read from its pipe
                    ioContext->WriteOutputBuffer(outputThread.pipeBuffer, outputThread.dataLength);
                }
                // the output thread may have encountered an error .. raise it now
                if (outputThread.error != 0)
                {
                    context->RaiseException1(Error_Execution_address_redirection_failed,
                                             context->Int32ToObject(outputThread.error));
                    return outputThread.error;
                }
            }
            // did we start an error thread?
            if (ioContext->IsErrorRedirected() && !ioContext->AreOutputAndErrorSameTarget())
            {
                // close the handle so DosRead will stop
                DosClose(rPipeErr);
                // wait for the error thread to finish
                errorThread.waitForTermination();
                if (errorThread.dataLength > 0)
                {   // return what the error thread read from its pipe
                    ioContext->WriteOutputBuffer(errorThread.pipeBuffer, errorThread.dataLength);
                }
                // the error thread may have encountered an error .. raise it now
                if (errorThread.error != 0)
                {
                    context->RaiseException1(Error_Execution_address_redirection_failed,
                                             context->Int32ToObject(errorThread.error));
                    return errorThread.error;
                }
            }
        }
        else
        {
            context->RaiseCondition("FAILURE", context->String(cmd), NULLOBJECT, context->WholeNumberToObject(rc));
            result = NULLOBJECT;
            return rc;
        }
    }
    else
    {
        // return this as a failure for now ... the caller might try this again
        // later
        return rc;
    }

    if (resCodes.codeResult != 0)
    {
        context->RaiseCondition("ERROR", context->String(cmd), NULLOBJECT, context->Int32ToObject(resCodes.codeResult));
        result = NULLOBJECT;
    }
    else
    {
        // this is a zero return
        result = context->False();
    }

    return resCodes.codeResult;
}


/******************************************************************************/
/* Name:       sysProcessSet                                                  */
/*                                                                            */
/* Arguments:  cmd - Command to be executed                                   */
/*                                                                            */
/* Returned:   rc - Return Code                                               */
/*                                                                            */
/* Notes:      Handle "SET XX=YYY" command in same process                    */
/*                                                                            */
/******************************************************************************/
static bool sysProcessSet(RexxExitContext *context, const char *cmd, RexxObjectPtr &result)
{
    char name [1024] = "";
    char value[1024] = "";
    result = NULLOBJECT;

    sscanf(cmd + 4, " %1024[^=]=%1024c", name, value);
    if (!*name || !*value)
    {
        return false;
    }

    APIRET rc = SysProcess::setEnvironmentVariable(name, value);

    if (rc == 0)
    {
        // just return a zero
        result = context->False();
    }
    else
    {
        context->RaiseCondition("ERROR", context->String(cmd), NULLOBJECT, context->WholeNumberToObject(rc));
    }

    return true;
}


/******************************************************************************/
/* Name:       sysProcessChDir                                                */
/*                                                                            */
/* Arguments:  cmd - Command to be executed                                   */
/*                                                                            */
/* Returned:   rc - Return Code                                               */
/*                                                                            */
/* Notes:      Handle "CD XXX" command in same process                        */
/*                                                                            */
/******************************************************************************/
static bool sysProcessChDir(RexxExitContext *context, const char *cmd, RexxObjectPtr &result)
{
    result = NULLOBJECT;
    PSZ pszDir = (PSZ)malloc(strlen(cmd) + 1);

    if (!pszDir)
    {
        context->RaiseCondition("ERROR", context->String(cmd), NULLOBJECT,
                                 context->String("Failed to allocate memory"));
        return true;
    }

    const char *s = cmd + 3;
    PSZ p = pszDir;

    // scan for the first non-whitespace character
    while (isspace(*s))
    {
        s++;
    }
    // unquote
    while ((*p = *s++) != 0)
    {
        if (*p != '"')
        {
            p++;
        }
    }

    APIRET rc = DosSetCurrentDir(pszDir);

    if (rc != 0)
    {
        context->RaiseCondition("ERROR", context->String(cmd), NULLOBJECT, context->WholeNumberToObject(rc));
    }
    else
    {
        result = context->False();
    }

    return true;
}


/******************************************************************************/
/* Name:       sysProcessChDrive                                              */
/*                                                                            */
/* Arguments:  cmd - Command to be executed                                   */
/*                                                                            */
/* Returned:   rc - Return Code                                               */
/*                                                                            */
/* Notes:      Handle "X:" command in same process                            */
/*                                                                            */
/******************************************************************************/
static bool sysProcessChDrive(RexxExitContext *context, const char *cmd, RexxObjectPtr &result)
{
    result = NULLOBJECT;
    APIRET rc = DosSetDefaultDisk(toupper(cmd[0])-'A'+1);

    if (rc != 0)
    {
        context->RaiseCondition("ERROR", context->String(cmd), NULLOBJECT, context->WholeNumberToObject(rc));
    }
    else
    {
        result = context->False();
    }

    return true;
}


/******************************************************************************/
/* Name:       systemCommandHandler                                           */
/*                                                                            */
/* Arguments:  command - Command to be executed                               */
/*                                                                            */
/* Returned:   rc - Return Code                                               */
/*                  Note: if non-zero rc from DosExecPgm return DosExecPgm rc */
/*                  else if non-zero termination code from system return code */
/*                  else return rc from executed command                      */
/*                                                                            */
/* Notes:      Handles processing of a system command.  Finds location of     */
/*             system command handler using the COMSPEC environment variable  */
/*             and invokes the system specific routine which invokes the      */
/*             command handler with the command to be executed                */
/*                                                                            */
/******************************************************************************/
RexxObjectPtr RexxEntry systemCommandHandler(RexxExitContext *context,
                                             RexxStringObject address,
                                             RexxStringObject command,
                                             RexxIORedirectorContext *ioContext)
{
    const char *cmd = context->StringData(command);
    RexxObjectPtr result = NULLOBJECT;
    APIRET rc;

    // is this directed to the no-shell "PATH" environment?
    if (stricmp(context->StringData(address), "path") == 0)
    {
        if ((rc = sysCommandOS2(context, cmd, cmd, result, ioContext)) != 0)
        {
            context->RaiseCondition("FAILURE", context->String(cmd), NULLOBJECT, context->WholeNumberToObject(rc));
            return NULLOBJECT;
        }
        return result;
    }

    // Remove the "quiet sign" if present
    if (cmd[0] == '@')
    {
        cmd++;
    }
    // scan for the first non-whitespace character
    while (isspace(*cmd))
    {
        cmd++;
    }

    // Check for redirection symbols, ignore them when enclosed in double quotes.
    bool   DirectExec = true;
    bool   inQuotes = false;
    size_t len = strlen(cmd);
    size_t i;

    for (i = 0; i < len; i++)
    {
        if (cmd[i] == '"')
        {
            inQuotes = !inQuotes;
        }
        else
        {
            // if we're in the unquoted part and the current character is one of */
            // the redirection characters or the & for multiple commands then we */
            // will no longer try to invoke the command directly                 */
            if (!inQuotes && (strchr("<>|&", cmd[i]) != NULL))
            {
                DirectExec = false;
            }
        }
    }

    if (DirectExec)
    {
        if (strnicmp(cmd, "set ", 4) == 0)
        {
            if (sysProcessSet(context, cmd, result))
            {
                return result;
            }
        }
        else if (strnicmp(cmd, "cd ", 3) == 0)
        {
            if (sysProcessChDir(context, cmd, result))
            {
                return result;
            }
        }
        // Check if the command is to change drive
        else if (cmd[1] == ':' && ((cmd[2] == ' ') || (!cmd[2])))
        {
            if (sysProcessChDrive(context, cmd, result))
            {
                return result;
            }
        }
        // Check if a START command is specified, if so do not
        // invoke the command directly.
        else if (strnicmp(cmd, "start ", 6) == 0)
        {
            DirectExec = false;
        }
    }

    // First check whether we can run the command directly as a program.
    // (There can be no file redirection)
    // N.B. ADDRESS ... WITH also implies redirection
    if (DirectExec && !ioContext->IsRedirectionRequested())
    {
        // Invoke this directly.  If we fail, we fall through and try again.
        if (sysCommandOS2(context, cmd, cmd, result, ioContext) == 0)
        {
            return result;
        }
    }

    // We couldn't invoke the command directly, or we tried and failed.  So,
    // pass the command to cmd.exe.

    PSZ  comspec;
    PCSZ comspec_opt = " /c ";  // The "/c" opt for cmd.exe

    // Determine the system command interpreter.  This could be the full path
    // name if COMSPEC is set, or a default name if it isn't.
    if (DosScanEnv("COMSPEC", &comspec) != 0)
    {
        comspec = (PSZ)"cmd.exe";
    }

    // Determine the maximum possible buffer size needed to pass the final
    // command to sysCommandOS2().
    size_t maxBufferSize = strlen(comspec) + 1
                           + strlen(comspec_opt)
                           + strlen(cmd)
                           + 1;

    char *cmdstring = (char*)malloc(maxBufferSize);
    if (!cmdstring)
    {
        context->RaiseException1(Rexx_Error_System_resources_user_defined,
                                 context->String("Failed to allocate memory"));
        return NULLOBJECT;
    }

    // Start the command buffer with the system command handler
    strcpy(cmdstring, comspec);

    // Check whether or not the user specified the /k option.  If so do not use
    // the /c option.  The /k option can only be specified as the first
    // argument, and if used, keeps the command handler process open after the
    // command has finished.  Normally the /c option would be usee to close the
    // command handler process when the command is finished..
    if (cmd[0] == '/' && (cmd[1] == 'k' || cmd[1] == 'K') && (cmd[2] == ' ' || cmd[2] == 0))
    {
        strcat(cmdstring, " ");
    }
    else
    {
        strcat(cmdstring, comspec_opt);
    }

    // Add cmd to be executed.
    strcat(cmdstring, cmd);

    // Invoke the command
    if ((rc = sysCommandOS2(context, cmd, cmdstring, result, ioContext)) != 0)
    {
        // Failed, get error code and return
        context->RaiseCondition("FAILURE", context->String(cmd), NULLOBJECT, context->WholeNumberToObject(rc));
        free(cmdstring);
        return NULLOBJECT;
    }

    free(cmdstring);
    return result;
}


/**
 * Register the standard system command handlers.
 *
 * @param instance The created instance.
 */
void SysInterpreterInstance::registerCommandHandlers(InterpreterInstance *instance)
{
    // This is a trick that allows OS/2 command processors to use the subcommand handler.
    // The best way is to assign a higher priority to handlers registered via
    // RexxRegistersubcomexe (see InterpreterInstance :: Resolvecommandhandler).
    if (!CommandHandler("CMD").isResolved())
    {
        instance->addCommandHandler("CMD", (REXXPFN)systemCommandHandler, HandlerType::REDIRECTING);
    }

    // The default command handler on OS/2 is "CMD"
    // It comes with three aliases named "", "COMMAND", and "SYSTEM"
    // "SYSTEM" is compatible with Regina
    instance->addCommandHandler("",        (REXXPFN)systemCommandHandler, HandlerType::REDIRECTING);
    instance->addCommandHandler("COMMAND", (REXXPFN)systemCommandHandler, HandlerType::REDIRECTING);
    instance->addCommandHandler("SYSTEM",  (REXXPFN)systemCommandHandler, HandlerType::REDIRECTING);

    // This is a no-shell environment that searches PATH.  It is named "PATH"
    // which happens to be compatible with Regina.
    instance->addCommandHandler("PATH",    (REXXPFN)systemCommandHandler, HandlerType::REDIRECTING);
}
